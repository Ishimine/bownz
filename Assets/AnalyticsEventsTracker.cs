﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class AnalyticsEventsTracker : MonoBehaviour {

    public GoogleAnalyticsV4 googleAnalytics;


    public Administrador_Progreso progreso;

    public VariableString id_modoActivo;

    public VariableFloat puntaje_Actual;

    private void Awake()
    {
    }

    private void Start()
    {

        googleAnalytics.StartSession();
    }

    public void EnviarPuntajeLogrado()
    {
        googleAnalytics.LogEvent("Puntajes", "Player Dead", "GameOver in " + id_modoActivo, (long)puntaje_Actual.Value);
    }


    public void EnviarSubidaDeNivel()
    {
        googleAnalytics.LogEvent("Progreso", "Level Up", "Level Up", (long)progreso.Progreso.nivel);
    }

}
