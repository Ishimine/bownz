﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Admin_Progreso_Proxy : MonoBehaviour {

    


    public Administrador_Progreso admin;
    public ConfiguracionGeneral config;

    private void Awake()
    {
        config.SwipeReader_Activado = true;
        admin.CargarProgresoLocal();
    }

    private void Start()
    {
        if (admin.IsFirstTime)
        {
            admin.BorrarProgreso();
            admin.IsFirstTime = false;
        }
    }
}
