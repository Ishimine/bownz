﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class MarcadorEstrellaD4 : MarcadorSimple {

    public bool usarDelayDeAnimacion = true;
    public float delayDeAnimacion = 2f;

    /// <summary>
    /// IfTrue: Se ejecutara la animacion de entrada seguida por la de salida sin activar la animacion central.
    /// </summary>
    public bool usarAnimInOut;

    [ShowIf("usarAnimInOut")]
    public float tEntreAnim = 1.5f;

    public float escalaMaxima = 1.2f;


    public SpriteRenderer render;
  //  public SpriteRenderer render_2;


    public Color cBrillo = Color.white;

    public float tAnimIn = .5f;
    public float tAnim = 4;
    public float tAnimOut = .5f;

    IEnumerator rutina;

    public bool animIn = false;

    public override void Activar(float t)
    {
        tAnim = 4;

        transform.localScale = Vector3.zero;
        if(usarAnimInOut)
        {
            tAnim = t;
            AnimInOut();
        }
        else
        {
            animIn = true;            
            AnimIn(tAnimIn);
        }
    }

    public void AnimPrincipal(float tiempo)
    {
        if (rutina != null) StopCoroutine(rutina);
        rutina = Anim(tiempo);
        StartCoroutine(rutina);
    }

    public void Activar()
    {
        Activar(tAnim);
    }

    public override void Desactivar()
    {
        StopCoroutine(rutina);
    }

    void AnimIn(float tiempo)
    {
        if (rutina != null) StopCoroutine(rutina);
        rutina = TransicionDeEscala(Vector3.zero, Vector3.one, tiempo);
        StartCoroutine(rutina);

    }

    void AnimOut(float tiempo)
    {
        if (rutina != null) StopCoroutine(rutina);
        rutina = TransicionDeEscala(Vector3.one * escalaMaxima, Vector3.zero, tiempo);
        StartCoroutine(rutina);        
    }

    void AnimInOut(float tiempo)
    {
        if (rutina != null) StopCoroutine(rutina);
        rutina = TransicionDeEscalaInOut(Vector3.zero, Vector3.one * escalaMaxima, tiempo);
        StartCoroutine(rutina);
    }


    IEnumerator TransicionDeEscala(Vector3 a, Vector3 b, float segundos)
    {
        if (usarDelayDeAnimacion)
        {
            yield return new WaitForSeconds(delayDeAnimacion);
        }

        float t = 0;
        do
        {
            t += Time.deltaTime / segundos;
            transform.localScale = Vector3.Lerp(a, b, Mathf.Sin((t/2) * Mathf.PI));

            render.color = Color.Lerp(Color.white, cBrillo, transform.localScale.x);
            
            yield return null;
        } while (t < 1);


        if(animIn)
        {
            AnimPrincipal(tAnim - tAnimIn - tAnimOut);
        }
    }

    IEnumerator TransicionDeEscalaInOut(Vector3 a, Vector3 b, float segundos)
    {
        if(usarDelayDeAnimacion)
        {
            yield return new WaitForSeconds(delayDeAnimacion);
        }

        float t = 0;
        do
        {
            t += Time.deltaTime / segundos;
            transform.localScale = Vector3.Lerp(a, b, Mathf.Sin(t * Mathf.PI));

            render.color = Color.Lerp(Color.white, cBrillo, transform.localScale.x);

            yield return null;
        } while (t < 1);

     /*   if(usarAnimInOut)
        {
            yield return new WaitForSeconds(tEntreAnim);

            if (rutina != null) StopCoroutine(rutina);
            rutina = TransicionDeEscalaInOut(Vector3.zero, Vector3.one * escalaMaxima, tAnimIn);
            StartCoroutine(rutina);
        }*/
    }


    IEnumerator Anim(float tiempo)
    {
        float t = 0;
        do
        {
            t += Time.deltaTime / tiempo;
            yield return null;
        } while (t < 1);

        if (animIn)
        {
            animIn = false;
            AnimOut();
        }
    }

    [Button]
    public void AnimIn()
    {
        AnimIn(tAnimIn);
    }

    [Button]
    public void AnimOut()
    {
        AnimOut(tAnimOut);
    }

    [Button]
    public void AnimInOut()
    {
        AnimInOut(tAnimIn);
    }



}
