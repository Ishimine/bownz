﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class BannerFisico : SerializedMonoBehaviour {

    /// <summary>
    /// Altura en pixeles
    /// </summary>
    public float altura = 50;
    public float aux = 1;

    public GameObject cuerpo;

    private void Start()
    {
        Aplicar();
    }


    [Button]
    public void Aplicar()
    {
        PosicionarEscalar();
    }

    [Button]
    public void CalcularAltura()
    {

    }

    [Button]
    public float GetAlturaDPI()
    {
        aux = DisplayMetricsAndroid.YDPI;
        return DisplayMetricsAndroid.YDPI;
    }

    [Button]
    public float GetDP()
    {
        float px = Screen.width * Screen.height;
        float dp = px / (Screen.dpi / 160);
        aux = dp;
        return dp;
    }

    [Button]
    public void PosicionarEscalar()
    {
        float y = Camera.main.orthographicSize;
        transform.position = Vector3.down * y;
        cuerpo.transform.localPosition = Vector3.up * (altura / 2);
        cuerpo.transform.localScale = new Vector3(Camera.main.orthographicSize * 2 * Camera.main.aspect, altura * aux, cuerpo.transform.localScale.z);
    }
	
}

