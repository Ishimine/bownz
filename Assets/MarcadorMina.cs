﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class MarcadorMina : SerializedMonoBehaviour, IMarcadorSpawn
{
    [Range(0,1)]
    public float activarAnimPreSpawn;

    public bool usarCrecimientoConstante = false;
    public bool destellar = false;
    [ShowIf("destellar")]
    public bool destellarConEscala = true;

    public int cantDestellos = 2;

    public bool usarFrecuenciaFija = true;
    public float frecuencia = 5;
    [HideIf("usarFrecuenciaFija")]
    public float frecInicial;


    public bool usarLatido = true;

    [ShowIf("MostrarEscala")]
    public float escalaMax = 1.2f;
    [ShowIf("MostrarEscala")]
    public float escalaMin = .5f;


    public bool usarBrilloLerp = false;

    
    public SpriteRenderer render;
    public Color cBajo;
    public Color cBrillo;
    IEnumerator rutina;

    public void SetParent(Transform parent)
    {
        transform.SetParent(parent);
    }

    private bool MostrarEscala()
    {
        if (usarLatido || usarCrecimientoConstante|| destellarConEscala)
            return true;
        else
            return false;
    }


    [Button]
    public void AplicarColorBajo()
    {
        render.color = cBajo;
    }

    [Button]
    public void AplicarColorBrillo()
    {
        render.color = cBrillo;
    }

    [Button]
    public void Activar()
    {
        Activar(4);
    }
   
    public void Activar(float t)
    {
        if (rutina != null) StopCoroutine(rutina);
        rutina = Animacion(t);
        StartCoroutine(rutina);
    }

    public void SetActive(bool isActive)
    {
        gameObject.SetActive(isActive);
    }

    IEnumerator Animacion(float tAnimacion)
    {
        gameObject.transform.localScale = Vector3.one * escalaMin;
        render.color = cBajo;
        float t = 0;
        float frecActual = frecuencia * Mathf.PI;

        bool destellando = false;
        do
        {
            t += Time.deltaTime / tAnimacion;

            if (!usarFrecuenciaFija)
                frecActual = Mathf.Lerp(frecInicial, frecuencia, t) * Mathf.PI;


            float x = Mathf.Abs(Mathf.Sin(t * frecActual));

            if (!destellar)
            {
                if (!usarBrilloLerp)
                {
                    if (x > .5f)
                    {
                        render.color = cBrillo;
                    }
                    else
                    {
                        render.color = cBajo;
                    }
                }
                else
                {
                    render.color = Color.Lerp(cBajo, cBrillo, x);
                }
            }            
            else if(!destellando && t >= activarAnimPreSpawn)
            {
                destellando = true;
                StartCoroutine(AnimDestellando((1-t)*tAnimacion));
            }
            

            if(usarLatido)
            {
                gameObject.transform.localScale = Vector3.one * Mathf.Lerp(escalaMin, escalaMax, x);
            }
            else if(usarCrecimientoConstante)
            {
                Vector3 escala = Vector3.one * Mathf.Lerp(escalaMin, escalaMax, t);
                gameObject.transform.localScale = escala;
            }

            //            render.color = new Color(cOrig.r, cOrig.g, cOrig.b, Mathf.Sin(t * frecActual) * cOrig.a);


            yield return null;
        } while (t < 1);
        
    }

    IEnumerator AnimDestellando(float x)
    {
        Debug.Log("ANIMDESTELLANDO SADSADSASADDSSDAS");
        float t = 0;

        if (cantDestellos == 0)
            cantDestellos = 1;

        float paso = 1 / (float)cantDestellos;
        int i = 0;        
        do
        {
            if (i * paso < t)
            {
                i++;
                StartCoroutine(Destello(paso));
            }
            t += Time.deltaTime / x;

            yield return null;
        } while (t < 1);
    }

    IEnumerator Destello(float x)
    {
        Debug.Log("DESTELLO");
        float t = 0;
        do
        {
            t += Time.deltaTime / x;

            render.color = Color.Lerp(cBrillo, cBajo, t);

            if (destellarConEscala)
            {
                Vector3 aux = Mathf.Lerp(escalaMax, escalaMin, t) * Vector3.one;
                gameObject.transform.localScale = aux;
            }

            yield return null;
        } while (t < 1);

        render.color = cBajo;
    }

    [Button]
    public void StopAll()
    {
        StopAllCoroutines();
    }

    public void Desactivar()
    {
        if (rutina != null) StopCoroutine(rutina);
    }

    public void OnDestroy()
    {
        if (rutina != null) StopCoroutine(rutina);
    }

    public void Posicionar(Vector3 pos)
    {
        StopAllCoroutines();
        gameObject.transform.position = pos;
    }
}
