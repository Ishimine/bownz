﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolGradual : MonoBehaviour {

    /// <summary>
    /// Cantidad de objetos en el pool
    /// </summary>
    public int cant = 10;


    public bool usarActivacionGradual = false;


    public int pasoMaximo;
    public int pasoActual;


    public int idActual;

    public GameObject item;
    public IItemSpawneable[] iSPpool;
    GameObject[] gOPool;

    public void Awake()
    {
        gOPool = new GameObject[cant];
        iSPpool = new IItemSpawneable[cant];
        for (int i = 0; i < cant; i++)
        {
            GameObject clone = Instantiate<Object>(item, transform) as GameObject;
            gOPool[i] = clone;
            iSPpool[i] = (IItemSpawneable)gOPool[i].GetComponent(typeof(IItemSpawneable));
            gOPool[i].SetActive(false);
        }
    }

    public void DesactivarAll()
    {
        foreach(IItemSpawneable i in iSPpool)
        {
            i.Desactivar();
        }
    }
    public void Resetear()
    {

        pasoActual = 0;
    }


    public void Siguiente()
    {
        //Si ya estamos en el paso maximo no hacemos nada
        if (pasoActual >= pasoMaximo) return;


        //Calculamos el id a desbloquear ahora
        int idActual = (int)Mathf.Lerp(0,cant, (float)pasoActual / (float)pasoMaximo);
        pasoActual++;

        //Chequeamos que no haya sido desbloqueado antes
        if (iSPpool[idActual].IsActive()) return;

        //Si no esta activado lo activamos y sumamos un paso
        iSPpool[idActual].Activar();
    }




}
