﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarcadorSimple : MonoBehaviour, IMarcadorSpawn
{

    public virtual void Activar(float t)
    {
    }

    public virtual void Desactivar()
    {
    }

    public virtual void OnDestroy()
    {
    }

    public virtual void Posicionar(Vector3 pos)
    {
        transform.position = pos;
    }

    public virtual void SetActive(bool isActive)
    {
        gameObject.SetActive(isActive);
    }

    public void SetParent(Transform parent)
    {
        transform.SetParent(parent);
    }
    
}
