﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Sirenix.OdinInspector;

public class SlowMotion_Proxy : SerializedMonoBehaviour {

    public float duracion = 4f;


    public float timeScaleSlow;


    IEnumerator rutina;

    [Button]
	public void Activar()
    {
        //Debug.LogError("sssssssssss");
        if (rutina != null) StopCoroutine(rutina);
        rutina = SlowMotion(duracion);
        StartCoroutine(rutina);
    }


    IEnumerator SlowMotion(float duracion)
    {
        yield return new WaitForSeconds(.1f);
        float timeScaleOrig = Time.timeScale;

        if (timeScaleOrig < 1) timeScaleOrig = 1;
        float t = 0;
        do
        {
            t += Time.unscaledDeltaTime / duracion;

            Debug.Log("Mathf.Lerp(timeScaleOrig, timeScaleSlow, Mathf.Sin(Mathf.PI * t) = " + Mathf.Lerp(timeScaleOrig, timeScaleSlow, Mathf.Sin(Mathf.PI * t)));

            Time.timeScale = Mathf.Lerp(timeScaleOrig, timeScaleSlow, Mathf.Sin(Mathf.PI * t));

            yield return null;
        } while (t < 1);
    }
}
