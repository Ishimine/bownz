﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Mina autonoma que no necesita control externo, continua reaparecienco constantemente
/// </summary>
public class Mina_Survival : Mina
{
    public bool usarMarcador = true;
    public IMarcadorSpawn marcador;

    public Vector3 posSig;
    public Vector3 posAct;

    public Vector2 areaSpawnAct;
    public Vector2 posSpawnAct;


    public RunTimeSet<GameObject>[] setDeSuperposicion;

    public float distMinimaEntreItems = 2.5f;


    /*  private void Awake()
      {
          ReAparecer();
      }*/

    private void Awake()
    {
        marcador.SetParent(null);
        marcador.SetActive(false);
    }

    public override void AutoDesactivacion()
    {
        ReActivar();
    }

    public override void Desactivar()
    {
        marcador.SetActive(false);
        base.Desactivar();
    }


    public void ReActivar()
    {
        Debug.Log("A");
        gameObject.SetActive(true);
        cuerpo.SetActive(true);
        col.enabled = true;
        CalcularPosSig();


        Debug.Log("B");

        //Activar y posicionar
        if (rutina != null) StopCoroutine(rutina);
        rutina = AnimIn();
        StartCoroutine(rutina);


        Debug.Log("C");

        transform.position = posAct;

        //Activamos y posicionamos marcador de item siguiente
        if (usarMarcador)
        {
            marcador.SetActive(true);
            Debug.Log("posSig:" + posSig);
            marcador.Posicionar(posSig);
            marcador.Activar(tiempoDeVida);
        }
        Debug.Log("D");
    }

    public override void Activar()
    {
        Debug.Log("A");
        gameObject.SetActive(true);
        col.enabled = false;
        CalcularPosSig();


        Debug.Log("B");

        //Activar y posicionar
        if (rutina != null) StopCoroutine(rutina);
        rutina = AnimIn();
        StartCoroutine(rutina);


        Debug.Log("C");

        transform.position = posAct;

        //Activamos y posicionamos marcador de item siguiente
        if (usarMarcador)
        {
            marcador.SetActive(true);
            Debug.Log("posSig:" + posSig);
            marcador.Posicionar(posSig);
            marcador.Activar(tiempoDeVida);
        }
        Debug.Log("D");

        cuerpo.SetActive(false);
    }


    public void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireCube(posSpawnAct, areaSpawnAct * 2);
    }




    public void CalcularPosSig()
    {
        posAct = posSig;

        int intento = 0;
        do
        {
            intento++;
            posSig = posSpawnAct + new Vector2(Random.Range(-areaSpawnAct.x, areaSpawnAct.x), Random.Range(-areaSpawnAct.y, areaSpawnAct.y));

        } while (intento <= 100 && !EsPosicionValida(posAct, posSig));

      
    }

    public bool EsPosicionValida(Vector3 actual, Vector3 siguiente)
    {
        if (EstaCerca(actual, siguiente))
        {
            Debug.LogWarning("                             POSICION INVALIDA:" + Vector2.Distance(actual, siguiente));
            return false;
        }

        foreach (RunTimeSet<GameObject> set in setDeSuperposicion)
        {
            for (int i = 0; i < set.Items.Count; i++)
            {
                if (EstaCerca(set.Items[i].transform.position, siguiente))
                {
                    Debug.LogWarning("                             POSICION INVALIDA:" + Vector2.Distance(set.Items[i].transform.position, siguiente));
                    return false;
                }
            }
        }
        Debug.Log("                             POSICION VALIDA, Distancia: " + Vector2.Distance(actual, siguiente));
        return true;
    }

    public bool EstaCerca(Vector3 actual, Vector3 siguiente)
    {
        return Vector2.Distance(actual, siguiente) < distMinimaEntreItems;
    }
}
