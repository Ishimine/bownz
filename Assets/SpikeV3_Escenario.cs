﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.Events;

public class SpikeV3_Escenario : SerializedMonoBehaviour {

    [Header("Sonidos")]
    public AudioSource aSource;
    public AudioClip puntoNormal;
    public AudioClip puntoEspecial;


    [Header("Prefabs")]
    public GameObject prefabSpike;
    public GameObject prefabTrigger;
    public SpriteRenderer prefabTriggerRender;


    [Header("Configuracion")]
    public Vector2 areaDeJuego;
    public Vector2 centroAreaDeJuego;



    [Header("Dependencias")]
    public SpikeV3_Pared[] paredes;
    
    [Header("Configuracion De Dependecias")]
    public int triggersPorPared;
    
    private Vector2 dimensionesPorPieza;
    [SerializeField] private VariableReference<float> triggerTamaño;
    public int TriggerTamaño
    {
        get { return (int)triggerTamaño.Value; }
    }

    [SerializeField] private VariableReference<float> triggerTamañoMin;
    public int TriggerTamañoMin
    {
        get
        {            
        return (int) triggerTamañoMin.Value;
        }
    }


    [Header("Animacion")]
    public float tTransicion;
    public float tSolapamiento = .1f;


    public UnityEvent respuestaNormal;
    public UnityEvent respuestaEspecial;

    [Header("Variables Compartidas")]
    [SerializeField] private VariableReference<Vector2> id_TriggerActivado;




    public RunTimeSet_GameObject contactosValidos;

    public VariableVector3 contactoTriggerPos;

    private void Awake()
    {
        InstanciarElementos();
    }


    public void Activar()
    {
        paredes[0].DesactivarPared();
        paredes[1].DesactivarPared();
        Siguiente();
    }


    public void TriggerActivado(bool especial, Vector3 point)
    {
        contactoTriggerPos.Value = point;
        TriggerActivado(especial);
    }

    public void TriggerActivado(bool especial)
    {
        if (especial)
            respuestaEspecial.Invoke();
        else
            respuestaNormal.Invoke();
    }


    public void Siguiente()
    {
        Debug.Log("SIGUIENTE");
        int pared = Random.Range(0,2);

        //int idTrigger = Random.Range(0, triggersPorPared);


        bool esEspecial = TriggerTamaño <= TriggerTamañoMin;

        paredes[pared].DesactivarPared();
        paredes[pared].ActivarPared(-1,TriggerTamaño,tTransicion, esEspecial);

    }



    [Button]
    public void CalcularElementosPorPared()
    {
        dimensionesPorPieza = prefabTriggerRender.sprite.bounds.size;

        triggersPorPared = (int)(areaDeJuego.y * 2 / dimensionesPorPieza.y);


        foreach(SpikeV3_Pared p in paredes)
        {
            p.triggersPorPared = triggersPorPared;
            p.dimensionesPorPieza = dimensionesPorPieza;
        }
    }

    
    [Button]
    public void CalcularAnchoAreaDeJuego()
    {
        areaDeJuego.x = Camera.main.orthographicSize * 2 * Camera.main.aspect / 2;
    }

    [Button]
    public void InstanciarElementos()
    {
        CalcularAnchoAreaDeJuego();
        CalcularElementosPorPared();

        paredes[1].transform.rotation = Quaternion.identity;

        paredes[0].transform.position = centroAreaDeJuego;
        paredes[1].transform.position = centroAreaDeJuego;


        paredes[0].Instanciar(triggersPorPared, dimensionesPorPieza);
        paredes[1].Instanciar(triggersPorPared, dimensionesPorPieza);


        paredes[0].transform.position = new Vector3(-areaDeJuego.x, centroAreaDeJuego.y);
        paredes[1].transform.position = new Vector3(areaDeJuego.x, centroAreaDeJuego.y);

        paredes[0].transform.rotation = Quaternion.identity;

        paredes[1].transform.rotation = Quaternion.Euler(0,0,180);
    }


    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(centroAreaDeJuego, areaDeJuego * 2);
    }



    [Button]
    public void DestruirHijos()
    {
        paredes[0].DestruirHijos();
        paredes[1].DestruirHijos();
    }



    public void ReproducirPuntoNormal()
    {
        aSource.clip = puntoNormal;
        aSource.Play();
    }

    public void ReproducirPuntoEspecial()
    {
        aSource.clip = puntoEspecial;
        aSource.Play();
    }

    public void ActivarTodo()
    {
        paredes[0].ActivarPared();
        paredes[1].ActivarPared();
    }

    public void DesactivarTodo()
    {
        paredes[0].ActivarPared();
        paredes[1].ActivarPared();
    }

}

