﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Pared_SpikeV2 : MonoBehaviour {

    public int idPared;
    public Escenario_SpikeV2 escenario;

    public bool esEspecial;
    public RunTimeSet<GameObject> contactosHabilitados;

    public UnityEventVector2 respuestaNormal;
    public UnityEventVector2 respuestaEspecial;

    public Trigger_Spike[] triggers;


    IEnumerator rutina;
    



    public void ActivarPared(bool especial)
    {
        for (int i = 0; i < triggers.GetLength(1); i++)
        {
            triggers[i].Activar(especial);
        }
    }


    void ActivarPared(int id, int tamañoTrigger, float segundos,  bool especial)
    {
        if (rutina != null) StopCoroutine(rutina);
        rutina = ActivarParedAnim(id, tamañoTrigger, segundos, especial);
        StartCoroutine(rutina);
    }


    IEnumerator ActivarParedAnim(int centro, int triggerTamaño, float segundos, bool especial)
    {

        float paso = segundos / triggerTamaño;
        triggers[centro].Activar(paso + escenario.tSolapamiento, especial);
        int aux = 1;
        yield return new WaitForSeconds(paso);

        for (int i = 1; i < triggers.GetLength(1); i++)
        {
            if (aux >= triggerTamaño) break;

            if (centro + i < triggers.GetLength(1))
            {
                triggers[centro + i].Activar(paso + escenario.tSolapamiento, especial);
                aux++;
            }
            if (aux >= triggerTamaño) break;
            if (centro - i >= 0)
            {
                triggers[centro - i].Activar(paso + escenario.tSolapamiento, especial);
                aux++;
            }
            yield return new WaitForSeconds(paso);
        }



    }
    

    public void TriggerActivado(int id)
    {

        DesactivarPared(id, escenario.tTransicion);
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if(contactosHabilitados.Contains(collision.gameObject))
        {
            TriggerActivado(GetIdPanelMasCercano(collision.contacts[0].point));
        }
    }


    public int GetIdPanelMasCercano(Vector3 posContacto)
    {
        float distMin = 100;

        int idMin = 0;

        foreach(Trigger_Spike t in triggers)
        {
            float dist = Vector3.Distance(t.transform.position, posContacto);

            if(dist < distMin)
            {
                distMin = dist;
                idMin = (int)t.id.y;
            }
        }
        return idMin;
    }

    

    public void DesactivarPared()
    {
        for (int i = 0; i < triggers.GetLength(1); i++)
        {
            triggers[i].Desactivar();
        }
    }

    public void DesactivarPared(int id, float tTransicion)
    {
        if (rutina != null) StopCoroutine(rutina);
        rutina = DesactivarParedAnim(id, tTransicion);
        StartCoroutine(rutina);
    }

    IEnumerator DesactivarParedAnim(int id, float tiempo)
    {
        float paso = tiempo / triggers.GetLength(1);

        for (int i = 0; i < triggers.GetLength(1); i++)
        {
            int pId = id + i;

            if (pId < triggers.GetLength(1))
            {
                triggers[pId].Desactivar(paso + escenario.tSolapamiento);
            }

            pId = id - i;
            if (pId >= 0)
            {
                triggers[pId].Desactivar(paso + escenario.tSolapamiento);
            }
            yield return new WaitForSeconds(paso);
        }
    }
}
