﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class SpikeV3_Pared : SerializedMonoBehaviour
{
    public bool esEspecial;
    public int idPared;
    public int triggersPorPared;


    public Transform tPivot;
    public Transform pPivot;

    public SpikeV3_Escenario escenario; 
    public GameObject[] pinches;
    public SpikeV3_Trigger[] triggers;

    
    public GameObject pinchePrefab;
    public GameObject triggerPrefab;
    public SpriteRenderer prefabTriggerRender;

    public Vector2 dimensionesPorPieza;


    [HideIf("hayEscenario")]
    public RunTimeSet_GameObject contactosValidos;

    public RunTimeSet_GameObject ContactosValidos
    {
        get
        {
            if (hayEscenario())
            {
                return  escenario.contactosValidos;
            }
            else
                return contactosValidos;
        }
    }



    [HideIf("hayEscenario")]
    public float tSolapamientoAnimacion;

    public float tSolapamiento
    {
        get
        {
            if (hayEscenario())
                return escenario.tSolapamiento;
            else
                return tSolapamientoAnimacion;
        }
        set
        {
            if (hayEscenario())
                escenario.tSolapamiento = value;
            else
                tSolapamientoAnimacion = value;
        }
    }


    [HideIf("hayEscenario")]
    public float tAnimacion;

    public float tTransicion
    {
        get
        {
            if (hayEscenario())
                return escenario.tTransicion;
            else
                return tAnimacion;
        }
        set
        {
            if (hayEscenario())
                escenario.tTransicion = value;
            else
                tAnimacion = value;
        }
    }

    /*[ShowIf("hayEscenario")]
    public int triggerTamaño;
    public int TriggerTamaño
    {        
        get
        {
            if (escenario != null)
                return escenario.TriggerTamaño;
            else
                return TriggerTamaño;
        }

    }*/


    IEnumerator rutina;


    public bool hayEscenario()
    {
        return escenario != null;
    }



    public void OnCollisionEnter2D(Collision2D collision)
    {
        if(ContactosValidos.Contains(collision.gameObject))
        {
            int id = GetIdPanelMasCercano(collision.contacts[0].point);

            DesactivarPared(id,tAnimacion);

            escenario.TriggerActivado(esEspecial, collision.contacts[0].point);
            
            //escenario.Siguiente();
        }
    }

        
    
    [Button]
    public void Instanciar()
    {
        DestruirHijos(tPivot);
        DestruirHijos(pPivot);

        triggers = new SpikeV3_Trigger[triggersPorPared];

        
        Vector3 spawnInicial = pPivot.position - Vector3.up * triggersPorPared * dimensionesPorPieza.y / 2 + Vector3.right * dimensionesPorPieza.x / 2;

        //Spawn de Pinches

        pinches = new GameObject[triggersPorPared + 1];
        for (int i = 0; i < triggersPorPared + 1; i++)
        {
            GameObject clone = Instantiate<GameObject>(pinchePrefab, pPivot);
            pinches[i] = clone;
            clone.transform.position = spawnInicial + Vector3.up * i *dimensionesPorPieza.y;
        }



         spawnInicial = tPivot.position - ((Vector3.up * triggersPorPared * dimensionesPorPieza.y) / 2) + Vector3.up * dimensionesPorPieza.y * .5f + Vector3.right * dimensionesPorPieza.x / 2;

        //Spawn de triggers
        for (int i = 0; i < triggersPorPared; i++)
        {
            GameObject clone = Instantiate<GameObject>(triggerPrefab);

            clone.transform.position = spawnInicial + Vector3.up * i * dimensionesPorPieza.y;

            clone.transform.rotation = Quaternion.Euler(0, 0, 180);

            triggers[i] = clone.GetComponent<SpikeV3_Trigger>();
            triggers[i].id = i;

            clone.transform.SetParent(tPivot);

            triggers[i].pinches = new SpikeV3_Pinche[2];

            triggers[i].pinches[0] = pinches[i].GetComponent<SpikeV3_Pinche>();
            triggers[i].pinches[1] = pinches[i+1].GetComponent<SpikeV3_Pinche>();

        }

    }

    public void Instanciar(int triggersPorPared, Vector2 dimensionesPorPieza)
    {
        this.triggersPorPared = triggersPorPared;
        this.dimensionesPorPieza = dimensionesPorPieza;
        Instanciar();
    }

    void DestruirHijos(Transform t)
    {
        for (int i = t.childCount - 1; i >= 0; i--)
        {
            if (Application.isPlaying)
            {
                Destroy(t.GetChild(i).gameObject);
            }
            else
            {
                DestroyImmediate(t.GetChild(i).gameObject);
            }
        }
    }

    public void DestruirHijos()
    {
        DestruirHijos(tPivot);
        DestruirHijos(pPivot);
    }

    public void ActivarPared()
    {
        ActivarPared(false);
    }

    public void ActivarPared(bool esEspecial)
    {
        foreach(SpikeV3_Trigger t in triggers)
        {
            t.Activar(esEspecial);
        }
    }

    public void ActivarPared(int id)
    {
        ActivarPared(id, triggersPorPared, tTransicion, false);
    }

    public void ActivarPared(int id, int cant, float segundos, bool esEspecial)
    {
        this.esEspecial = esEspecial;


        if (rutina != null) StopCoroutine(rutina);
        rutina = ActivarParedAnim(id, cant, segundos, esEspecial);
        StartCoroutine(rutina);

    }


    public void DesactivarPared()
    {
        foreach(SpikeV3_Trigger t in triggers)
        {
            t.Desactivar();
        }
    }

    public void DesactivarPared(int id, float segundos)
    {
        if (rutina != null) StopCoroutine(rutina);
        rutina = DesactivarParedAnim(id, segundos);
        StartCoroutine(rutina);


    }    

   

    IEnumerator DesactivarParedAnim(int centro, float tiempo)
    {

        float paso = tiempo / triggers.Length;
        

        for (int i = 0; i < triggers.Length; i++)
        {
            if (centro + i < triggers.Length)
            {
                triggers[centro + i].Desactivar(paso + escenario.tSolapamiento,true);
            }
            
            if (centro - i >= 0)
            {
                triggers[centro - i].Desactivar(paso + escenario.tSolapamiento, true);
            }
            yield return new WaitForSeconds(paso);
        }
    }    

    IEnumerator ActivarParedAnim(int id, int cant, float segundos, bool especial)
    {
        Debug.Log("Activando Pared ID: " + id);
        float paso = segundos / cant;

        

        int centro = id;

        if (id != -1)
            triggers[centro].Activar(paso + tSolapamiento, especial);
        else
            triggers[triggersPorPared / 2].Activar(paso + tSolapamiento, especial);

        
        int aux = 1;


        if (id != -1)
            yield return new WaitForSeconds(paso);
        else
            centro = triggersPorPared/2;

        for (int i = 1; i < triggers.Length; i++)
        {
            if (aux >= cant) break;

            if (centro + i < triggers.Length)
            {
                triggers[centro + i].Activar(paso + tSolapamiento, especial);
                aux++;
            }


            if (aux >= cant) break;
            if (centro - i >= 0)
            {
                triggers[centro - i].Activar(paso + tSolapamiento, especial);
                aux++;
            }

            if (id != -1)
                yield return new WaitForSeconds(paso);
        }
    }







    public int GetIdPanelMasCercano(Vector3 posContacto)
    {
        float distMin = 100;

        int idMin = 0;

        foreach (SpikeV3_Trigger t in triggers)
        {
            float dist = Vector3.Distance(t.transform.position, posContacto);

            if (dist < distMin)
            {
                distMin = dist;
                idMin = t.id;
            }
        }
        return idMin;
    }

}

