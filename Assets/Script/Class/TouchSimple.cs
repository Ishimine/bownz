﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

[Serializable]
public class TouchSimple  {

    public TouchPhase phase;
    public int fingerId;
    public Vector2 position;
    public Vector2 rawPosition;
    public Vector2 deltaPosition;
    public Vector2 worldPos;
    public float deltaTime;
    public int tapCount;

    public TouchSimple(TouchPhase phase, int fingerId,Vector2 position)
    {
        this.phase = phase;
        this.fingerId = fingerId;
        this.position = position;
        worldPos = GetWorldPosition();
    }
    public TouchSimple(Touch t)
    {
        fingerId = t.fingerId;
        position = t.position;
        rawPosition = t.rawPosition;
        deltaPosition = t.deltaPosition;
        deltaTime = t.deltaTime;
        tapCount = t.tapCount;
        phase = t.phase;
        worldPos = GetWorldPosition();
    }


    private Vector2 GetWorldPosition()
    {
        return Camera.main.ScreenToWorldPoint(position) - new Vector3(0f, 0f, Camera.main.transform.position.z);
    }

}
