﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_AdaptarARatioPantalla : MonoBehaviour {


    public void Start()
    {
        Vector2 t = new Vector2(12, 22);


        float y = Camera.main.orthographicSize * 2;
        Vector2 dimPantalla = new Vector2(y * Camera.main.aspect, y);
        transform.localScale = new Vector2(dimPantalla.x / t.x, dimPantalla.y / t.y);
        //Debug.Log(dimPantalla);
    //    Destroy(this);
    }
}
