﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_SetInactive : MonoBehaviour {

	public bool SetInactive
    {
        set { gameObject.SetActive(!value); }
    }
}
