﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_ScaleInOut : MonoBehaviour {

    

    public float scaleMax;
    public float vel = .3f;
    IEnumerator rutina;

    public bool animIn = false;
    public bool animOut = false;

    public void ScaleIn()
    {
        //if (animIn) return;
        if (rutina != null) StopCoroutine(rutina);
        rutina = In();
        StartCoroutine(rutina);
    }
    
    public void ScaleOut()
    {
       // if (animOut) return;
        if (rutina != null) StopCoroutine(rutina);
        rutina = Out();
        StartCoroutine(rutina);
    }


    private void OnDisable()
    {
        animIn = false;
        animOut = false;
        if (rutina != null) StopCoroutine(rutina);
    }

    


    IEnumerator In()
    {
        animIn = true;
        gameObject.SetActive(true);
        gameObject.transform.localScale = Vector3.zero;
        do
        {
            gameObject.transform.localScale += Vector3.one * Time.unscaledDeltaTime / vel;
            yield return null;
        } while (gameObject.transform.localScale.x < scaleMax);
        gameObject.transform.localScale = Vector3.one* scaleMax;

        animIn = false;
    }



    IEnumerator Out()
    {
        animOut = true;
        gameObject.transform.localScale = Vector3.one* scaleMax;
        do
        {
            gameObject.transform.localScale -= Vector3.one * Time.unscaledDeltaTime / vel;
            yield return null;
        } while (gameObject.transform.localScale.x > 0);
        gameObject.transform.localScale = Vector3.zero;
        gameObject.SetActive(false);
        animOut = false;
    }

}
