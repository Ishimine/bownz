﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_Wrap : MonoBehaviour {

    public bool hayBanner = true;
    static float alturaBanner = 1.5f;


    public bool activadoEnX = true;
    public bool activadoEnY = false;
    public Vector2 dimensionPantalla;
    float distExtra = .5f;


    public VariableDireccion direccion;

    public WrapVFX vfx;


    public GameEvent wrapEvent;

    //GameObject[] copias;

    void Start()
    {
        dimensionPantalla = new Vector2(Camera.main.orthographicSize / Screen.height * Screen.width + distExtra, Camera.main.orthographicSize);
    }   

    void LateUpdate()
    {
        if (activadoEnX && SalePorX())
        {
            DesplazarX(dimensionPantalla.x);
        }

        if (activadoEnY && SalePorY())
        {
            DesplazarY(dimensionPantalla.y);
        }
    }

    public bool SalePorX()
    {
        if ((gameObject.transform.position.x < (dimensionPantalla.x + distExtra/2)) && (gameObject.transform.position.x > (-dimensionPantalla.x - distExtra/2)))
            return false;
        else
            return true;
    }

    public bool SalePorY()
    {
        float extraBanner = 0;

        if (hayBanner)
            extraBanner = alturaBanner;




            if ((gameObject.transform.position.y < dimensionPantalla.y + distExtra/2 ) && (gameObject.transform.position.y > (-dimensionPantalla.y - distExtra/2) + extraBanner))
            return false;
        else
            return true;
    }

    void DesplazarX(float x)
    {
        if (vfx != null)        
            VfxIn();        

        transform.position += new Vector3((x * 2  * (-Mathf.Sign(transform.position.x))) + distExtra/2, 0);

        if (vfx != null)
            VfxOut();

        if (Mathf.Sign(transform.position.x) > 0)
            direccion.Value = Direccion.Izquierda_Derecha;
        else
            direccion.Value = Direccion.Derecha_Izquierda;


        if (wrapEvent != null) wrapEvent.Raise();
    }

    void DesplazarY(float x)
    {
        float extraBanner = 0;



        /* if (hayBanner && -Mathf.Sign(transform.position.y) < 0)
             extraBanner = -alturaBanner ;
         else if(hayBanner)
             extraBanner = alturaBanner ;*/

        if (hayBanner)
            extraBanner = alturaBanner;

        if (vfx != null)    
            VfxIn();

        transform.position += new Vector3(0,(x * 2 * (-Mathf.Sign(transform.position.y)) ) + distExtra/2 + extraBanner * Mathf.Sign(transform.position.y)) ;

        if (vfx != null)
            VfxOut();

        if (Mathf.Sign(transform.position.y) > 0)
            direccion.Value = Direccion.Abajo_Arriba;
        else
            direccion.Value = Direccion.Arriba_Abajo;

        if (wrapEvent != null) wrapEvent.Raise();

    }



    public void Desplazar(Vector2 dir)
    {
        transform.position += new Vector3(dimensionPantalla.x * Mathf.Sign(dir.x), dimensionPantalla.y * Mathf.Sign(dir.y));
    }



    public void VfxIn()
    {
        Vfx(WrapVFX.Portal.In);
    }

    public void VfxOut()
    {
        Vfx(WrapVFX.Portal.Out);
    }

    public void Vfx(WrapVFX.Portal p)
    {
        float angulo = 90;
        if (Mathf.Sign(transform.position.x) < 0) angulo = 270;
        vfx.Activar(transform.position, angulo, p);
    }
}
