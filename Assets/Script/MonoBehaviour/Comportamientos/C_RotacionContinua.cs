﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class C_RotacionContinua : SerializedMonoBehaviour {

    public VariableReference<float> vel;
    public float Vel
    {
        get { return vel.Value; }
    }


    private void Update()
    {
        transform.Rotate(Vector3.forward * Vel * Time.unscaledDeltaTime);
    }
}
