﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class C_TimeScaleController : SerializedMonoBehaviour {

    public bool usarLimite;
    [ShowIf("usarLimite")]
    [HideReferenceObjectPicker]
    public VariableReference<float> limite;
    [HideReferenceObjectPicker]
    public VariableReference<float> pasoMax;
    [HideReferenceObjectPicker]
    public VariableReference<float> pasoAct;
    [HideReferenceObjectPicker]
    public VariableReference<float> timeScaleBase;

    [HideReferenceObjectPicker]
    public VariableReference<float> timeScaleActual;


    public void Reiniciar()
    {
        Time.timeScale = timeScaleBase.Value;
        timeScaleActual.Value = Time.timeScale;
    }

    public void Aumentar(float x)
    {
        Time.timeScale = Time.timeScale + x;

        Debug.Log("TimeScale actual = " + Time.timeScale);
        timeScaleActual.Value = Time.timeScale;
    }

    public void AumentarPaso()
    {
        if (pasoAct.Value >= pasoMax.Value) return;
        pasoAct.Value++;
    //    Debug.Log(" (pasoAct.Value / pasoMax.Value) * (limite.Value - timeScaleBase.Value)  = " + (pasoAct.Value / pasoMax.Value) * (limite.Value - timeScaleBase.Value));
  //      Debug.Log(" (pasoAct.Value / pasoMax.Value)  = " + (pasoAct.Value / pasoMax.Value) );
//        Debug.Log(" ((limite.Value - timeScaleBase.Value)  = " +  (limite.Value - timeScaleBase.Value));



        Time.timeScale = ((pasoAct.Value / pasoMax.Value) * (limite.Value - timeScaleBase.Value)) + timeScaleBase.Value;

        timeScaleActual.Value = Time.timeScale;
        //Debug.Log("TimeScale: " + Time.timeScale);
    }


    public void ReducirPaso()
    {
        if (pasoAct.Value >= pasoMax.Value) return;
        pasoAct.Value--;
        Time.timeScale = ((pasoAct.Value / pasoMax.Value) * (limite.Value - timeScaleBase.Value)) + timeScaleBase.Value;
        timeScaleActual.Value = Time.timeScale;
        Debug.Log("TimeScale: " + Time.timeScale);
    }

    public void Reducir(float x)
    {
        Time.timeScale = Time.timeScale - x;
        if (Time.timeScale < 0)
            Time.timeScale = 0;
        timeScaleActual.Value = Time.timeScale;
    }


    public void Set(float x)
    {
        Time.timeScale = x;
        timeScaleActual.Value = Time.timeScale;
    }

    private void ChequearLimite()
    {
        if (usarLimite && Time.timeScale > limite.Value)
            Time.timeScale = limite.Value;
        timeScaleActual.Value = Time.timeScale;
    }

}
