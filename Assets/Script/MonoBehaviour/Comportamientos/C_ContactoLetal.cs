﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class C_ContactoLetal : MonoBehaviour {

    public void OnCollisionEnter2D(Collision2D collision)
    {
        PelotaJugador player = collision.gameObject.GetComponent<PelotaJugador>();
        if(player != null)
        {
            player.Destruir();
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        PelotaJugador player = collision.gameObject.GetComponent<PelotaJugador>();
        if (player != null)
        {
            player.Destruir();
        }
    }

}
