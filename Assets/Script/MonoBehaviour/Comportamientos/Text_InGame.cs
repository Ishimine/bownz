﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[RequireComponent(typeof(Mesh))]
public class Text_InGame : SerializedMonoBehaviour {
    [HideReferenceObjectPicker]
    public VariableReference<float> txt;
    public TextMesh mesh;

    public void Actualizar()
    {
        if (txt.Value == 0 && !Game_Cycle.EnJuego)
        {
            return;
        }
        mesh.text = txt.Value.ToString("0");
    }

    private void OnEnable()
    {
        Debug.Log("Text in game ENABLE  ---------------------------------");
    }

    private void OnDisable()
    {
        Debug.Log("Text in game DISABLE  ---------------------------------");
        
    }

}
