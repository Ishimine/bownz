﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[RequireComponent(typeof(TextMesh))]
public class TextMesh_PopUp : SerializedMonoBehaviour
{

    
    public bool usarColorCompartido;

    [ShowIf("usarColorCompartido")]
    public VariableColor colorCompartido;


    public TextMesh txt;
    private IEnumerator rutina;
    Color colorOrig = Color.white;


    public bool sobreEscribirTexto;

    [ShowIf("sobreEscribirTexto")]
    public VariableReference<string> newText;


    [Header("Animacion")]
    /// <summary>
    /// Velocidad de la animacion en segundos
    /// <summary>
    public float vel = 1.5f;
    /// <summary>
    /// altura de "salto" del texto
    /// </summary>
    public float altura = 1;

    public float caida = 2;

    [SerializeField]private bool conCaida;
    public bool ConCaida
    {
        get { return conCaida; }
        set { conCaida = value; }
    }



    #region Activar + SobreCargas
    public void Activar(Vector3 pos)
    {

        transform.position = pos;
        Animacion(pos,conCaida);
    }

    [Button]
    public void Activar()
    {
        Activar(transform.position);        
    }

    public void Activar(string text)
    {
        txt.text = text;
        Activar();
    }

    public void Activar(Vector3 pos, string txt)
    {
        this.txt.text = txt;
        Activar(pos);
    }

    #endregion

    private void Awake()
    {
        colorOrig = txt.color;
        txt.color = Color.clear;
    }


    public void OnValidate()
    {
        if (txt == null)
        {
            txt = GetComponent<TextMesh>();
        }
    }


    private void Animacion (Vector3 pos, bool conCaida)
    {

        if (sobreEscribirTexto)
            txt.text = newText.Value;

        if (usarColorCompartido)
            colorOrig = colorCompartido.Value;
        
        if (rutina != null) StopCoroutine(rutina);
        if (conCaida)
            rutina = PopAnimSalto(true);
        else
            rutina = PopAnimSalto(false);
        StartCoroutine(rutina);
    }
        

    IEnumerator PopAnimSalto(bool conCaida)
    {
        txt.color = colorOrig;
        float a = 0;
        Vector3 posIni = transform.position;
        Vector3 extra = Vector3.up * altura;
        float velFin = vel / 2;
        do
        {
            a += Time.deltaTime / velFin;
            transform.position = extra * Mathf.Sin(a) + posIni;

            if (!conCaida)
                txt.color = Color.Lerp(colorOrig, Color.clear, a / (Mathf.PI/2));
            yield return null;
        } while (a < Mathf.PI/2);
        if (conCaida)
        {
            if (rutina != null)
                StopCoroutine(rutina);
            rutina = PopAnimCaida();
            StartCoroutine(rutina);
        }
    }

    IEnumerator PopAnimCaida()
    {
        Vector3 posIni = transform.position - Vector3.up * caida;
        float a = Mathf.PI/2;
        Vector3 extra = Vector3.up * caida;
        float velFin = vel / 2;
        do
        {
            a += Time.deltaTime / velFin;
            transform.position = extra * Mathf.Sin(a) + posIni;
            txt.color = Color.Lerp(colorOrig, Color.clear, a / Mathf.PI);
            yield return null;
        } while (a < Mathf.PI);
    }
    
    private void OnDisable()
    {
        if (rutina != null)
            StopCoroutine(rutina);
    }
}
