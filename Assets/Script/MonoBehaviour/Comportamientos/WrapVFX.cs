﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WrapVFX : MonoBehaviour {

	public enum Direccion { arriba, abajo, izquierda,derecha}
    public enum Portal { In,Out}

    public ParticleSystem pIn;
    public GameObject gIn;
    public ParticleSystem pOut;
    public GameObject gOut;

    public void Activar(Vector3 pos, float angulo, Portal pId)
    {
        Debug.Log("ACTIVADO");
        GameObject portal = GetPortalGO(pId);
        ParticleSystem particulas = GetParticulas(pId);


        portal.transform.position = pos;
        portal.transform.rotation = Quaternion.Euler(Vector3.forward * (angulo /*+ GetOrientacion(pId)*/));
        particulas.Play();
    }

    public float GetOrientacion(Portal pId)
    {
        if (pId == Portal.In)
            return 0;
        else
            return 180;
    }
    public GameObject GetPortalGO(Portal pId)
    {
        if (pId == 0)
            return gIn;
        else
            return gOut;
    }

    public ParticleSystem GetParticulas(Portal pId)
    {
        if (pId == 0)
            return pIn;
        else
            return pOut;
    }

    public void Activar(Vector3 pos,Direccion dir, Portal pId)
    {
        switch (dir)
        {
            case Direccion.arriba:
                Activar(pos, (float)0, pId);
                break;
            case Direccion.abajo:
                Activar(pos, (float)90, pId);
                break;
            case Direccion.izquierda:
                Activar(pos, (float)135, pId);
                break;
            case Direccion.derecha:
                Activar(pos, (float)45, pId);
                break;
        }

    }



}
