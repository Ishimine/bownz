﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class C_Resplandor_TextMEsh : SerializedMonoBehaviour {

    public TextMesh render;
    public float duracion;

    IEnumerator rutina;

    public Color cResplandor;
    public Color cOrig;

    private void Awake()
    {
        cOrig = render.color;
    }

    [Button]
    public void Activar()
    {
        if (rutina != null) StopCoroutine(rutina);
        rutina = Resplandor();
        StartCoroutine(rutina);
    }



    public IEnumerator Resplandor()
    {
        float a = 0;
         cOrig = render.color;

        render.color = cResplandor;
        do
        {
            a += Time.deltaTime/ duracion;

            render.color = Color.Lerp(cResplandor, cOrig, a);
            


            yield return null;
        } while (a < 1);

        render.color = cOrig;

    }


}
