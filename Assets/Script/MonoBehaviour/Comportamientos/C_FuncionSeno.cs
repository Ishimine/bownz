﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_FuncionSeno : MonoBehaviour
{

    public VariableReference<float> xValue;
    public float XValue
    {
        get { return xValue.Value; }
        set { xValue.Value = value; }
    }

    public VariableReference<float> frecuencia;
    public float Frecuencia
    {
        get { return frecuencia.Value; }
    }

    public VariableReference<float> magnitud;
    public float Magnitud
    {
        get {return  magnitud.Value; }
    }
    
    public void Update()
    {
        XValue = Mathf.Sin(Time.deltaTime / Frecuencia) * Magnitud;
    }

}
