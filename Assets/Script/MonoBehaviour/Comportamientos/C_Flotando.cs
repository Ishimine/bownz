﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_Flotando : MonoBehaviour {
    

    public bool usarEnPausa = true;
    public bool usarEnJuego = false;


    public float magnitud = 10;
    public float velocidad = 13;

    public Vector3 lastMove;
	
	// Update is called once per frame
	void Update ()
    {
        if (Time.timeScale == 0 && usarEnPausa)
        {
            Vector3 a = Vector3.up * (Mathf.Sin(Time.realtimeSinceStartup * velocidad) / 25) * magnitud;
            transform.position = transform.position - lastMove + a;
            lastMove = a;
        }
        else if (usarEnJuego)
        {
            Vector3 a = Vector3.up * (Mathf.Sin(Time.realtimeSinceStartup * velocidad) / 25) * magnitud * Time.timeScale;
            transform.position = transform.position - lastMove + a;
            lastMove = a;
        }
    }
}
