﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class C_Parpadeo : SerializedMonoBehaviour {

    public bool activado = true;
    
    public bool Activado
    {
        get { return activado; }
        set { activado = value; }
    }

    public bool Desactivado
    {
        get { return !activado; }
        set { activado = !value; }
    }

    public bool esUI;

    [HideIf("esUI")]
    public SpriteRenderer renderSprite;
    [ShowIf("esUI")]
    public Image renderImage;

    public float frecuencia = 2;
    public float offset = 2;

    public bool suavisado;


    private void Awake()
    {
        if(renderSprite == null && renderImage == null)
        {
            Debug.LogError("Falta render en :" + gameObject.name);
        }
    }

    private void OnValidate()
    {
        if (renderSprite == null && renderImage == null)
        {
            BuscarRender();
        }
    }

    public void BuscarRender()
    {
        if (!esUI)
        {
            renderSprite = GetComponent<SpriteRenderer>();
        }
        else
        {
            renderImage = GetComponent<Image>();
        }
    }

    void Update()
    {
        if (Desactivado) return;

        float x = 0;



        if (suavisado)
        {
            x = Mathf.PingPong(Time.realtimeSinceStartup * frecuencia + offset, 1);
        }
        else
        {
            x = Time.realtimeSinceStartup % frecuencia + offset;
            if (x > .5f)
                x = 1;
            else
                x = 0;

        }
        if (!esUI)
        {
            Color c = new Color(renderSprite.color.r, renderSprite.color.g, renderSprite.color.b, x);
            renderSprite.color = c;
        }
        else
        {
            Color c = new Color(renderImage.color.r, renderImage.color.g, renderImage.color.b, x);
            renderImage.color = c;
        }
    }

    
}
