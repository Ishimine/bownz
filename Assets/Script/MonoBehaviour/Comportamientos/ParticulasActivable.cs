﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class ParticulasActivable : MonoBehaviour {

    public ParticleSystem particulas;

    public ConfiguracionGrafica config;


    private void OnValidate()
    {
        if(particulas == null)        particulas = GetComponent<ParticleSystem>();
    }

    [Button]
    public void Activar()
    {
        if(config.UsarParticulas)
            particulas.Play();
    }

    public void Activar(Vector3 pos)
    {
        if (!config.UsarParticulas) return;
        transform.position = pos; 
        Activar();
    }
}
