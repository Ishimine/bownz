﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class C_Resplandor_Text_UI : SerializedMonoBehaviour {


    public Text txt;

    IEnumerator rutina;
    public float duracion = .4f;
    public bool usarColorExterno = false;

    [HideIf("usarColorExterno")]
    public Color cResplandor;

    public Color cResplandorActivo
    {
        get
        {
            if (usarColorExterno)
            {
                return cResplandorExterno.Value;
            }
            else
            {
                return cResplandor;
            }
        }
    }

    [ShowIf("usarColorExterno")]
    public VariableColor cResplandorExterno;

    public Color cOrig;

    public void Activar()
    {
        if (rutina != null) StopCoroutine(rutina);
        rutina = Resplandor();
        StartCoroutine(rutina);
    }


    private void Awake()
    {
        cOrig = txt.color;
    }

    public IEnumerator Resplandor()
    {
        float a = 0;
        //cOrig = render.color;

        txt.color = cResplandorActivo;
        do
        {
            a += Time.unscaledDeltaTime / duracion;
            txt.color = Color.Lerp(cResplandorActivo, cOrig, a);

            yield return null;
        } while (a < 1);

        txt.color = cOrig;
    }

}
