﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class C_Resplandor : MonoBehaviour {

    public SpriteRenderer render;

    IEnumerator rutina;
    public float duracion = .4f;
    public Color cResplandor;
    public Color cOrig;

    public void Activar()
    {
        if (rutina != null) StopCoroutine(rutina);
        rutina = Resplandor();
        StartCoroutine(rutina);
    }


    public void LeerNuevoColor()
    {
        cOrig = render.color;
    }

    private void Awake()
    {
        LeerNuevoColor();
    }

    public IEnumerator Resplandor()
    {
        float a = 0;
        //cOrig = render.color;

        render.color = cResplandor;
        do
        {
            a += Time.unscaledDeltaTime / duracion;
            render.color = Color.Lerp(cResplandor, cOrig, a);

            yield return null;
        } while (a < 1);

        render.color = cOrig;
    }


}
