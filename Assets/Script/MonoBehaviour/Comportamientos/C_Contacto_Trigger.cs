﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
//using Sirenix.OdinInspector;


[RequireComponent(typeof(Collider2D),typeof(UnityEvent))]

public class C_Contacto_Trigger : MonoBehaviour {

    public UnityEvent respuesta;
    public RunTimeSet_GameObject listaActivadores;


    private void OnValidate()
    {
        if (gameObject.GetComponent<Collider2D>() == null) Debug.LogWarning("Falta collider 2d en " + gameObject.name);
    }


    private void OnCollisionExit2D(Collision2D collision)
    {
        if(respuesta != null && listaActivadores.Contains(collision.gameObject))
        {
            respuesta.Invoke();
        }       
    }


    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (respuesta != null && listaActivadores.Contains(collision.gameObject))
        {
            respuesta.Invoke();
        }
    }


}
