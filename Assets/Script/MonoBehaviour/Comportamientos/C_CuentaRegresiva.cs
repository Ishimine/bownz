﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.Events;

public class C_CuentaRegresiva : SerializedMonoBehaviour
{

    [HideReferenceObjectPicker]
    public VariableReference<float> tiempoIni;
    [HideReferenceObjectPicker]
    public VariableReference<float> tiempoAct;
    public GameEvent eventoActivable;

    public UnityEvent evento;
    
    public void IniciarCuentaRegresiva()
    {
        StopAllCoroutines();
        StartCoroutine(CuentaRegresiva());
    }

    IEnumerator CuentaRegresiva()
    {
        tiempoAct.Value = tiempoIni.Value;
        do
        {
            tiempoAct.Value -= Time.deltaTime;
            yield return null;
        } while (tiempoAct.Value > 0);

        ActivarEvento();
        StartCoroutine(CuentaRegresiva());

    }

    public void Detener()
    {
        StopAllCoroutines();
    }

    public void ActivarEvento()
    {
        if (eventoActivable != null) eventoActivable.Raise();
        if (evento != null) evento.Invoke();
    }


}
