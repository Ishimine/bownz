﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotacionActivos : MonoBehaviour {

    public GameObject[] objetos;
    public int actual;

    public void Siguiente()
    {
        objetos[actual%objetos.Length].SetActive(false);
        actual++;
        objetos[actual % objetos.Length].SetActive(true);
    }
}
