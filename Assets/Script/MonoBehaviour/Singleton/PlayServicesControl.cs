﻿using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using GooglePlayGames.OurUtils;
using System.Collections;
using System.Collections.Generic;


public class PlayServicesControl : MonoBehaviour {


    //public static PlayServicesControl instance;
    private static IPlayGamesClient mClient = null;

    public static PlayServicesControl instance;




    public GameEvent get_LeaderBoard_Iniciado;
    public GameEvent get_LeaderBoard_Finalizado;

    public GameEvent logInEvent;

    public static LeaderboardScoreData extractedData;

    IEnumerator rutina;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
            Destroy(this.gameObject);

        if (Social.localUser.authenticated) return;

            PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
            .Build();
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();

        SignIn();
        
    }

    

    public static void SignOut()
    {
        PlayGamesPlatform.Instance.SignOut();
    }

    static public void SignIn()
    {
        //if(Network.cone)

        Social.localUser.Authenticate(success => {

            if(success)
            {
                Debug.Log("Login EXITOSO");
                instance.logInEvent.Raise();
       // DebugEnPantalla.instance.CambiarTexto("Login EXITOSO");
            }
            else
            {
                Debug.Log("Login FALLIDO");
        //DebugEnPantalla.instance.CambiarTexto("Login FALLIDO");
            }
        });
    }


    #region Achivements
    public static  void UnlockAchivement(string id)
    {
        Social.ReportProgress(id, 100, success => {
            if (success)
            {
                Debug.Log("UnlockAchivement EXITOSO");
            }
            else
            {
                Debug.Log("UnlockAchivement FALLIDO");
            }
        });

    }



    static public void IncrementAchivement(string id, int stepsToIncrement)
    {
      PlayGamesPlatform.Instance.IncrementAchievement(id, stepsToIncrement, success => {
            if (success)
            {
                Debug.Log("IncrementAchivement EXITOSO");
            }
            else
            {
                Debug.Log("IncrementAchivement FALLIDO");
            }
        });
    
    }


    static public void ShowAchivementUI()
    {
       // DebugEnPantalla.instance.CambiarTexto("AchivementsUi");
        Social.ShowAchievementsUI();
    }

    #endregion

    #region LeaderBoard
    
      

    static public string GetPlayersIDs(string idTablaPuntaje, LeaderboardTimeSpan timeSpan)
    {
        string txt = "";

        foreach (IScore s in extractedData.Scores)
        {
            txt += s.userID + " " + s.formattedValue;
        }

        /* PlayGamesPlatform.Instance.LoadScores(idTablaPuntaje, 
             LeaderboardStart.TopScores, 
             1, 
             LeaderboardCollection.Public,
             timeSpan,
             (data) =>
             {

             }
             );*/


        return txt;
    }

    static public void GetScores(string idTablaPuntaje, LeaderboardTimeSpan timeSpan)
    {
        if(instance.get_LeaderBoard_Iniciado != null)
            instance.get_LeaderBoard_Iniciado.Raise();

        PlayGamesPlatform.Instance.LoadScores(idTablaPuntaje,
            LeaderboardStart.TopScores,
            1,
            LeaderboardCollection.Public,
            timeSpan,
            (LeaderboardScoreData data) =>
            {
                extractedData = data;

                if (instance.get_LeaderBoard_Finalizado != null)
                    instance.get_LeaderBoard_Finalizado.Raise();
            });
    }

    static public IScore GetFirstPlace(string idTablaPuntaje, LeaderboardTimeSpan timeSpan)
    {
        IScore firstPlace = null;

        PlayGamesPlatform.Instance.LoadScores(idTablaPuntaje,
            LeaderboardStart.TopScores,
            1,
            LeaderboardCollection.Public,
            timeSpan,
            (LeaderboardScoreData data) =>
            {
                Debug.Log(data.Valid);
                Debug.Log(data.Id);
                Debug.Log(data.PlayerScore);
                Debug.Log(data.PlayerScore.userID);
                Debug.Log(data.PlayerScore.formattedValue);
                extractedData = data;
                firstPlace = data.PlayerScore;
            });
        return firstPlace;
    }


    static public IScore GetNextPlace(string idTablaPuntaje, LeaderboardTimeSpan timeSpan)
    {
        IScore nextPlace = null;
        PlayGamesPlatform.Instance.LoadScores(idTablaPuntaje,
            LeaderboardStart.PlayerCentered,
            1,
            LeaderboardCollection.Public,
            timeSpan,
            (LeaderboardScoreData data) =>
            {
                Debug.Log(data.Valid);
                Debug.Log(data.Id);
                Debug.Log(data.PlayerScore);
                Debug.Log(data.PlayerScore.userID);
                Debug.Log(data.PlayerScore.formattedValue);
                nextPlace = data.PlayerScore;
            });
        return nextPlace;
    }


    public string TestGetList(string idTablaPuntaje, LeaderboardTimeSpan timeSpan)
    {
        LeaderboardScoreData d = null;
        string mStatus = "invalid";


        PlayGamesPlatform.Instance.LoadScores(idTablaPuntaje,
            LeaderboardStart.PlayerCentered,
            20,
            LeaderboardCollection.Public,
            timeSpan,
            (data) =>
            {
                Debug.Log(data.Valid);
                Debug.Log(data.Id);
                Debug.Log(data.PlayerScore);
                Debug.Log(data.PlayerScore.userID);
                Debug.Log(data.PlayerScore.formattedValue);

                d = data;
            });


        mStatus = "Leaderboard data valid: " + d.Valid;
        mStatus += "\n approx:" + d.ApproximateCount + " have " + d.Scores.Length;
        return mStatus;
    }

    /*void GetNextPage(LeaderboardScoreData data)
    {
        string mStatus = "invalid";
        PlayGamesPlatform.Instance.LoadMoreScores(data.NextPageToken, 10,
            (results) =>
            {
                mStatus = "Leaderboard data valid: " + data.Valid;
                mStatus += "\n approx:" + data.ApproximateCount + " have " + data.Scores.Length;
            });
        return mStatus;
    }*/

    /* static public IScore GetNext100Places(string idTablaPuntaje, LeaderboardTimeSpan timeSpan)
     {

     }*/



    static public string GetLeaderBoardStatus(string id,LeaderboardTimeSpan x)
    {
        string mStatus = "";
        if (!Social.localUser.authenticated) SignIn();

        PlayGamesPlatform.Activate().LoadScores(id,
            LeaderboardStart.PlayerCentered,
            50,
            LeaderboardCollection.Public,
            x,
            (LeaderboardScoreData data) =>
            {
                mStatus = "Leaderboard data valid: " + data.Valid;
                mStatus += "\n approx:" + data.ApproximateCount + " have " + data.Scores.Length;                
            });

        return mStatus;
    }

    static public void ShowLeaderBoard(string id)
    {
         if(!Social.localUser.authenticated) SignIn();
        ((PlayGamesPlatform)Social.Active).ShowLeaderboardUI(id);
    }



    static public void ShowLeaderBoard(string id, LeaderboardTimeSpan x)
    {
        if (!Social.localUser.authenticated) SignIn();
        ((PlayGamesPlatform)Social.Active).ShowLeaderboardUI(id, x);
    }


    static public void ShowLeaderBoards()
    {
        ((PlayGamesPlatform)Social.Active).ShowLeaderboardUI();        
    }
    
    static public void AddScoreToLeaderBoard(string id, long score)
    {
        ((PlayGamesPlatform)Social.Active).ReportScore(score, id, success => {
            if (success)
            {
                Debug.Log("AddScoreToLeaderBoard EXITOSO");
            }
            else
            {
                Debug.Log("AddScoreToLeaderBoard FALLIDO");
            }
        });

    }
    #endregion
    

    
    
  

}



