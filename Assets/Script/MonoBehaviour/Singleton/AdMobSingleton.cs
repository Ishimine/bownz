﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds;
using GoogleMobileAds.Api;
using Sirenix.OdinInspector;
using System;

public class AdMobSingleton : SerializedMonoBehaviour
{
    public bool isDebugBuild;


    public static AdMobSingleton instance;
    private BannerView bannerView;
    private RewardBasedVideoAd rewardBasedVideoAd;


    public string appIdAndroid;
    public string appIdIPhone;

    public string bannerAndroid = "";
    public string bannerIPhone = "";
    public string rVideoAndroid = "";
    public string rVideoIPhone = "";
    public string interstitialAndroid = "";
    public string interstitialIPhone = "";

    private string bannerAndroidTest = "ca-app-pub-3940256099942544/6300978111";
    private string rVideoAndroidTest = "ca-app-pub-3940256099942544/5224354917";
    private string interstitialAndroidTest = "ca-app-pub-3940256099942544/1033173712";  

    public string BannerAndroid
    {
        get { if (isDebugBuild)
                return bannerAndroidTest;
            else
                return bannerAndroid;
        }
    }
        
    public string RVideoAndroid
    {
        get
        {
            if (isDebugBuild)
                return rVideoAndroidTest;
            else
                return rVideoAndroid;
        }
    }

    public string InterstitialAndroidTest
    {
        get
        {
            if (isDebugBuild)
                return interstitialAndroidTest;
            else
                return interstitialAndroid;
        }
    }

    public float timepoEntreInterstitials;



    public GameEvent finDelJuego;
    public GameEvent videoRewardSucess;


    bool rewarded = false;


    void Awake()
    {
        if (instance == null)
        {
            instance = this;
#if UNITY_ANDROID
            string appID = appIdAndroid;
#elif UNITY_IPHONE
            string appId = appIdIPhone;
#else
            string appId = "unexpected_platform";
#endif
            MobileAds.Initialize(appID);
            this.rewardBasedVideoAd = RewardBasedVideoAd.Instance;




            rewardBasedVideoAd.OnAdClosed += HandleOnAdClosed;
            rewardBasedVideoAd.OnAdFailedToLoad += HandleOnAdFailedToLoad;
            rewardBasedVideoAd.OnAdLeavingApplication += HandleOnAdLeavingApplication;
            rewardBasedVideoAd.OnAdLoaded += HandleOnAdLoaded;
            rewardBasedVideoAd.OnAdOpening += HandleOnAdOpening;
            rewardBasedVideoAd.OnAdRewarded += HandleOnAdRewarded;
            rewardBasedVideoAd.OnAdStarted += HandleOnAdStarted;

            BannerInferior();


            LoadVideoReward();
        }
        else
            Destroy(this.gameObject);
    }

    public void ShowBanner()
    {
    }

    public void BannerInferior()
    {
        RequestBanner(AdPosition.Bottom);
    }

    public void BannerSuperior()
    {
        RequestBanner(AdPosition.Top);
        
    }



    public bool ShowVideoReward()
    {
        if (rewardBasedVideoAd.IsLoaded())
        {
            rewarded = false;
            rewardBasedVideoAd.Show();
            return true;
        }
        else
        {
            print("VideoReward no cargado aún...");
            return false;
        }
    }

    public void LoadVideoReward()
    {


        print("VideoReward Solicitado");
#if UNITY_EDITOR
        string adUnityId = "unused";
#elif UNITY_ANDROID
        string adUnityId = RVideoAndroid;
#elif UNITY_IPHONE
        string adUnityId = rVideoIPhone;
#else
        string adUnityId = "unused";
#endif
        //rewardBasedVideoAd.LoadAd(new AdRequest.Builder().Build(), adUnityId);

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the rewarded video ad with the request.
        this.rewardBasedVideoAd.LoadAd(request, adUnityId);
    }

    private void RequestBanner(AdPosition pos)
    {
#if UNITY_ANDROID
        string adUnitId = BannerAndroid;
#elif UNITY_IPHONE
            string adUnitId = bannerIPhone;
#else
            string adUnitId = "unexpected_platform";
#endif
        // Create a 320x50 banner at the top of the screen.
        bannerView = new BannerView(adUnitId,AdSize.Banner, pos);

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();

        // Load the banner with the request.
        bannerView.LoadAd(request);
    }



    public void HandleOnAdLoaded(object sender, EventArgs args)
    {
    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        LoadVideoReward();
    }

    public void HandleOnAdOpening(object sender, EventArgs args)
    {
        Time.timeScale = 0;
    }

    public void HandleOnAdStarted(object sender, EventArgs args)
    {
        //Mute sound
    }

    public void HandleOnAdClosed(object sender, EventArgs args)         //Se cerro la publicidad
    {
        if(rewarded)
        {
            videoRewardSucess.Raise();
        }
        else
        {
            finDelJuego.Raise();                //Se comunica evento de fin de juego
        }


        LoadVideoReward();
        //Encender Audio
    }

    public void HandleOnAdRewarded(object sender, EventArgs args)
    {
        MonoBehaviour.print("Rewarded Video Sucess");

        rewarded = true;


    }

    public void HandleOnAdLeavingApplication(object sender, EventArgs args)
    {
    }


    public bool IsVideoRewardReady()
    {
        return rewardBasedVideoAd.IsLoaded();
    }


    public IEnumerator InterstitialCuentaRegresiva()
    {
        yield return new WaitForSeconds(timepoEntreInterstitials);
    }



}
