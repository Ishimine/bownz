﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class UI_TextoReferencia : SerializedMonoBehaviour
{
    public bool actualizarOnEnable;
    public Text txt;

    public bool usarFloat;

    [ShowIf("usarFloat")]
    [HideReferenceObjectPicker]
    public VariableReference<float> numero;

    [HideIf("usarFloat")]
    [HideReferenceObjectPicker]
    public VariableReference<string> texto;


    public void OnEnable()
    {
        if (actualizarOnEnable)
            Actualizar();
    }

    [Button]
    public void Actualizar()
    {
       // Debug.Log("Actualizar: " + numero.Value);
        if(usarFloat)
        {
            if (numero.Value == 0)
                txt.text = "";
            else
                txt.text = numero.Value.ToString("");
        }
        else
        {
            txt.text = texto.Value;
        }
    }

}
