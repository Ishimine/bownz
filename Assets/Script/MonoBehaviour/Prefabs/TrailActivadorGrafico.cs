﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailActivadorGrafico : MonoBehaviour {

    public ParticleSystem trail;
    public ConfiguracionGrafica graficConfig;

    private void OnValidate()
    {
        if (trail == null) trail = GetComponent<ParticleSystem>();
    }

    public void ActualizarTrail()
    {
//        Debug.Log("Actualizar Trail");
        if(graficConfig.UsarTrail)
        {
            //          Debug.Log("Play Trail");
            //trail.Play();
            trail.gameObject.SetActive(false);
        }
        else
        {
    //        Debug.Log("Stop Trail");
          //  trail.Stop();
            trail.gameObject.SetActive(true);
        }
    }

    public void OnDisable()
    {
//        Debug.Log("Desabilitado");
    }
}
