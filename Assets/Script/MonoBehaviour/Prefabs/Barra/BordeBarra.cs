﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BordeBarra : MonoBehaviour {

	void Update ()
    {
        //transform.localScale = transform.parent.parent.localScale * 0.13f;
        Vector3 aux = new Vector3(transform.parent.parent.localScale.y / transform.parent.parent.localScale.x * .13f, transform.localScale.y);

        if (float.IsNaN(aux.x)) aux.x = 0;

        if ( !float.IsInfinity(aux.x) && !float.IsInfinity(aux.x))
        {
            transform.localScale = aux;
        }

    }
}
