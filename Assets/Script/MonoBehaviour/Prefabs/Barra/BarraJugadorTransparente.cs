﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarraJugadorTransparente : MonoBehaviour {

    public ConfiguracionBarras config;
    public BarraTouch barra;
    public Transform pivot;


    /// <summary>
    /// Si es TRUE, la esfera al hacer contacto con la transparencia dara por finalizado el proceso de creacion de barra.
    /// </summary>
    [SerializeField] private Collider2D col;


    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(config.usarContactoTransparente && collision.tag == "Player")
        {
            barra.ContactoTransparente();
        }
    }
}
