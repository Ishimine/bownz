﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

[Serializable]
public class BarraTouch : SerializedMonoBehaviour
{
    public ConfiguracionBarras config;
    public RecursoBarra recurso;
    public AudioClip sonidoIn;
    public AudioClip sonidoOut;
    public AudioClip sonidoImpacto;
    public AudioSource playerA;
    public AudioSource playerB;


    public bool vibrar = false;
    public float vel = 6;
    public float porcentajeCrecimientoX = .1f;
    public float porcentajeCrecimientoY = .2f;
    public SpriteRenderer renderFisico;

    public float dimActual;

    IEnumerator rutina;

    ContenedorBarras contenedor;



    [SerializeField] private bool activa;
    public bool Activa
    {
        get { return activa; }
    }

    [SerializeField] private bool enProduccion = false;
    public bool EnProduccion
    {
        get { return enProduccion; }
        set { enProduccion = value; }
    }


    [SerializeField] private int id;
    public int IdBarra
    {
        get { return id; }
        set { id = value; }
    }


  [SerializeField] private int idTouch = -1;
    public int IdTouch
    {
        get { return idTouch; }
        set { idTouch = value; }
    }

    public Vector2 pitchContacto = new Vector2(.8f,1.3f);

    public BarraJugadorFisica pFisica;
    public BarraJugadorTransparente pTransparente;

    

    public void LateUpdate()
    {
        if(vibrar && pTransparente.gameObject.activeSelf)
        {

            float y = Mathf.Abs((Mathf.Sin(Time.realtimeSinceStartup * vel) * porcentajeCrecimientoY));
            pTransparente.transform.localScale = new Vector3(pTransparente.transform.localScale.x, y + 1);
            if (config.usarVibracion && Vibration.isActive)
            {
                long x = (long)Mathf.Lerp(config.FVibracion[0], config.FVibracion[1], dimActual);
                Vibration.Vibrate(x);
//                Debug.Log(x);
            }
        }
    }

    public void SetContenedor(ContenedorBarras c)
    {
        contenedor = c;
    }
    
    public void SetTransparenteTransform(Vector2 pos, Quaternion rot, float longitud, float grosor)
    {
        SetTransparenteTransform(pos, rot);
        SetTransparenteLongitud(longitud);
        SetTransparenteGrosor(grosor);
    }

    public void SetTransparenteTransform(Vector2 pos, Quaternion rot)
    {
        SetTransparentePosicion(pos);
        SetTransparenteRotacion(rot);
    }

    public void SetTransparenteLongitud(float longitud)
    {
        pTransparente.pivot.transform.localScale = new Vector2(longitud, pTransparente.pivot.transform.localScale.y);
    }

    public void SetTransparentePosicion(Vector3 pos)
    {
        pTransparente.gameObject.SetActive(true);
        pTransparente.pivot.transform.position = pos;
    }

    public void SetTransparenteRotacion(Quaternion rot)
    {
        pTransparente.pivot.transform.rotation = rot;
    }

    public void SetTransparenteGrosor(float x)
    {
        pTransparente.pivot.transform.localScale = new Vector2(pTransparente.pivot.transform.localScale.x, x);
    }

    public void SetFisicoGrosor(float x)
    {
        pFisica.pivot.transform.localScale = new Vector2(pTransparente.pivot.transform.localScale.x, x);
    }




    public Quaternion GetRotTo(Vector2 target)
    {
       return Quaternion.Euler(0, 0, CalcularRotacionAyB(transform.position, target));
    }

    public Vector2 GetScaleTo(Vector2 target)
    {


        float dist = Vector2.Distance(transform.position, target);

        
        
        if (dist > config.longitudBarra.Value.y) dist = config.longitudBarra.Value.y;   //Chequeo Longitud Maxima

        if(EnProduccion)
        {
           if (config.usarRecurso)
            {
                dist = recurso.SolicitarDisponibilidadRecurso(dist);
            }
        }

        if(!EnProduccion && Activa)
        {
            if (dist < config.longitudBarra.Value.x)
            {
                Debug.Log("Longitud muy chica");
                return Vector2.zero;               //Chequeo Longitud Minima
            }
            else if(config.usarRecurso)
            {
                if(dist > recurso.actual.Value)
                {
                    dist = recurso.actual.Value;
                }                
                recurso.Consumir(dist);
            }

        }

        Vector2 dim = new Vector2(dist, Mathf.Lerp(config.grosorBarra.Value.x, config.grosorBarra.Value.y, dist / config.longitudBarra.Value.y));

        if (float.IsNaN(dim.y)) dim.y = 0;
        return dim;
    }
    


    public void TouchIn(Vector2 worldPos)
    {
        pTransparente.gameObject.SetActive(true);
        pFisica.gameObject.SetActive(false);
        transform.position = worldPos;
        pTransparente.transform.parent.localScale = Vector3.zero;
        pFisica.transform.parent.localScale = Vector3.zero;
        transform.localScale = Vector3.one;
        EnProduccion = true;
        activa = false;
    }

    public bool TouchMove(Vector2 worldPos)
    {
        pTransparente.transform.parent.rotation = GetRotTo(worldPos);
        pTransparente.transform.parent.localScale = GetScaleTo(worldPos);

        if (pTransparente.transform.parent.localScale.x / config.longitudBarra.Value.y > config.vibrarAPartirDe)
        { 
            vibrar = true;
            float maximo;

            if(config.usarEstiramientoFragil)
            {
                maximo = config.PuntoDeRuptura;
            }
            else
            {
                maximo = config.longitudBarra.Value.y;
            }
            float a = (config.longitudBarra.Value.y * config.vibrarAPartirDe);

            dimActual = (Vector2.Distance((Vector2)transform.position, worldPos) - a ) / (maximo - a);
//            Debug.Log("dimActual:" + dimActual);
        }
        else vibrar = false;



        if(config.usarEstiramientoFragil &&  Vector2.Distance(transform.position,worldPos) >= config.PuntoDeRuptura)
        {
            ContactoTransparente();
            TouchOut(worldPos);
            return true;
        }
        else
        {
            return false;
        }
    }


    public bool TouchOut(Vector2 worldPos)
    {
        pFisica.col.isTrigger = false;
        pFisica.gameObject.SetActive(false);
        pFisica.transform.localPosition = Vector3.right * .5f;
        EnProduccion = false;
        activa = true;
        pTransparente.gameObject.SetActive(false);

        pFisica.transform.parent.position = pTransparente.transform.parent.position;
        pFisica.transform.parent.rotation = GetRotTo(worldPos);
        pFisica.transform.parent.localScale = GetScaleTo(worldPos);
        //IdTouch = -1;

        pFisica.gameObject.SetActive(true);

        if (pFisica.transform.parent.localScale == Vector3.zero)
            return false;


        SonidoIn();     //Sonido despues de chequear si se dibujo o no

        if (config.usarTiempoPorBarra)
        {
            if (rutina != null) StopCoroutine(rutina);
            rutina = Esperar(config.tiempoDeVidaPorBarra.Value);
            StartCoroutine(rutina);
        }

//        Debug.Log("bbb");
        return true;

    }







    public void ContactoTransparente()
    {
        pTransparente.gameObject.SetActive(false);
        pFisica.gameObject.SetActive(true);

        pFisica.transform.parent.localScale = pTransparente.transform.parent.localScale;
        pFisica.transform.parent.rotation = pTransparente.transform.parent.rotation;

        
        EnProduccion = false;
        activa = true;
        SonidoIn();


        contenedor.ContactoTransparente(this);
        if (config.usarVibracion && Vibration.isActive) Vibration.Vibrate((long)50);

    }

    public void ContactoFisico(Vector3 dir)
    {
        if (config.desaparecerPorContacto)
        {
            if (rutina != null) StopCoroutine(rutina);
            pFisica.AnimarImpacto(dir);
            ContactoFisico();
        }
    }

    public void ContactoFisico()
    {
        if (config.desaparecerPorContacto)
        {
            SonidoImpacto();
            activa = false;
            contenedor.ContactoFisico(this);
        }
    }



    public void Deshabilitar()
    {
        if (rutina != null) StopCoroutine(rutina);
        activa = false;
        EnProduccion = false;

        pFisica.gameObject.SetActive(false);
        pTransparente.gameObject.SetActive(false);
        pTransparente.transform.localScale = Vector2.one;
        pFisica.transform.localScale = Vector2.one;
        
    }


    IEnumerator Esperar(float x)
    {
        yield return new WaitForSeconds(x);
        rutina = Reduccion();
        StartCoroutine(rutina);
    }


    IEnumerator Reduccion()
    {
        Color orig = pFisica.GetColor();
        Vector2 dimInicial = pFisica.transform.localScale;

        Vector2 dimReducida = dimInicial * .8f;
        float a = 0;
        float tiempo = .8f;


        Vector2 dim;
//        float m = 8;
      //  float d = 7 / m;
    //    bool s = true;


        do
        {
            a += Time.deltaTime / tiempo;
            dim = Vector2.Lerp(dimInicial, dimReducida, a);
            pFisica.transform.localScale = dim;
            pFisica.CambiarColor(Color.Lerp(orig, Color.white, a));
            yield return null;
        } while (a < 1);

        rutina = Desaparecer();
        StartCoroutine(rutina);
    }


    public void DesaparicionPrematura()
    {
        if (rutina != null) StopCoroutine(rutina);
        rutina = Desaparecer();
        StartCoroutine(rutina);
    }

    IEnumerator Desaparecer()
    {
        Debug.Log("Desaparecer");

       // Color orig = pFisica.GetColor();
        Vector2 dimInicial = pFisica.transform.localScale;
        Vector2 dimPuntoMedio = dimInicial * .8f;
        float a = 0;
        float tiempo = .2f;
        Vector2 dim;
        SonidoOut();
        do
        {
            a += Time.deltaTime / tiempo;
            dim = Vector2.Lerp(dimPuntoMedio, Vector2.zero, a);
            pFisica.transform.localScale = dim;
            yield return null;
        } while (a < 1);

        pFisica.ColorOriginal();
        pFisica.transform.localScale = Vector3.one;
        ContactoFisico();

        pFisica.gameObject.SetActive(false);
    }
    /*
    IEnumerator DesaparecerBarra()
    {
        Color orig = pFisica.GetColor();
        Vector2 dimInicial = pFisica.transform.localScale;
        Vector2 dimPuntoMedio = dimInicial * .8f;
        float a = 0;
        float tiempo = 1.2f;
        Vector2 dim;
        float m = 8;
        float d = 7/m;
        bool s = true;
        do
        {
            a += Time.unscaledDeltaTime / tiempo;

            if(a < (d))
            {
                dim = Vector2.Lerp(dimInicial, dimPuntoMedio, a * d);
            }
            else
            {
                if (s)
                {
                    SonidoOut();
                    s = false;
                }

                dim = Vector2.Lerp(dimPuntoMedio, Vector2.zero, (a - (d))*m);
            }
             //pTransparente.transform.localScale = dim;
            pFisica.transform.localScale = dim;

            pFisica.CambiarColor(Color.Lerp(orig, Color.white, a));
            yield return null;
        } while (a < 1);

        pFisica.CambiarColor(orig);
        pFisica.transform.localScale = Vector3.one;

        ContactoFisico();
    }
    */
    

    private void OnDisable()
    {
        if (rutina != null) StopCoroutine(rutina);
    }


    #region Sonido

    #endregion
    public void SonidoIn()
    {
        SonidoReproducir(sonidoIn, playerA);
    }
    public void SonidoOut()
    {
        SonidoReproducir(sonidoOut, playerA);
    }
    public void SonidoImpacto()
    {
        SonidoReproducir(sonidoImpacto, playerB);
    }

    public void SonidoReproducir(AudioClip clip, AudioSource player)
    {
        if (Time.timeScale == 0) return;

//        Debug.Log("Pitch = " + pFisica.transform.parent.localScale.x);


        float t = (pTransparente.transform.parent.localScale.x - config.longitudBarra.Value[1]) / (config.longitudBarra.Value[0] - config.longitudBarra.Value[1]);


        float pitchFinal = Mathf.Lerp(pitchContacto.y, pitchContacto.x, t) ;           //UnityEngine.Random.Range(.8f, 1.1f);

        player.pitch = pitchFinal;
        player.clip = clip;
        player.Play();
    }

    public void BarraPreparada()
    {
        pTransparente.transform.localScale = Vector2.one;
        pFisica.transform.localScale = Vector2.one;
    }



    public float CalcularRotacionAyB(Vector2 posA, Vector2 posB)
    {
        Vector2 dif = posB - posA;
        return Mathf.Atan2(dif.y, dif.x) * Mathf.Rad2Deg;
    }
}
