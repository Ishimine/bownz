﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarraJugadorFisica : MonoBehaviour {

    public BarraTouch barra;
    public Transform pivot;

    public Collider2D col;

    public SpriteRenderer[] renders;


    public float velFxImpacto = 10;
    public float fImpacto = 2;
    public float fCaida = 9.81f;

    public Color cOriginal;
    IEnumerator rutina;

    private void Start()
    {
        cOriginal = renders[0].color;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            barra.ContactoFisico(((Vector3)collision.contacts[0].point - collision.transform.position).normalized);
        }
    }


    public void AnimarAparicion()
    {
        rutina = Aparicion();
        StartCoroutine(rutina);
    }

    public void AnimarImpacto(Vector3 dir)
    {
        if (rutina != null) StopCoroutine(rutina);
        rutina = Impacto((dir));
        StartCoroutine(rutina);
    }

    public void AnimarDesaparicion()
    {
        rutina = Desaparicion();
        StartCoroutine(rutina);
    }

    IEnumerator Impacto(Vector3 dir)    
    {
        Vector3 escalaIni = transform.localScale;

        Vector3 posIni = transform.position;

//        Debug.Log("Impacto: " + dir);
        col.isTrigger = true;

        float f = fImpacto;

        foreach(SpriteRenderer r in renders)
        {
            r.color = Color.white;
        }



        float t = 0;
        float caida = 0;
        do
        {

            t += Time.deltaTime / velFxImpacto;
            f = (Time.deltaTime * Mathf.Lerp(fImpacto, 0, t));

            caida = t * fCaida;
            transform.position +=   (dir * f ) + (caida * Vector3.down);
            transform.localScale = Vector3.Lerp(escalaIni, Vector3.zero, t);

            //COLOR
            if (t <= .8f)
            {
                foreach (SpriteRenderer r in renders)
                {
                    r.color = Color.Lerp(Color.white, Color.clear, t / .8f);
                }
            }
            else
            {
                foreach (SpriteRenderer r in renders)
                {
                    r.color = Color.clear;
                }
            }

            yield return null;

        } while (t < 1);

        col.isTrigger = false;

        foreach (SpriteRenderer r in renders)
        {
            r.color = cOriginal;
        }
        transform.position = posIni;

        transform.localScale = Vector3.one;

        gameObject.SetActive(false);
    }

    IEnumerator Desaparicion()
    {
        yield return null;
    }

    IEnumerator Aparicion()
    {
        yield return null;
    }

    private void OnDestroy()
    {
        if (rutina != null) StopCoroutine(rutina);
    }


    public Color GetColor()
    {
     return   renders[0].color;
    }

    public void ColorOriginal()
    {
        renders[0].color = cOriginal;
    }


    public void CambiarColor(Color c)
    {
        foreach(SpriteRenderer r in renders)
        {
            r.color = c;
        }
    }
}
