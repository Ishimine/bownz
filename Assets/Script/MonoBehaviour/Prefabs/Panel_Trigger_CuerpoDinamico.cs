﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;


public class Panel_Trigger_CuerpoDinamico : SerializedMonoBehaviour
{
    public Vector3 direccion = Vector2.zero;
    
    public bool usarPiezaUnica = true;

    [HideIf("usarPiezaUnica")]
    public bool usarBordesIguales = true;

    public GameObject p_Inicial;

    [HideIf("usarPiezaUnica")]
    public GameObject p_Intermedia;

    [HideIf("usarBordesIguales")]
    [HideIf("usarPiezaUnica")]
    public GameObject p_Final;


    [Tooltip("Altura que debe tener cada pieza en unidades de unity")]
    public float alturaPiezas = 1;

    [Tooltip("Altura que debe tener cada pieza en unidades de unity")]
    public float anchuraPiezas = .5f;

    [Tooltip("Cantidad de piezas activas, no puede superara a las piezas maximas")]
    [SerializeField]
    public int piezasActivas = 3;
    public int PiezasActivas
    {
        get { return piezasActivas; }
        set
        {
            piezasActivas = value;
            if (piezasActivas > piezasMaximas)
                piezasActivas = piezasMaximas;
            else if (piezasActivas < 0)
                piezasActivas = 0;
        }
    }



    [SerializeField] private int piezasMaximas = 7;
    public int PiezasMaximas
    {
        get { return piezasMaximas; }
    }

    [SerializeField] private List<GameObject> piezas;


    public Vector3 correcionDePosicion;



    public void SetActivas(int i)
    {
        PiezasActivas = i;
        DistribuirPartes();
    }


    public void SetMaximas(int i)
    {
        if (i < 1)
            return;
        piezasMaximas = i;
        piezasActivas = PiezasMaximas;
        CrearPiezas();
        DistribuirPartes();
    }

    

    [Button(name:"Crear Piezas")]
    private void CrearPiezas()
    {
        DestruirHijos();
        #region Preparar Referecias en base a la configuracion
        if (p_Inicial == null)
        {
            Debug.LogWarning("p_Inicial es NULL");
            return;
        }

        GameObject bordeA = p_Inicial;
        GameObject centro;
        GameObject bordeB;


        if (usarPiezaUnica)
        {
            centro = p_Inicial;
            bordeB = p_Inicial;
        }
        else
        {
            if (usarBordesIguales)
            {
                bordeB = bordeA;
            }
            else
            {
                if (p_Final == null)
                {
                    Debug.LogWarning("p_Final es NULL");
                    return;
                }
                bordeB = p_Final;
            }
            if (p_Intermedia == null)
            {
                Debug.LogWarning("p_Intermedia es NULL");
                return;
            }
            centro = p_Intermedia;
        }
        #endregion


        //Borde A
        GameObject obj = Instantiate<GameObject>(bordeA, transform);
        obj.transform.localScale = new Vector3(anchuraPiezas, alturaPiezas, 1);
        piezas.Add(obj);

        if (piezasMaximas == 1) return;


        //Borde B
        GameObject objFinal = Instantiate<GameObject>(bordeB, transform);
        objFinal.transform.localScale = new Vector3(anchuraPiezas, alturaPiezas, 1);
        if (usarBordesIguales) obj.transform.localScale = new Vector3(anchuraPiezas, -alturaPiezas, 1);


        if (piezasMaximas == 2)
        {
            piezas.Add(objFinal);
            return;
        }



        int partesCentrales = piezasMaximas - 2;

        //Centro
        for (int i = 0; i < partesCentrales; i++)
        {
            obj = Instantiate<GameObject>(centro, transform);
            obj.transform.localScale = new Vector3(anchuraPiezas, alturaPiezas, 1);
            piezas.Add(obj);
        }
        

        piezas.Add(objFinal);
    }


    private void SetActivosInactivos()
    {
        if(piezasActivas == 0)
        {
            foreach(GameObject g in piezas)
            {
                g.SetActive(false);
            }
            return;
        }
        else if (piezasActivas == 1)
        {
            piezas[0].SetActive(true);

            for (int i = 1; i < piezasMaximas; i++)
            {
                piezas[i].SetActive(false);
            }
        }
        else if(piezasActivas == 2)
        {
            for (int i = 1; i < piezasMaximas; i++)
            {
                piezas[i].SetActive(false);
            }
            piezas[0].SetActive(true);
            piezas[piezas.Count - 1].SetActive(true);
        }
        else
        {

            for (int i = 1; i < piezasActivas; i++)
            {
                piezas[i].SetActive(true);
            }
            for (int i = piezasActivas-1; i < piezasMaximas-1; i++)
            {
                piezas[i].SetActive(false);
            }

            piezas[0].SetActive(true);
            piezas[piezas.Count - 1].SetActive(true);
        }





    }

    [Button(name:"Distribuir")]
    private void DistribuirPartes()
    {

        SetActivosInactivos();
        
        float alturaTotal = (float)piezasActivas * alturaPiezas;
        float desplazamiento =  alturaTotal / 2;
        float aux = alturaPiezas / 2;


        piezas[0].transform.position = transform.position + this.transform.up * -(desplazamiento - aux) + correcionDePosicion * alturaPiezas;

        if (direccion != Vector3.zero)
            piezas[0].transform.position += direccion * alturaTotal/2;

      



        piezas[piezas.Count - 1].transform.position = transform.position + this.transform.up * (desplazamiento - aux) + correcionDePosicion * alturaPiezas;

        if (direccion != Vector3.zero)
            piezas[piezas.Count - 1].transform.position += direccion * alturaTotal/2;


    


   //     int piezasCentrales = piezasActivas - 2;

        for (int i = 1; i < piezas.Count - 1; i++)
        {

            piezas[i].transform.position = transform.position + this.transform.up * -(desplazamiento - aux) + this.transform.up * i * alturaPiezas + correcionDePosicion * alturaPiezas;
            if (direccion != Vector3.zero)
                piezas[i].transform.position += direccion * alturaTotal / 2;

        }

    }



    [Button (name: "Destruir Hijos")]
    private void DestruirHijos()
    {
        foreach(GameObject t in piezas)
        {
            if (!Application.isPlaying && Application.isEditor)
                DestroyImmediate(t.gameObject);
            else
                Destroy(t.gameObject);
        }
        piezas.Clear();
    }
}

