﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class MonedaDeTiempo : Moneda
{



    [HideReferenceObjectPicker]    
    public VariableReference<float> tiempoDeVidaMoneda;


    
    public int cantShakes = 3;


    public GameObject bordeCuerpo;
    public SpriteRenderer borde;
    public Moneda_Shake shaker;
    private IEnumerator rutinaSecundaria;
    


    public override void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            base.OnTriggerEnter2D(collision);
            StopCoroutine(rutinaSecundaria);
            bordeCuerpo.SetActive(false);
        }
    }

    IEnumerator CuentaDeShake(float x)
    {

        float paso = (1 / (float)(cantShakes + 1));
        float ciclo = 1 - paso * 2;

        float a = 1;

        do
        {
            a -= Time.deltaTime / x;
            if (a <= ciclo)
            {
                if (ciclo >= paso)
                {
                    SonidoAlerta();
                }
                StartShake(a);
                ciclo -= paso;
                //particulas.Play();
            }
            yield return null;
        }
        while (a > 0);

        if (rutinaPrimaria != null) StopCoroutine(rutinaPrimaria);

        rutinaPrimaria = Desaparecer(true);
        StartCoroutine(rutinaPrimaria);
    }


    public void StartShake(float lerpValue)
    {
        Moneda_Shake.ShakeConfig c = Moneda_Shake.ShakeConfig.Lerp(shaker.shakeFuerte, shaker.shakeDebil, lerpValue);
        shaker.IniciarShake(c);
    }
    

    public override void Activar()
    {
        bordeCuerpo.SetActive(true);
         if(rutinaSecundaria != null) StopCoroutine(rutinaSecundaria);
        base.Activar();
        rutinaSecundaria = CuentaDeShake(tiempoDeVidaMoneda.Value);
        StartCoroutine(rutinaSecundaria);
    }

}
