﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody2D),typeof(CircleCollider2D))]
public class PelotaJugador : MonoBehaviour
{

    public AudioSource aSource;

    public ConfiguracionPelota config;

    public RunTimeSet_Pelota lista;

    public ParticleSystem trailActual;
    public ParticleSystem particulasExplosion;



    public GameEvent Evento_Pelota_Destruida;

    public GameEvent Evento_Pelota_PreDestruida;

    public GameEvent Evento_Pelota_Desaparecida;


    public C_Wrap wrap;

    public CircleCollider2D col;
    public Rigidbody2D rb;

    Vector3 lastMove;


    public UnityEventVector3 rebote;

    Vector3 posInicial;



    public Player_AnimControl pAnim;

    private void Awake()
    {
        posInicial = transform.position;

        pAnim.Born_Play();

    }


    public void AplicarTamaño()
    {
        transform.localScale = Vector3.one * config.Tamaño;
    }

    public void AplicarMasa()
    {
        rb.mass = config.Masa;
    }

    public void AplicarConfiguracion()
    {
        wrap.activadoEnX = config.wrapX;
        wrap.activadoEnY = config.wrapY;        
        AplicarTamaño();
        AplicarMasa();

    }

    public void Update()
    {
        if (Time.timeScale == 0)
        {
            Vector3 a = Vector3.up * (Mathf.Sin(Time.realtimeSinceStartup * 13) / 25);
            transform.position = transform.position - lastMove + a;
            lastMove = a;
        }
        
    }

    public void DesactivarFisica()
    {
        col.isTrigger = true;
        rb.simulated = false;
        rb.isKinematic = true;
    }

    public void ActivarFisica()
    {
        col.isTrigger = false;
        rb.simulated = true;
        rb.isKinematic = false;
    }

    public void Activar ()
    {
        Debug.Log("Pelota Activada");
        trailActual.Play();
    }

    public void Desactivar()
    {
        Debug.Log("Pelota Desactivada");
        trailActual.Stop();
    }

    public void Destruir()
    {
        //Animar
        Debug.Log("                                                                         Metodo Pelota Destuida");
        ShakeControl.instance.ActivarShake(ShakeControl.FuerzaShake.Medio);

        if (Vibration.isActive) Vibration.Vibrate((long)200);

        //Vibration.Vibrate((long)200);
        //Destroy(this.gameObject);
        col.isTrigger = true;
        rb.velocity = Vector2.zero;
        rb.angularVelocity = 0;
        rb.simulated = false;
        //gameObject.SetActive(false);

        trailActual.Stop();
        //trailActual.gameObject.SetActive(false);

        particulasExplosion.Play();
        aSource.Play();
        pAnim.Explosion_Play();
    }

    public void EventoPrePelotaDestruida()
    {
        Debug.Log("                                                                         Evento Pelota PRE -Destruida");

        Evento_Pelota_PreDestruida.Raise();
    }


    public void EventPelotaDestruida()
    {
        Debug.Log("                                                                         Evento Pelota Destruida");
        Evento_Pelota_Destruida.Raise();
    }

    public void Aparecer(Vector3 pos)
    {
        Debug.Log("                                                                         Metodo APARECER");
        gameObject.transform.position = pos;
        gameObject.transform.rotation = Quaternion.Euler(0, 0, 0);
        rb.simulated = true;
        col.isTrigger = false;
    }


    public void Aparecer()
    {
        Aparecer(posInicial);
    }


    public void Desaparecer()
    {
        Debug.Log("                                                                        Desaparecida");
        //Animar
        Evento_Pelota_Desaparecida.Raise();
        gameObject.transform.position = posInicial;

    }

    public void OnEnable()
    {
        Debug.LogWarning("Player: ENABLE");
        AplicarConfiguracion();
        lista.Add(this);   
    }

    public void OnDisable()
    {
        Debug.LogWarning("Player: DISABLE");
        lista.Remove(this);
    }

    public void Born()
    {
        pAnim.Born_Play();
        Activar();
    }

    public void Alive()
    {
        pAnim.IsAlive_Play();
        Activar();
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        //        Debug.Log(other);

     /*   ContactPoint2D[] contacts = new ContactPoint2D[2];
        int cant = other.contacts;*/

       // Debug.Log("             Contactos " + cant + ": " + contacts[0].point);

        //        Debug.Log(cant);
        Vector2 dir = ((Vector2)transform.position - other.contacts[0].point).normalized;

        float fRebote = config.FuerzaRebote;

        PropiedadRebote propRebote = other.gameObject.GetComponent<PropiedadRebote>();

        if (propRebote != null)
        {
            fRebote *= propRebote.indiceDeRebote.Value;
            rb.AddForce(dir * fRebote, ForceMode2D.Impulse);
        }
        else if (config.rebotarSiempre)
            rb.AddForce(dir * fRebote, ForceMode2D.Impulse);


        //Evento_Pelota_Rebote.Raise();
        rebote.Invoke(other.contacts[0].point);


    }


    
}