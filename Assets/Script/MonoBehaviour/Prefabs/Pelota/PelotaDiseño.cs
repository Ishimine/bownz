﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class PelotaDiseño : SerializedMonoBehaviour {


    public Administrador_Progreso progreso;
    public SpriteRenderer rDiseño;
    public SpriteRenderer rCuerpo;
    public SpriteRenderer rSombra;
    public SpriteRenderer rBrillo;

    [HideReferenceObjectPicker]
    public VariableReference<PelotaSkin> skinActivo;

    public void Awake()
    {
        SkinAleatorio();
    }

    public void SkinAleatorio()
    {
        int id = 0;




        if (progreso.ListaSkinsDesbloqueados.Count > 1)
        {
            id = progreso.ListaSkinsDesbloqueados[ Random.Range(1, progreso.ListaSkinsDesbloqueados.Count)];
        }
        AplicarSkin(progreso.skins.GetDiseño(id));
    }

    public void AplicarSkin(PelotaSkin skin)
    {
        rDiseño.sprite = skin.Diseño;
        rCuerpo.sprite = skin.Cuerpo;
        rSombra.sprite = skin.Sombra;
        rBrillo.sprite = skin.Brillo;
        rDiseño.color = skin.cDiseño;
        rCuerpo.color = skin.cCuerpo;
        rSombra.color = skin.cSombra;
    }


    public void AplicarSkinActivo()
    {
        AplicarSkin(skinActivo.Value);
    }

}
