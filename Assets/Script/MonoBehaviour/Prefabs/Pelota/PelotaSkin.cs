﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;


[System.Serializable]
[CreateAssetMenu(fileName = "skin_Pelota", menuName = "Skins/Pelota")]
public class PelotaSkin : SerializedScriptableObject
{
    [SerializeField] private Sprite diseño;
    public Sprite Diseño
    {
        get { return diseño; }
        set { diseño = value; }
    }
    public Color cDiseño;

    [SerializeField] private Sprite cuerpo;
    public Sprite Cuerpo
    {
        get { return cuerpo; }
        set { cuerpo = value; }

    }
    public Color cCuerpo;

    [SerializeField] private Sprite sombra;
    public Sprite Sombra
    {
        get {return sombra; } set { sombra = value; }
    }
    public Color cSombra = new Color(0,0,0,.3f);

    [SerializeField] private Sprite brillo;
    public Sprite Brillo
    {
        get { return brillo; }
        set { brillo = value; }

    }



    public void Copiar(PelotaSkin skin)
    {
        this.Diseño = skin.Diseño;
        this.Sombra = skin.Sombra;
        this.Cuerpo = skin.Cuerpo;
        this.Brillo = skin.Brillo;
        this.cCuerpo = skin.cCuerpo;
        this.cDiseño = skin.cDiseño;
        this.cSombra = skin.cSombra;

    }
}
