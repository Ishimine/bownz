﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;


public class Moneda : SerializedMonoBehaviour
{

    /*[HideReferenceObjectPicker]
    public VariableReference<float> puntaje;
    */

    [HideReferenceObjectPicker]
    public VariableReference<float> valorMoneda;
    public ConfiguracionGrafica config;



    public bool act = true;
    public Collider2D col;
    public float tiempoDesaparicion = 2;
    public IEnumerator rutinaPrimaria;

    public Vector3 lastMove;
    private float vel;

    [Header("GameObjects")]
    public GameObject cuerpo;


    [Header("Audio")]
    public AudioClip sonidoIn;
    public AudioClip sonidoOut;
    public AudioClip sonidoPickUp;
    public AudioClip sonidoAlerta;
    public AudioSource playerA;
    public AudioSource playerB;



    [Header("Eventos")]
    public GameEvent monedaAtrapada;
    public GameEvent monedaPerdida;

    [Header("Visual")]
    public SpriteRenderer render;
    public Color colorInicial;
    public ParticleSystem particulas;

    public virtual void Start()
    {
  
    }
 


    public virtual void OnTriggerEnter2D(Collider2D other)
    {
        if (!act) return;
        Debug.Log("OnTriggerEnter2D");
        if (other.tag == "Player")
        {
            act = false;
            if (rutinaPrimaria != null) StopCoroutine(rutinaPrimaria);
            rutinaPrimaria = Desaparecer(false);
            StartCoroutine(rutinaPrimaria);
            if (config.UsarParticulas)
                particulas.Play();
        }
    }


    private IEnumerator Aparecer()
    {
        Debug.Log("Aparecer");
        float dimInicial = 1;
        float dimMaxima = transform.localScale.x*1.2f;

        transform.localScale = Vector3.one;
        float x = 0;
        float a = 0;
        
        
        SonidoIn();

        while (x < 1)
        {
            x += Time.deltaTime / tiempoDesaparicion;

            if (x < .5)
            {
                a = Mathf.Lerp(0, dimMaxima, x * 2);
                transform.localScale = new Vector3(a, a, a);
            }
            else
            {
                a = Mathf.Lerp(dimMaxima, dimInicial / 2, x - .5f);
                transform.localScale = new Vector3(a, a, a);
            }            
            yield return null;
        }
        transform.localScale = new Vector3(dimInicial, dimInicial, dimInicial);
        rutinaPrimaria = Flotando();
        StartCoroutine(rutinaPrimaria);
    }

    public IEnumerator Desaparecer(bool perdida)
    {
        Debug.Log("Desaparecer");
        //Debug.Log("Desaparecer");
        if (!perdida) MonedaAtrapada();
        Color orig = render.color;
        float dimInicial = transform.localScale.x;
        float dimMaxima = transform.localScale.x * 1.45f;
        float x = 0;
        SonidoOut();

        while (x < 1)
        {
            x += Time.deltaTime / tiempoDesaparicion;
            float a;
            if (x < .5)
            {
                a = Mathf.Lerp(dimInicial, dimMaxima, x * 2);
            }
            else
            {
                a = Mathf.Lerp(dimMaxima * 2, 0, x);
            }

            transform.localScale = new Vector3(a, a, a);
            Color c = Color.Lerp(orig, Color.white, x +.2f);

            render.color = c;

            yield return null;
        }
        transform.localScale = new Vector3(dimInicial, dimInicial, dimInicial);
        if (perdida) MonedaPerdida();
        DeshabilitarMoneda();
    }

    IEnumerator Flotando()
    {
        Debug.Log("Flotando");
        while (true)
        {
            Vector3 a  = Vector3.up * (Mathf.Sin(Time.realtimeSinceStartup * 7) / 25);
            transform.position = transform.position - lastMove + a;
            lastMove = a;
            yield return null;
        }
    }

    public void MonedaAtrapada()
    {
        SonidoPickUp();
        if (monedaAtrapada != null) monedaAtrapada.Raise();
       // puntaje.Value += valorMoneda.Value;
    }

    public void MonedaPerdida()
    {
        if(monedaPerdida != null)monedaPerdida.Raise();
    }


    public void OnDestroy()
    {

        StopAllCoroutines();

    }

    public void DeshabilitarMoneda()
    {
        Debug.Log("DeshabilitarMoneda");
        act = false;
        cuerpo.SetActive(false);
        if (rutinaPrimaria != null) StopCoroutine(rutinaPrimaria);

        rutinaPrimaria = EsperaryMorir(2);
        StartCoroutine(rutinaPrimaria);
    }
    
    IEnumerator EsperaryMorir(float x)
    {
        Debug.Log("EsperaryMorir");
        yield return new WaitForSeconds(x);
        gameObject.SetActive(false);
    }


    #region Sonido

    #endregion
    public void SonidoIn()
    {
        SonidoReproducir(sonidoIn, playerA);
    }
    public void SonidoOut()
    {
        SonidoReproducir(sonidoOut, playerA);
    }
    public void SonidoPickUp()
    {
        SonidoReproducir(sonidoPickUp, playerB);
    }
    public void SonidoAlerta()
    {
        SonidoReproducir(sonidoAlerta, playerB);
    }


    public void SonidoReproducir(AudioClip clip, AudioSource player)
    {
       // player.pitch = Random.Range(.8f, 1.1f);
        player.clip = clip;
        player.Play();
    }



    public virtual void Activar()
    {
        act = true;
        Debug.Log("Activar");
        cuerpo.SetActive(true);
        render.color = colorInicial;

        if (rutinaPrimaria != null) StopCoroutine(rutinaPrimaria);
        gameObject.SetActive(true);
        rutinaPrimaria = Aparecer();
        StartCoroutine(rutinaPrimaria);
    }

    public virtual void Desactivar()
    {
        StopAllCoroutines();
        DeshabilitarMoneda();
    }

    public bool IsActive()
    {
        return gameObject.activeSelf;
    }
}
