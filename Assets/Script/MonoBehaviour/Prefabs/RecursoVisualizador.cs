﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecursoVisualizador : MonoBehaviour {


    public Slider sliderInf;
    public Slider sliderMed;
    public Slider sliderSup;

    public Image colorMed;

    public Color descargando;
    public Color cargando;

    public VariableFloat actual;
    public VariableFloat maximo;
    public VariableFloat solicitado;

    float sliderMedObj;

    public float smooth = 5f;
    public float smooth2 = 0;
    private float vel;
    private float vel2;

//    int t = 0;



    public void Llenar()
    {
        sliderInf.value = 1;
        sliderSup.value = 1;
        sliderMed.value = 1;
    }


    public void Update()
    {
        float targetSup = solicitado.Value / maximo.Value;
        sliderSup.value = Mathf.SmoothDamp(sliderSup.value, targetSup, ref vel, smooth);


        float targetMid = actual.Value / maximo.Value;
        float ini = sliderMed.value;
        sliderMed.value = Mathf.SmoothDamp(sliderMed.value, targetMid, ref vel2, smooth2,1000f);

       // sliderMed.value = targetMid;

        float fin = sliderMed.value;

        if (fin - ini > 0) colorMed.color = cargando;
        else  if (fin - ini < 0) colorMed.color = descargando;

    }

}
