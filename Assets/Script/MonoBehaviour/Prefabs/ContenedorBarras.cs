﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

[Serializable]
public class ContenedorBarras {

    public ConfiguracionBarras config;
    public GameObject prefab;
    public BarraTouch[] barras = new BarraTouch[0];
 
    

    [SerializeField] List<BarraTouch> barrasInactivas = new List<BarraTouch>();
    [SerializeField] List<BarraTouch> barrasActivas = new List<BarraTouch>();
    [SerializeField] List<BarraTouch> barrasEnProduccion = new List<BarraTouch>();




    public void Awake()
    {
        CrearBarras();
    }

    public void CrearBarras()
    {
        barras = new BarraTouch[config.barrasTotales.Value];
        for (int i = 0; i < config.barrasTotales.Value; i++)
        {
            GameObject clone = GameObject.Instantiate(prefab);
            barras[i] = clone.GetComponent<BarraTouch>();
            barras[i].IdBarra = i;
            barras[i].SetContenedor(this);
            barras[i].pFisica.gameObject.SetActive(false);
            barras[i].pTransparente.gameObject.SetActive(false);

            if (i < config.barrasActivas.Value + 2)
            {
                if (!barrasInactivas.Contains(barras[i])) barrasInactivas.Add(barras[i]);
            }
        }
    }


    public void VaciarProduccion()
    {
        for (int i = barrasEnProduccion.Count-1; i >= 0; i--)
        {
        /*     barrasInactivas.Add(barrasEnProduccion[i]);
             barrasEnProduccion[i].IdBarra = -1;
             barrasEnProduccion[i].IdTouch = -1;
             barrasEnProduccion.RemoveAt(i);*/
            ContactoFisico(barrasEnProduccion[i]);
        }
    }



    public BarraTouch ExtraerBarraListaFiFo(List<BarraTouch> lista)
    {
        if (lista.Count == 0) return null;
        BarraTouch t = lista[0];
        lista.RemoveAt(0);
        return t;
    }

    public BarraTouch ExtractBarraActivaFIFO()
    {
        return ExtraerBarraListaFiFo(barrasActivas);
    }

    public BarraTouch ExtractBarraInactivaFIFO()
    {
        return ExtraerBarraListaFiFo(barrasInactivas);
    }

    public BarraTouch ExtractBarraEnProduccionFIFO()
    {
        return ExtraerBarraListaFiFo(barrasEnProduccion);
    }



    public BarraTouch ExtractBarraPorID(List<BarraTouch> lista ,int id)
    {
        BarraTouch t = GetBarraPorId(lista,id);
        lista.Remove(t);
        return t;
    }
    

    public BarraTouch GetBarraPorId(List<BarraTouch> lista, int id)
    {

     return lista.Find(x => x.IdTouch == id);
    }


    public BarraTouch ExtractBarraActivaPorID(int id)
    {
        return ExtractBarraPorID(barrasActivas, id);
    }

    public BarraTouch ExtractBarraInactivaPorID(int id)
    {
        return ExtractBarraPorID(barrasInactivas, id);
    }

    public BarraTouch ExtractBarraEnProduccionPorID(int id)
    {
        return ExtractBarraPorID(barrasEnProduccion, id);
    }



    public void TouchIn(TouchSimple t)
    {
        BarraTouch b = ExtractBarraInactivaFIFO();
        if (b == null)
        {
            Debug.LogWarning("No quedan barras inactivas");

            return;
        }
        b.IdTouch = t.fingerId;
        b.gameObject.SetActive(true);
        b.TouchIn(t.worldPos);



        if(!barrasEnProduccion.Contains(b)) barrasEnProduccion.Add(b);
    }


    public bool TouchMove(TouchSimple t)
    {
        BarraTouch b = GetBarraPorId(barrasEnProduccion, t.fingerId);
        if (b == null) return false;
        return b.TouchMove(t.worldPos);     

    }

    public bool TouchOut(TouchSimple t)
    {
        BarraTouch b = ExtractBarraEnProduccionPorID(t.fingerId);
        if (b == null)
        {
            return false;
        }

        if (b.TouchOut(t.worldPos))
        {
            if (!barrasActivas.Contains(b)) barrasActivas.Add(b);

            if (barrasActivas.Count > config.barrasActivas.Value)
            {
                barrasActivas[0].DesaparicionPrematura();
            }
            return true;
        }
        else
        {
            //    Debug.Log("id: " + b.IdTouch + " agregado a lista INACTIVAS");
            if (!barrasInactivas.Contains(b)) barrasInactivas.Add(b);
            b.IdTouch = -1;
            return false;
        }
        
}

    public void ContactoTransparente(BarraTouch b)
    {
        if(!barrasActivas.Contains(b)) barrasActivas.Add(b);
        barrasEnProduccion.Remove(b);
    }

    public void ContactoFisico (BarraTouch b)
    {
        barrasActivas.Remove(b);
        if (!barrasInactivas.Contains(b)) barrasInactivas.Add(b);       

        b.IdTouch = -1;
    }
    
    public void DeshabilitartTodas()
    {
        foreach (BarraTouch b in barrasActivas)
        {
            b.Deshabilitar();
            if (!barrasInactivas.Contains(b)) barrasInactivas.Add(b);
        }

        foreach (BarraTouch b in barrasEnProduccion)
        {
            b.Deshabilitar();
            if (!barrasInactivas.Contains(b)) barrasInactivas.Add(b);
        }
    }

    
}

