﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class Escenario_Spike : SerializedMonoBehaviour {

    public bool esActivaDer;

    public ParedDinamica[] paredes;



    public int tamañoTriggerActual = 22;
    public int tamañoTriggerFinal = 2;
    public int tamañoTriggerInicial = 22;
    public int pinchesPorEtapa = 2;
    public int pasosPorEtapa = 7;

    [SerializeField] private int pasoActual = 0;
    public int PasoActual
    {
        get { return pasoActual; }
        set
        {
            if(value % pasosPorEtapa == 0)
            {
                ReducirTrigger();
            }
            pasoActual = value;
        }
    }


    public void Awake()
    {
        PasoActual = 0;
        tamañoTriggerActual = tamañoTriggerInicial;
    }

    public void FullPinches()
    {
        PasoActual = 0;
        tamañoTriggerActual = tamañoTriggerInicial;
        paredes[0].FullPinches();
        paredes[1].FullPinches();
    }

    public void ReducirTrigger()
    {
        if (tamañoTriggerActual == tamañoTriggerFinal) return;
        tamañoTriggerActual -= pinchesPorEtapa;
    }

    public void AumentarTrigger()
    {
        if (tamañoTriggerActual == tamañoTriggerInicial) return;
        tamañoTriggerActual += pinchesPorEtapa;
    }

    public int GetParedActiva()
    {
        if (esActivaDer) return 1;
        else return 0;
    }

    public int GetParedInactiva()
    {
        if (!esActivaDer) return 1;
        else return 0;
    }


    [Button(name:"Siguiente")]
    public void Siguiente()
    {
        PasoActual++;

        esActivaDer = Random.Range(0, 2) == 1;

        paredes[GetParedActiva()].SetTamañoTrigger(tamañoTriggerActual);
        paredes[GetParedActiva()].ReRoll();
        paredes[GetParedInactiva()].FullPinches();
    }

}








