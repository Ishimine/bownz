﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarcadorTouch : MonoBehaviour {

    public SpriteRenderer render;

    public C_ScaleInOut scale;

    public bool enMovimiento;

    public IEnumerator rutina;

    private void Awake()
    {
        rutina = contadorInactividad();
    }

    public void TouchIn()
    {
        gameObject.SetActive(true);
        scale.ScaleIn();
        if(rutina != null) StartCoroutine(rutina);
    }

    public void TouchMove()
    {
        enMovimiento = true;
    }

    public void TouchOut()
    {
        if(rutina != null) StopCoroutine(rutina);
        gameObject.SetActive(true);
        scale.ScaleOut();
    }

    IEnumerator contadorInactividad()
    {
//        Debug.Log("Iniciado");
        yield return new WaitForSecondsRealtime(1);
        if (enMovimiento)
        {
            enMovimiento = false;
            StartCoroutine(rutina);
        }
        else
        {
            TouchOut();
        }
    }

}
