﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[RequireComponent(typeof(C_CuentaRegresiva))]
public class Spawner : SerializedMonoBehaviour
{

   /* public bool usarTiempoVidaItem;

    [ShowIf("usarTiempoVidaItem")]
    public float tVidaItem;*/




    private C_CuentaRegresiva contador;
    [HideReferenceObjectPicker]
    public VariableReference<GameObject> item;
    public GameObject Item
    {
        get { return item.Value; }
        set { item.Value = value; }
    }

    //public Vector3 posPrimerEstrella;
    public Transform posPrimerSpawn;


    [HideReferenceObjectPicker]
    public VariableReference<Vector2> posActual;
    public Vector2 PosActual
    {
        get { return posActual.Value; }
        set { posActual.Value = value; }
    }
   
    [HideReferenceObjectPicker]
    public VariableReference<Vector2> posSiguiente;
    public Vector2 PosSiguiente
    {
        get { return posSiguiente.Value; }
        set { posSiguiente.Value = value; }
    }



    public bool areaDeSpawnInCamera = true;



    public List<GameObject> items;


    [ShowIf("areaDeSpawnInCamera")]
    public Vector2 recorteFinal;


    [Tooltip("IfTrue: el area de Spawn iniciara con el valor asignado en areaDeSpawn, menos, el recorte inicial y se ira expandiendo hasta el tamaño total")]
    public bool usarDificultadCreciente = true;
    [ShowIf("usarDificultadCreciente")]
    public Vector2 recorteInicial;

    [ShowIf("usarDificultadCreciente")]
    public int pasosDeCrecimiento = 10;
    [ShowIf("usarDificultadCreciente")]
    [SerializeField]  private int pasoActual;


    [ShowIf("usarDificultadCreciente")]
    public Vector2 centroInicial;
    [ShowIf("usarDificultadCreciente")]
    public Vector2 centroFinal;
    [ShowIf("usarDificultadCreciente")]
    public Vector2 centro;


    public Vector2 areaDeSpawn;

    private Vector2 areaDeSpawnFinal;
    private Vector2 areaDeSpawnInicial;

    public IMarcadorSpawn marcadorSiguiente;

    public float distanciaMinima = 1.5f;
   


    private void Awake()
    {
        contador = GetComponent<C_CuentaRegresiva>();

        
        CalcularAreaDeCamara();
        //PosicionarMarcador();
        PosSiguiente = ProximaPosicion();
    }

    public void Reiniciar()
    {
        pasoActual = 0;
        areaDeSpawn = areaDeSpawnInicial;
        centro = centroInicial;
    }

    public void CalcularAreaDeCamara()
    {
        Vector2 dimPantalla = new Vector2(Camera.main.orthographicSize / Screen.height * Screen.width, Camera.main.orthographicSize);
        if (usarDificultadCreciente)
        {
            areaDeSpawnFinal = dimPantalla - recorteFinal;
            areaDeSpawnInicial = dimPantalla  - recorteInicial;
            areaDeSpawn = areaDeSpawnInicial;
        }
        else
        {
            areaDeSpawn = dimPantalla - recorteFinal;
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawCube(centro, areaDeSpawn*2);
    }

    [Button(name: "InGame: Iniciar Creacion")]
    public void IniciarCreacion()
    {
        Reiniciar();
        PosSiguiente = posPrimerSpawn.position;
        CrearItem();
        contador.IniciarCuentaRegresiva();
    }

    [Button(name: "InGame: Detener Creacion")]
    public void DetenerCreacion()
    {
        contador.Detener();
    }


    [Button(name: "Eliminar Creaciones")]
    public void EliminarCreaciones()
    {
        for (int i = items.Count - 1; i >= 0; i--)
        {
            Destroy(items[i].gameObject);
            items.RemoveAt(i);
        }
    }

    [Button(name: "Spawn Item")]
    public void CrearItem()
    {
        PosActual = PosSiguiente;
        GameObject item = Instantiate<GameObject>(Item, PosSiguiente, Quaternion.identity);

        items.Add(item);
        PosSiguiente = ProximaPosicion();
        PosicionarMarcador();

        AplicarCrecimientoDeAreaSpawn();

        //Item.GetComponent<IItemSpawneable>().Activar();


    }



    public Vector2 ProximaPosicion()
    {
        Vector2 punto;
        do
        {
           punto  = new Vector2(Random.Range(-areaDeSpawn.x, areaDeSpawn.x), Random.Range(-areaDeSpawn.y, areaDeSpawn.y));
        } while (Mathf.Abs(Vector2.Distance(PosSiguiente, punto)) < distanciaMinima);

//        Debug.Log(Mathf.Abs(Vector2.Distance(PosSiguiente, punto)));
        AplicarMovimientoDeArea();
        return punto + centro;
    }

    public void PosicionarMarcador()
    {
        marcadorSiguiente.Posicionar(posSiguiente.Value);
        marcadorSiguiente.Activar(contador.tiempoIni.Value);
    }

    public void AplicarCrecimientoDeAreaSpawn()
    {
        if (usarDificultadCreciente && pasoActual <= pasosDeCrecimiento)
        {
            pasoActual++;
            float aux = ((float)pasoActual / (float)pasosDeCrecimiento);
            areaDeSpawn = Vector3.Lerp(areaDeSpawnInicial, areaDeSpawnFinal, aux);
        }
    }

    public void AplicarMovimientoDeArea()
    {
        float aux = ((float)pasoActual / (float)pasosDeCrecimiento);
        centro = Vector2.Lerp(centroInicial, centroFinal, aux);
    }

    public void LimpiarListaNull()
    {
        items.Remove(null);        
    }
}

