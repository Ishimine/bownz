﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;


public class ParedDinamica : SerializedMonoBehaviour {

    public static int piezasPorPared = 22;

    public Panel_Trigger_CuerpoDinamico[] pPinches;
    public Panel_Trigger_CuerpoDinamico pTrigger;


    public int tamañoTrigger;
    public int TamañoTrigger
    {
        get { return tamañoTrigger; }
        set
        {
            tamañoTrigger = value;
            if (tamañoTrigger < 2)
                tamañoTrigger = 2;
            else if (tamañoTrigger > 22)
                tamañoTrigger = 22;
        }
    }
    

    public void SetTamañoTrigger(int x)
    {
        TamañoTrigger = x;
    }


    [Button(name:"ReRoll")]
    public void ReRoll()
    {
        pPinches[0].gameObject.SetActive(true);
        pTrigger.gameObject.SetActive(true);
        pPinches[1].gameObject.SetActive(true);



        int rango = (piezasPorPared - tamañoTrigger) / 2;

        int posTrigger = Random.Range(-rango, rango);


        pPinches[0].SetActivas( (piezasPorPared / 2) - tamañoTrigger/ 2 - posTrigger) ;
        pPinches[1].SetActivas( (piezasPorPared / 2) - tamañoTrigger / 2 + posTrigger);

        pTrigger.SetActivas(tamañoTrigger);

        pTrigger.transform.position =  Vector3.up * posTrigger + Vector3.right * transform.position.x;

    }

    [Button(name:"Full Pinches")]
    public void FullPinches()
    {
        pPinches[0].SetActivas(piezasPorPared / 2);
        pPinches[0].gameObject.SetActive(true);
        
        pPinches[1].SetActivas(piezasPorPared / 2);
        pPinches[1].gameObject.SetActive(true);

        pTrigger.gameObject.SetActive(false);
    }


    [Button(name:"Full Trigger")]
    public void FullTrigger()
    {
        pPinches[0].gameObject.SetActive(false);

        pPinches[1].gameObject.SetActive(false);

        pTrigger.SetActivas(piezasPorPared);
        pTrigger.gameObject.SetActive(true);
        pTrigger.gameObject.transform.position = new Vector3(pTrigger.gameObject.transform.parent.position.x,0,0);
    }


}
