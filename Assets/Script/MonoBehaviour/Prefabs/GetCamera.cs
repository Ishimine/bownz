﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetCamera : MonoBehaviour {

    public Canvas canvas;

    private void Awake()
    {
        canvas.worldCamera = Camera.main;
        
    }
}
