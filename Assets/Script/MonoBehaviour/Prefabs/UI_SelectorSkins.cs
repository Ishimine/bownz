﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class UI_SelectorSkins : SerializedMonoBehaviour {

    public GameObject botonPrefab;
    public Administrador_Skin diseños;
    public Administrador_Progreso progreso;

    public RectTransform panel;
    

    public void Awake()
    {
        CrearBotones();
    }


    [Button]
    public void CrearBotones()
    {
        
        Archivo_Progreso p = progreso.Progreso;

        GameObject obj = Instantiate<GameObject>(botonPrefab, transform);
        obj.GetComponent<BotonSkin>().Id = 0;

        for (int i = 1; i < progreso.skins.diseños.Length; i++)
        {
            obj = Instantiate<GameObject>(botonPrefab, transform);

            BotonSkin s = obj.GetComponent<BotonSkin>();
            s.Id = i;
            Button b = obj.GetComponent<Button>();

            if (p.skinsUnlockedIds.Contains(i))
            {
                s.CargarDiseñoIcono();
                b.interactable = true;
            }
            else
            {
                s.CargarDiseñoBloqueado();
                b.interactable = false;

            }
        }

        //panel.sizeDelta = new Vector2(panel.sizeDelta.x, ((p.skinsUnlockedIds.Count / 5) + 1)  * 100);
        panel.sizeDelta = new Vector2((progreso.skins.diseños.Length)  * 100,  panel.sizeDelta.y);

    }

    [Button]
    public void DestruirHijos()
    {
        if(Application.isPlaying)
        {
            for (int i = transform.childCount-1; i >= 0; i--)
            {
                Destroy(transform.GetChild(i).gameObject);
            }
        }
    }


    public void OnEnable()
    {
        DestruirHijos();
        CrearBotones();
        
    }
}
