﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class PoolRotativo : SerializedMonoBehaviour {

    /// <summary>
    /// IfTrue: Cuando se active el proximo objeto el previo se desactivara
    /// </summary>
    public bool desactivarPrevio = false;

    public bool usarSpawnContinuo = false;
    public VariableReference<float> tiempoEntreSpawns;
    
    public VariableReference<float> tVidaItem;

    public GameObject item;

    public Vector2 posPrimerSpawn = new Vector2(0,5);

    public int cant = 3;
    public int idAct;
    private int act = 0;

    public GameObject goActual;
    public GameObject[] gOPool;
    public IItemSpawneable[] iSPpool;


    [Space(2)]
    public bool usarCrecimientoDeArea;

    [ShowIf("usarCrecimientoDeArea")]
    public int pasoAct = 0;
    /// <summary>
    /// Cantidad de "pasos" que se deben dar hasta que el area de spawn llega a su dimensino maxima
    /// </summary>
    [ShowIf("usarCrecimientoDeArea")]
    public int pasoFinal = 20;


    [Header("Area De Spawn")]
    [SerializeField]  private Vector2 areaSpawnAct;
    public Vector2 areaSpawnIni;
    /// <summary>
    /// Variable de REFERENCIA, modificarlo NO afectara la funcino del objeto
    /// </summary>
    [ShowIf("usarCrecimientoDeArea")]
    public Vector2 areaSpawnFin;

    [Header("Pos. Area de Spawn")]
    [SerializeField] private Vector2 posSpawnAct;
    public Vector2 posSpawnIni;
    [ShowIf("usarCrecimientoDeArea")]
    public Vector2 posSpawnFin;

    [Header("Pos. item/marcador")]
    public Vector2 posSig;
    public Vector2 posAct;


    public bool usarMarcador;
    [ShowIf("usarMarcador")]
    public IMarcadorSpawn[] marcadores;


    public Vector2 distAlBorde = new Vector2(1.5f, 5);

    [Header("Set de Superposicion")]
    /// <summary>
    /// Se evitara spawnear cerca de los objetos dentro de estas listas
    /// </summary>
    public RunTimeSet<GameObject>[] setDeSuperposicion;

    public float distMinimaEntreItems = 3;

    /// <summary>
    /// Define la cantidad de intentos que se permiten de chequeo de Superposicion
    /// </summary>
    public int intentosSuperposicion = 10;

    IEnumerator rutina;

    public void Awake()
    {
        gOPool = new GameObject[cant];
        iSPpool = new IItemSpawneable[cant];

        for (int i = 0; i < cant; i++)
        {
            GameObject clone = Instantiate<Object>(item, transform) as GameObject;
            gOPool[i] = clone;
            iSPpool[i] = (IItemSpawneable)gOPool[i].GetComponent(typeof(IItemSpawneable));
            gOPool[i].SetActive(false);
        }

        //Calcular dim Pantalla
        float y = Camera.main.orthographicSize * 2;
        areaSpawnFin =  new Vector2(y * Camera.main.aspect -1f, y) /2;
        areaSpawnFin = new Vector2(areaSpawnFin.x - distAlBorde.x, areaSpawnFin.y - distAlBorde.y);
        
        if(distMinimaEntreItems >= areaSpawnIni.x)
        {
            distMinimaEntreItems = areaSpawnIni.x;
            Debug.LogError("'Distancia minima entre items' es menor a la dimension X del area de spawn. Objeto:" + name);
            Debug.LogWarning("'La distancia minima entre items' a sido sobreescrita al valor = " + distMinimaEntreItems);
        }
    }
    
    
    public void Iniciar()
    {
        Siguiente();
    }


    [Button]
    public void Siguiente()
    {
//        Debug.Log("Siguiente");
        //Aplica crecimiento de area
        if (usarCrecimientoDeArea)
            ActualizarArea();

        //Desactivamos Previo
        if(iSPpool[idAct].IsActive() && desactivarPrevio)
            iSPpool[idAct].Desactivar();

        //Calculamos ID siguiente
        act++;
        idAct = act % cant;

        //Calculamos Posicion Siguiente
        CalcularPosSig();

        //Activamos y posicionamos item actual
        iSPpool[idAct].Activar(tVidaItem.Value);
        gOPool[idAct].transform.position = posAct;
        gOPool[idAct].transform.rotation = Quaternion.identity;

        //Activamos y posicionamos marcador de item siguiente
        if (usarMarcador)
        {
            foreach(IMarcadorSpawn marcador in marcadores)
            {
                marcador.SetActive(true);
                marcador.Posicionar(posSig);
                marcador.Activar(tiempoEntreSpawns.Value);
            }
        }

        if(usarSpawnContinuo)
        {
            if (rutina != null) StopCoroutine(rutina);
            rutina = CuentaRegresiva(tiempoEntreSpawns.Value);
            StartCoroutine(rutina);
        }
    }

    IEnumerator CuentaRegresiva(float tiempo)
    {
        yield return new WaitForSeconds(tiempo);
        Siguiente();
    }
        
    public void CalcularPosSig()
    {
        posAct = posSig;

        int intento = 0;
        do
        {
            intento++;
            posSig = posSpawnAct + new Vector2(Random.Range(-areaSpawnAct.x, areaSpawnAct.x), Random.Range(-areaSpawnAct.y, areaSpawnAct.y));

        } while (intento <= intentosSuperposicion && !EsPosicionValida(posAct, posSig));
        /*
        if(intento > intentosSuperposicion)
        {
            Debug.LogWarning("Se SUPERO la cantidad de intentos de superposicion: " + intento);
        }
        else
        {
            Debug.LogWarning("Se ENCONTRO posicion valida en " + intento + " intentos.");
            Debug.Log("Actual: "+ posAct + " Siguiente:" + posSig);
            Debug.Log("Distancia: " + Vector2.Distance(posAct,posSig));
        }*/
    }

    public bool EsPosicionValida(Vector3 actual, Vector3 siguiente)
    {
        if (EstaCerca(actual, siguiente))
        {
            Debug.LogWarning("                             POSICION INVALIDA:" + Vector2.Distance(actual, siguiente));
            return false;
        }                

        foreach (RunTimeSet<GameObject> set in setDeSuperposicion)
        {
            for(int i = 0; i < set.Items.Count; i++)
            {
                if(EstaCerca(set.Items[i].transform.position, siguiente))
                {
                    Debug.LogWarning("                             POSICION INVALIDA:" + Vector2.Distance(set.Items[i].transform.position, siguiente));
                    return false;
                }
            }
        }
        Debug.Log("                             POSICION VALIDA, Distancia: " + Vector2.Distance(actual, siguiente));
        return true;
    }

    public bool EstaCerca(Vector3 actual, Vector3 siguiente)
    {
        return Vector2.Distance(actual, siguiente) < distMinimaEntreItems;
    }

    public void ActualizarArea()
    {
        if (pasoAct >= pasoFinal) return;
        pasoAct++;
        if (pasoFinal == 0) Debug.LogError("Paso Final = 0 IMPOSIBLE WACHO");

//        Debug.Log(" pasoAct / pasoFinal = " + (float)pasoAct / (float)pasoFinal);

        float a = (float)pasoAct / (float)pasoFinal;
        areaSpawnAct = Vector2.Lerp(areaSpawnIni, areaSpawnFin, a);
        posSpawnAct = Vector2.Lerp(posSpawnIni, posSpawnFin, a);

    }

    public void Inicializar()
    {
        posAct = posPrimerSpawn;
        posSig = posAct;
        areaSpawnAct = areaSpawnIni;
        posSpawnAct = posSpawnIni;
        pasoAct = 0;
        posAct = posSpawnIni;

    }

    public void DeshabilitarItems()
    {
        for(int i = 0; i < cant; i++)
        {
            iSPpool[i].Desactivar();
        }
    }

    public void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireCube(posSpawnAct, areaSpawnAct*2);
    }
    

   
}
