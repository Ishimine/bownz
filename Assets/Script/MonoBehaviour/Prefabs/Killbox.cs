﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Killbox : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D collision)
    {
        PelotaJugador pelota = collision.GetComponent<PelotaJugador>();
        if (pelota != null)
        {
          //  Debug.Log("aaa");
            pelota.Destruir();
        }
    }
}
