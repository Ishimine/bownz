﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BotonSkin : MonoBehaviour {


    public Administrador_Skin diseños;
    public Image diseño;
    public Image cuerpo;
    public Image sombra;
    public Image brillo;

    [SerializeField]private int id;



    public Color cDiseñoBloqueado;
    public Color cCuerpoBloqueado;

    public int Id
    {
        get { return id; }
        set
        {
            id = value;
        }
    }

    private void OnValidate()
    {
        CargarDiseñoIcono();
    }

    public void Presionado()
    {
        diseños.SetDiseño(id);
    }


    public void CargarDiseñoIcono()
    {
        PelotaSkin nDiseño = diseños.GetDiseño(id);

        diseño.sprite = nDiseño.Diseño;
        CheckColor(diseño, nDiseño.cDiseño);

        cuerpo.sprite = nDiseño.Cuerpo;
        CheckColor(cuerpo, nDiseño.cCuerpo);

        sombra.sprite = nDiseño.Sombra;
        CheckColor(sombra, nDiseño.cSombra);

        brillo.sprite = nDiseño.Brillo;
        CheckColor(brillo, 1);
    }

    public void CargarDiseñoBloqueado()
    {
        PelotaSkin nDiseño = diseños.GetDiseño(id);

        diseño.sprite = nDiseño.Diseño;
        CheckColor(diseño, cDiseñoBloqueado);

        cuerpo.sprite = nDiseño.Cuerpo;
        CheckColor(cuerpo, cCuerpoBloqueado);

        sombra.sprite = nDiseño.Sombra;
        CheckColor(sombra, Color.clear);

        brillo.sprite = nDiseño.Brillo;
        CheckColor(brillo, Color.clear);
    }

    public void CheckColor(Image render, float alfa)
    {
        if (render.sprite == null)
            render.color = Color.clear;
        else
            render.color = new Color(render.color.r, render.color.g, render.color.b, alfa);
    }


    public void CheckColor(Image render, Color c)
    {
        if (render.sprite == null)
            render.color = new Color(render.color.r, render.color.g, render.color.b, 0f);
        else
            render.color = c;
    }
}
