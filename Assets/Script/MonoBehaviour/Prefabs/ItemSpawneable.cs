﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public abstract class ItemSpawneable : SerializedMonoBehaviour
{

    public abstract void Activar();
    public abstract void Desactivar();
}
