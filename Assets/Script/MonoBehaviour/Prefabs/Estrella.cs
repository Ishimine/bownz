﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class Estrella : SerializedMonoBehaviour, IItemSpawneable {


    public ConfiguracionGrafica configGrafica;
    public VariableReference<float> tVida;
    public Variable<Vector3> ultimaMonedaPos;

    public bool isActive = true;

    [Header("Audio")]
    public AudioClip sonidoIn;
    public AudioClip sonidoOut;
    public AudioClip sonidoPickUp;
    public AudioClip sonidoAlerta;
    public AudioSource playerA;
    public AudioSource playerB;


    [Header("Visual")]
    public GameObject cuerpo;
    public SpriteRenderer rCentro;
    public Color cCentro;
    public SpriteRenderer rBorde;
    public Color cBorde;



    [Header("Animacion")]
    public bool flotando;
    public IEnumerator rutina;
    public Moneda_Shake shaker;
    public float velDesaparicion = .3f;
    public int cantShakes = 3;
    private Vector3 lastMove;
    public ParticleSystem particulas;


    public bool usarShake;
    [ShowIf("usarShake")]
    public float frecInicial = 3;
    [ShowIf("usarShake")]
    public float frecFinal = 3;

    [ShowIf("usarShake")]
    public float magnitudInicial;
    [ShowIf("usarShake")]
    public float magnitudFinal;


    [Range(0,1)]
    public float tShakePreOut;


    [Header("Eventos")]
    public GameEvent monedaAtrapada;
    public GameEvent monedaPerdida;


    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player" && isActive)
        {
            ultimaMonedaPos.Value = transform.position;
            isActive = false;
            if (rutina != null) StopCoroutine(rutina);
            rutina = Desaparecer(false);
            StartCoroutine(rutina);

            if (configGrafica.UsarParticulas)
                particulas.Play();
        }        
    }


    public void Update()
    {
        if (flotando) Flotar();
    }

    /// <summary>
    /// Animacion flotando
    /// </summary>
    public void Flotar()
    {
        Vector3 a = Vector3.up * (Mathf.Sin(Time.realtimeSinceStartup * 7) / 25);
        transform.position = transform.position - lastMove + a;
        lastMove = a;
    }



    


    public void StartShake(float lerpValue)
    {
        Moneda_Shake.ShakeConfig c = Moneda_Shake.ShakeConfig.Lerp(shaker.shakeFuerte, shaker.shakeDebil, lerpValue);
        shaker.IniciarShake(c);
    }



    public void SetParent(Transform parent)
    {
        transform.SetParent(parent);
    }

    public void Activar()
    {
        gameObject.SetActive(true); isActive = true;
        if (rutina != null) StopCoroutine(rutina);
        rutina = Aparecer();
        StartCoroutine(rutina);

        cuerpo.SetActive(true);
        rCentro.color = cCentro;
        rBorde.color = cBorde;

    }

    public void Activar(float tDeVida)
    {
        tVida.Value = tDeVida;
        Activar();
    }

    public void Desactivar()
    {
        isActive = false;
        //cuerpo.SetActive(false);
        if (rutina != null) StopCoroutine(rutina);
        gameObject.SetActive(false);
    }


    public void MonedaAtrapada()
    {
        if (monedaAtrapada != null) monedaAtrapada.Raise();
    }

    public void MonedaPerdida()
    {
        Debug.Log("Moneda Perdida");
        if (monedaPerdida != null) monedaPerdida.Raise();
    }


    #region Sonido

    #endregion
    public void SonidoIn()
    {
        SonidoReproducir(sonidoIn, playerA);
    }
    public void SonidoOut()
    {
        SonidoReproducir(sonidoOut, playerA);
    }
    public void SonidoPickUp()
    {
        SonidoReproducir(sonidoPickUp, playerB);
    }
    public void SonidoAlerta()
    {
        SonidoReproducir(sonidoAlerta, playerB);
    }


    public void SonidoReproducir(AudioClip clip, AudioSource player)
    {
        // player.pitch = Random.Range(.8f, 1.1f);
        player.clip = clip;
        player.Play();
    }


    private IEnumerator Aparecer()
    {
        float dimInicial = 1;
        float dimMaxima = transform.localScale.x * 1.2f;

        transform.localScale = Vector3.one;
        float x = 0;
        float a = 0;


        SonidoIn();

        while (x < 1)
        {
            x += Time.deltaTime / velDesaparicion;

            if (x < .5)
            {
                a = Mathf.Lerp(0, dimMaxima, x * 2);
                transform.localScale = new Vector3(a, a, a);
            }
            else
            {
                a = Mathf.Lerp(dimMaxima, dimInicial / 2, x - .5f);
                transform.localScale = new Vector3(a, a, a);
            }
            yield return null;
        }
        transform.localScale = new Vector3(dimInicial, dimInicial, dimInicial);
        flotando = true;

        rutina = CuentaDeShake(tVida.Value);
        StartCoroutine(rutina);
    }

    private IEnumerator Desaparecer(bool perdida)
    {
        flotando = false;
        //Debug.Log("Desaparecer");
        if (!perdida) MonedaAtrapada();

        float dimInicial = transform.localScale.x;
        float dimMaxima = transform.localScale.x * 1.45f;
        float x = 0;

        if (perdida) SonidoOut();

        while (x < 1)
        {
            x += Time.deltaTime / velDesaparicion;
            float a;
            if (x < .5)
            {
                a = Mathf.Lerp(dimInicial, dimMaxima, x * 2);
            }
            else
            {
                a = Mathf.Lerp(dimMaxima * 2, 0, x);
            }

            transform.localScale = new Vector3(a, a, a);
            Color c = Color.Lerp(cCentro, Color.white, x + .2f);

            rCentro.color = c;
            yield return null;
        }
        transform.localScale = new Vector3(dimInicial, dimInicial, dimInicial);
        if (perdida) MonedaPerdida();
        Desactivar();
    }

    private IEnumerator CuentaDeShake(float x)
    {
        Debug.Log("CuentaDeShake");
        bool activado = false;
        float a = 0;

        float frecActual = 0;
        float magnitudActual = 0;

        do
        {
            a += Time.deltaTime / x;
            frecActual = Mathf.Lerp(frecInicial, frecFinal,a);
            magnitudActual = Mathf.Lerp(magnitudInicial,magnitudFinal,a);

            if (a >= tShakePreOut && !activado)
            {
                activado = !activado;
                SonidoAlerta();
                StartShake(0);
            }
            else if (!activado && usarShake)
            {
                Debug.Log("activado!");
                transform.rotation = Quaternion.Euler(Vector3.forward *  Mathf.Sin(Mathf.PI * frecActual * a) * magnitudActual);
            }
            yield return null;
        }
        while (a < 1);

        if (rutina != null) StopCoroutine(rutina);
        rutina = Desaparecer(true);
        StartCoroutine(rutina);



        /*
        float paso = (1 / (float)(cantShakes + 1));
        float ciclo = 1 - paso * 2;
        float a = 1;
        do
        {
            a -= Time.deltaTime / x;
            if (a <= ciclo)
            {
                if (ciclo >= paso)
                {
                    SonidoAlerta();
                }
                StartShake(a);
                ciclo -= paso;
            }
            yield return null;
        }
        while (a > 0);

        if (rutina != null) StopCoroutine(rutina);
        rutina = Desaparecer(true);
        StartCoroutine(rutina);*/
    }

    public bool IsActive()
    {
        return gameObject.activeSelf;
    }
}
