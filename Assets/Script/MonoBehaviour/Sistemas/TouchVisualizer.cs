﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchVisualizer : MonoBehaviour {

    public ConfiguracionBarras config;
    public GameObject obj;
    public VariableTouch touch;

    public MarcadorTouch[] visualizadores;
    public int actual;

    public void Start()
    {        
        visualizadores = new MarcadorTouch[config.barrasTotales.Value];
        for (int i = 0; i < config.barrasTotales.Value; i++)
          {
              GameObject clone = Instantiate<GameObject>(obj, transform);
              visualizadores[i] = clone.GetComponent<MarcadorTouch>();
              clone.SetActive(false);
          }
    }

    public void TouchIn()
    {
        //Debug.Log("Touch In");
        visualizadores[actual].TouchIn();
        PosicionarTouch();
    }

    public void TouchMove()
    {
        //Debug.Log("Touch Move");
        visualizadores[actual].TouchMove();
        PosicionarTouch();
    }
    public void TouchOut()
    {
        //Debug.Log("Touch Out");
        visualizadores[actual].TouchOut();
        PosicionarTouch();
    }

    public void LateUpdate()
    {
        actual = 0;
    }




    public void PosicionarTouch()
    {
        visualizadores[actual].transform.position = touch.Value.worldPos;
        actual++;
    }



}
