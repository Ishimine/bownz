﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class UI_FloatToText : MonoBehaviour {

    public Text txt;

    public void FloatToText(float value)
    {
        txt.text = value.ToString("0.0");
    }



    public void OnValidate()
    {
        txt = GetComponent<Text>();
    }
}
