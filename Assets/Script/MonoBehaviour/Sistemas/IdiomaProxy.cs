﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdiomaProxy : MonoBehaviour {

    public ConfiguracionIdioma idiomaConfig;

    private void Awake()
    {
        idiomaConfig.IdentificarLenguajeSistema();
    }

}
