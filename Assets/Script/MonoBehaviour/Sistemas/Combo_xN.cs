﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class Combo_xN : SerializedMonoBehaviour {

    [HideReferenceObjectPicker]
    public VariableReference<float> valorPorPaso;
    public float ValorPorPaso
    {
        get { return valorPorPaso.Value; }
        set { valorPorPaso.Value = value; }
    }   


    [HideReferenceObjectPicker]
    public VariableReference<int> pasoActual;
    public int PasoActual
    {
        get { return pasoActual.Value; }
        set { pasoActual.Value = value; }
    }

    [HideReferenceObjectPicker]
    public VariableReference<float> multiplicadorActual;
    public float MultiplicadorActual
    {
        get { return multiplicadorActual.Value; }
        set { multiplicadorActual.Value = value; }
    }

    [HideReferenceObjectPicker]
    public VariableReference<float> puntaje;
    public float Puntaje
    {
        get { return puntaje.Value; }
        set { puntaje.Value = value; }
    }

    public bool usarMultiplicadorExterno = false;

    [ShowIf("usarMultiplicadorExterno")]
    public VariableFloat multExterno;

    public float MultExterno
    {
        get {
            if (usarMultiplicadorExterno && multExterno != null)
            {
               return multExterno.Value;
            }
            else
                return 1;
        }
    }


    public void ComboBreak()
    {
        //Debug.LogError("CCCOCCOCOCOCOCOMBO BREAKER");
        PasoActual = 1;
        MultiplicadorActual = 1;
    }

    public void Sumar()
    {
        PasoActual++;
        MultiplicadorActual = PasoActual * ValorPorPaso;
        AplicarPuntaje();
    }

    private void AplicarPuntaje()
    {
        float aux;
        float aux2 = 1;
        if (usarMultiplicadorExterno)
        {
            aux = MultExterno * MultiplicadorActual;
            aux2 = 0;
        }
        else
        {
            aux = MultiplicadorActual;
        }

        Debug.Log("Puntaje: " + (aux - aux2));
        Puntaje = aux - aux2; 
    }
}
