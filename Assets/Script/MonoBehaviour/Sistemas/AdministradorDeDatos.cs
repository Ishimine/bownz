﻿using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;


public class AdministradorDeDatos {


    public static bool CargarDatos<T>(ref T ar, string nombreFisico)
    {
        if (File.Exists(Application.persistentDataPath + "/" + nombreFisico))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/" + nombreFisico, FileMode.Open);
            ar = (T)bf.Deserialize(file);   
            file.Close();
            return true;
        }
        else
        {
            return false;
        }
    }
   
    public static void GuardarDatos<T>(ref T ar, string nombreFisico)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/" + nombreFisico);
        bf.Serialize(file, ar);
        file.Close();
        
    }
}
