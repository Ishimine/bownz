﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchReader : MonoBehaviour {

    public ConfiguracionGeneral config;
    
    public bool mouse;
    public VariableTouch touchActual;
        
    public GameEvent touchIn;
    public GameEvent touchStationary;
    public GameEvent touchMove;
    public GameEvent touchOut;
    public GameEvent touchCanceled;


    Vector3 touchPos;

    public LayerMask AreaDeTouchInvalida;
    /// <summary>
    /// 
    /// </summary>
    /// <param name="tPos">Posicion en pixeles</param>
    /// <returns></returns>
    public bool TouchIsInValidArea(Vector2 tPos)
    {
        touchPos = Camera.main.ScreenToWorldPoint(tPos);

        RaycastHit ray = new RaycastHit();

        bool contacto = Physics.Raycast(touchPos, Vector3.forward, out ray, 30, AreaDeTouchInvalida);

        if(contacto)
            Debug.DrawLine(touchPos, ray.point,Color.green);
        else
            Debug.DrawLine(touchPos, touchPos + Vector3.forward * 30, Color.red);


        return !contacto;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(touchPos, .5f);
       // Gizmos.DrawLine(touchPos, touchPos + Vector3.forward * 30);
    }

    public void Update()
    {
      /*  if (!config.TouchReader_Activado) return;*/
#if UNITY_EDITOR


        if(!TouchIsInValidArea(Input.mousePosition) && mouse)
        {
            print("Touch Invalido");
            return;
        }

        if (Input.GetMouseButtonDown(0) && mouse)
        {
            touchActual.Value = new TouchSimple(TouchPhase.Began,1, Input.mousePosition);
            touchIn.Raise();
        }
        else if (Input.GetMouseButton(0) && mouse)
        {
            touchActual.Value = new TouchSimple(TouchPhase.Moved,1, Input.mousePosition);
            touchMove.Raise();
        }
        else if (Input.GetMouseButtonUp(0) && mouse)
        {
            touchActual.Value = new TouchSimple(TouchPhase.Ended,1, Input.mousePosition);
            touchOut.Raise();
        }


#endif


#if UNITY_ANDROID


     
        

        foreach (Touch t in Input.touches)
        {
            if (!TouchIsInValidArea(t.position))
                continue;


            touchActual.Value = new TouchSimple(t);
            switch (t.phase)
            {
                case TouchPhase.Began:
                    touchIn.Raise();
                    break;
                case TouchPhase.Moved:
                    touchStationary.Raise();
                    break;
                case TouchPhase.Stationary:
                    touchMove.Raise();
                    break;
                case TouchPhase.Ended:
                    touchOut.Raise();
                    break;
                case TouchPhase.Canceled:
                    touchCanceled.Raise();
                    break;
                default:
                    break;
            }
        }
#endif
    }


}
