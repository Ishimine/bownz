﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AutoLenguaje_UI_Text : MonoBehaviour {

    public ConfiguracionIdioma idiomaConfig;
    public string textId;
    public Text txt;

    public bool todoMayuscula;

    public bool todoMinuscula;

    [ConditionalHide("todoMayuscula")]
    public bool iniciarConMayuscula;


    public bool inicialMayuscula;


    public void Start()
    {
        string text = idiomaConfig.Activo.GetText(textId);


        if (todoMayuscula)
            text.ToUpper();
        else if (todoMinuscula)
            text.ToLower();

     /*   if (iniciarConMayuscula)
        {
            string aux = text[0].ToString().ToUpper();
        } */

        txt.text = text;
    }

}
