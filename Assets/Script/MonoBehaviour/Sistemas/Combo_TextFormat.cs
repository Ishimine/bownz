﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class Combo_TextFormat : SerializedMonoBehaviour {

    [HideReferenceObjectPicker]
    public VariableReference<string> prefijo;
    [HideReferenceObjectPicker]
    public VariableReference<string> sufijo;
    [HideReferenceObjectPicker]
    public VariableReference<string> textoFinal;
    [HideReferenceObjectPicker]
    public VariableReference<float> multiplicador;


    public void FormatearTexto()
    {
        textoFinal.Value = prefijo.Value + multiplicador.Value.ToString("0.") + sufijo.Value;
    }


}

