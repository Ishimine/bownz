﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

/// <summary>
/// Este objeto solo puede ser "conocido" por el GameManager del modo activo,
/// quien le dice cuando ejecutar que eventos, como tambien que "ModoActivo" es el actual
/// y que "id_TablaActiva" hay que utilizar
/// 
/// Actualmente existe 1 GameManager para el modo survival
/// </summary>
[CreateAssetMenu(fileName = "gameCycle", menuName = "Game_Cycle")]
public class Game_Cycle : SerializedScriptableObject, IGame_Cycle {

    [HideReferenceObjectPicker]
    public VariableReference<String> id_ModoActivo;
    [HideReferenceObjectPicker]
    public VariableReference<String> id_TablaActiva;

    [HideReferenceObjectPicker]
    public Id_Modo id_ModoAct;

    public Id_Modo Id_ModoAct
    {
        get
        {
            return id_ModoAct;
        }
        set
        {
            id_ModoAct = value;
        }
    }
    



    public GameEvent preStart_Game;
    public GameEvent start_Game;
    public GameEvent pause_Game;
    public GameEvent resume_Game;
    public GameEvent end_Game;
    public GameEvent exit_Game;


    public GameEvent post_End_Game;

    private float timeScaleInGame;



    public void SetIdModoActivo(string id)
    {
        id_ModoActivo.Value = id;
    }

    public void SetIdTablaActiva(string id)
    {
        id_TablaActiva.Value = id;
    }

    [SerializeField] private static bool enJuego;
    public static bool EnJuego
    {
        get { return enJuego; }
        set { enJuego = value; }
    }

    [SerializeField] private static bool enPause;
    public static bool EnPausa
    {
        get { return enPause; }
        set { enPause = value; }
    }



    public void Juego_PreStart()
    {
        if (preStart_Game != null)
        {
            preStart_Game.Raise();
            EnJuego = false;
            EnPausa = false;
        }
    }

    public void Juego_Start()
    {
        if (start_Game != null)
        {
            Debug.Log("   >>>> Game Cycle:                 <<<Start>>>");
            start_Game.Raise();
            EnJuego = true;
        }

    }   

    public void Juego_Pause()
    {
        if (pause_Game != null)
        {
            timeScaleInGame = Time.timeScale;
            Time.timeScale = 0;
            Debug.Log("   >>>> Game Cycle:                 <<<Pause>>>");
            pause_Game.Raise();
            EnPausa = true;
        }
    }
   

    public void SetJuego_Pause(bool x)
    {
        if(x)
        {
            Juego_Pause();
        }
        else
        {
            Juego_Resume();
        }
    }

    public void Juego_Resume()
    {
        if (resume_Game != null)
        {
            Time.timeScale = timeScaleInGame;
            Debug.Log("   >>>> Game Cycle:                 <<<Resume>>>");
            resume_Game.Raise();
            EnPausa = false;
        }
    }

    
    public void Juego_End()
    {
        if (end_Game != null && EnJuego)
        {
            Debug.Log("   >>>> Game Cycle:                <<<End>>>");
            end_Game.Raise();
            EnJuego = false;

            post_End_Game.Raise();
        }
    }




    public void Juego_Exit()
    {
        /*
        if (exit_Game != null)
        {
            exit_Game.Raise();
            Debug.Log("   >>>> Game Cycle:                <<<Exit>>>");
            EnJuego = false;
            EnPausa = false;
        }*/
    }

}
