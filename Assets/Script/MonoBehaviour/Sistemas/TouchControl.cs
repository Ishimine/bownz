﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;


public class TouchControl : SerializedMonoBehaviour {


    public ConfiguracionGeneral config_Gen;
    public VariableTouch touch;
    public List<TouchSimple> listaTouchs;
    public ContenedorBarras contenedor;

    public GameEvent swipeInicial;
    public int swipeCount;
    



    public void Awake()
    {
        contenedor.Awake();
    }

    public void ResetCount()
    {
        swipeCount = 0;
    }

    public void TouchIn()
    {
        if (Game_Cycle.EnPausa || config_Gen.SwipeReader_Desactivado)
            return;
        contenedor.TouchIn(touch.Value);
    }

    public void TouchMove()
    {
        if (Game_Cycle.EnPausa || config_Gen.SwipeReader_Desactivado)
            return;
        if (contenedor.TouchMove(touch.Value))
        {
            swipeCount++;
            if (swipeCount == 1)
                swipeInicial.Raise();
        }

    }

    public void VaciarProduccion()
    {
        contenedor.VaciarProduccion();
    }



    public void TouchOut()
    {
        if (Game_Cycle.EnPausa || config_Gen.SwipeReader_Desactivado)
            return;


        if (contenedor.TouchOut(touch.Value))
        {
            swipeCount++;
            if (swipeCount == 1)
                swipeInicial.Raise();
        }

    }

    public void DeshabilitarBarras()
    {
        contenedor.DeshabilitartTodas();
    }


    public void CheckSwipe()
    {
        if (contenedor.TouchOut(touch.Value))
        {
            swipeCount++;
            if (swipeCount == 1)
                swipeInicial.Raise();
        }
    }
}
