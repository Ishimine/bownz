﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Canvas_Control : MonoBehaviour {

    public ConfiguracionIdioma idiomaConfig; 
    public ConfiguracionGeneral generalConfig;
    public Animator anim;
    public Game_Cycle game_Cycle;
    public Text txt;
    public Text desc;

    public Image candado;


    public void ActualizarTexto()
    {
        txt.text = game_Cycle.Id_ModoAct.Nombre;




        if (game_Cycle.Id_ModoAct.isBetaMode)
        {
            desc.text = idiomaConfig.Activo.GetText(game_Cycle.Id_ModoAct.GetIdDescBeta(generalConfig.BetaModesActive));

            if (generalConfig.BetaModesActive)
                candado.gameObject.SetActive(false);    
            else
                candado.gameObject.SetActive(true);
        }
        else
        {

            desc.text = idiomaConfig.Activo.GetText(game_Cycle.Id_ModoAct.id_Descripcion);

            if (game_Cycle.Id_ModoAct.IsUnlocked)
                candado.gameObject.SetActive(false);
            else
                candado.gameObject.SetActive(true);
        }




    }

    private void Awake()
    {
        SetHome();
    }
    /// <summary>
    /// 0 = Home | 1 = InGame | 2 = Pausa | 3 = EndGame
    /// </summary>
    /// <param name="i"></param>
	public void SetEstado(int i)
    {
        anim.SetInteger("Estado", i);
    }

    public void SetHome()
    {
        anim.SetInteger("Estado", 0);
    }


    public void SetInGame()
    {
        anim.SetInteger("Estado", 1);
    }

    public void SetPausa()
    {
        anim.SetInteger("Estado", 2);
    }

    public void SetEndGame()
    {
        anim.SetInteger("Estado", 3);
    }




}
