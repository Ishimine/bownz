﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boton_Vibracion : MonoBehaviour {


    public Color cPrendido;
    public Color cApagado;

    public Image img;


    public void Awake()
    {
        AplicarColor();
    }
    public void Presionado()
    {
        Vibration.isActive = !Vibration.isActive;
        if(Vibration.isActive)        
            Vibration.Vibrate(100);
        AplicarColor();
    }


    public void AplicarColor()
    {
        if(Vibration.isActive)        
            img.color = cPrendido;        
        else
            img.color = cApagado;
    }
}
