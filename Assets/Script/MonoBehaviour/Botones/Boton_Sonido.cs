﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class Boton_Sonido : MonoBehaviour {

    public AudioMixer mixer;
    public static bool activo = true;

    public Color cPrendido;
    public Color cApagado;

    public Image img;


    public void Awake()
    {
        if (activo)
        {
            img.color = cPrendido;
        }
        else
        {
            img.color = cApagado;
        }
    }

    public void Presionado()
    {
        activo = !activo;
        if(activo)
        {
            mixer.SetFloat("VolMaster", 0);
            img.color = cPrendido;
        }
        else
        {
            mixer.SetFloat("VolMaster", -80);
            img.color = cApagado;
        }
    }
}
