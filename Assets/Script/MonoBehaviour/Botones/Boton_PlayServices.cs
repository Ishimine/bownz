﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boton_PlayServices : MonoBehaviour
{
    public VariableString id_ModoActivo;
    public VariableString id_TablaActiva;

    public void ShowActiveLeaderboard()
    {
        PlayServicesControl.ShowLeaderBoard(id_TablaActiva.Value, GooglePlayGames.BasicApi.LeaderboardTimeSpan.Daily);
    }
     
    public void ShowAllLeadeboard()
    {
        PlayServicesControl.ShowLeaderBoards();
    }

    public void SignIn()
    {
        PlayServicesControl.SignIn();
    }

    public void SignOut()
    {
        PlayServicesControl.SignOut();
    }

}
