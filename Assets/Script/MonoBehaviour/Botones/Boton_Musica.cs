﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class Boton_Musica : MonoBehaviour {

    public static bool activo = true;

    public AudioMixer mixer;

    public Color cPrendido;
    public Color cApagado;

    public Image img;


    public void Awake()
    {
        if (activo)
        {
            img.color = cPrendido;
        }
        else
        {
            img.color = cApagado;
        }
    }
    public void Presionado()
    {
        activo = !activo;

        if (activo)
        {
            mixer.SetFloat("VolMusica", 0);
        }
        else
        {
            mixer.SetFloat("VolMusica", -80);
        }
    }
}
