﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_AnimControl : MonoBehaviour {

    public Animator anim;

    public void Born_Play()
    {
        SetState(0);
    }

    public void Death_Play()
    {
        SetState(1);
    }

    public void IsAlive_Play()
    {
        SetState(2);
    }


    public void Implosion_Play()
    {
        SetState(3);
    }

    public void Explosion_Play()
    {
        SetState(4);
    }

    private void SetState(int n)
    {
        anim.SetInteger("State", n);
        anim.SetTrigger("Go");
    }
}
