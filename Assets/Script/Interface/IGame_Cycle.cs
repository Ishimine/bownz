﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGame_Cycle  {


    void Juego_Start();
    void Juego_Pause();
    void Juego_Resume();
    void Juego_End();
    void Juego_Exit();
}
