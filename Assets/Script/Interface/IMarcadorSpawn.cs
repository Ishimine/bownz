﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMarcadorSpawn {

    /// <summary>
    /// 
    /// </summary>
    /// <param name="t">Tiempo de animacion que tiene el marcador</param>
    void Activar(float t);

    void Desactivar();

    void OnDestroy();   

    void Posicionar(Vector3 pos);

    void SetActive(bool isActive);

    void SetParent(Transform parent);

}
