﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IItemSpawneable
{

    void Activar();

    void Activar(float tiempoVida);

    void Desactivar();

    bool IsActive();


}
