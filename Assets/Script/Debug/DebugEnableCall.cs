﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugEnableCall : MonoBehaviour {



    public void OnEnable()
    {
        Debug.LogWarning("Enable");
    }

    public void OnDisable()
    {
        Debug.LogWarning("Disable");
    }
}
