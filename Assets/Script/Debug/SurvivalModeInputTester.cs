﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurvivalModeInputTester : MonoBehaviour {

    public GameMode_Survival surv;


	void Update () {

        if (Input.GetKeyDown(KeyCode.L))
        {
            surv.Siguiente();
        }


        if (Input.GetKeyDown(KeyCode.K))
        {
            surv.Anterior();
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            surv.InstanciarObjetos();
        }

    }
}
