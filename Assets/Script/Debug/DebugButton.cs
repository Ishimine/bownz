﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugButton : MonoBehaviour {

    public bool encendido = false;

    public int clicksNecesarios = 10;

    public int clickActual = 0;

    public Toggle debugToggle;

    public void Presionado()
    {
        clickActual++;
        if (clickActual < clicksNecesarios) return;

        PresionadoValido();
    }

    public void PresionadoValido()
    {
        encendido = !encendido;

        debugToggle.isOn = encendido;

    }





}
