﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugTimeScaleShow : MonoBehaviour {

    public Text txt;


    public void Update()
    {
        txt.text = "TimeScale: " + Time.timeScale.ToString("0.000");
    }

}
