﻿
using UnityEngine;
using System;

[Serializable]
[CreateAssetMenu(fileName = "String_", menuName = "Variables/String")]
public class VariableString : Variable<String> {
}
