﻿
using UnityEngine;
using System;

[Serializable]
[CreateAssetMenu(fileName = "Color_", menuName = "Variables/Color")]
public class VariableColor : Variable<Color> {
}
