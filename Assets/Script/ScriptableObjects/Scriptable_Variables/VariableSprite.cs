﻿
using UnityEngine;
using System;

[Serializable]
[CreateAssetMenu(fileName = "sprite", menuName = "Variables/Sprite")]
public class VariableSprite : Variable<Sprite> {
}
