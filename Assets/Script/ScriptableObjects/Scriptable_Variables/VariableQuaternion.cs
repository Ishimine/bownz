﻿
using UnityEngine;
using System;

[Serializable]
[CreateAssetMenu(fileName = "Quaternion_", menuName = "Variables/Quaterion")]
public class VariableQuaternion : Variable<Quaternion> {
}
