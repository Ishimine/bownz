﻿
using UnityEngine;
using System;

[Serializable]
[CreateAssetMenu(fileName = "Vector3_", menuName = "Variables/Vector3")]
public class VariableVector3 : Variable<Vector3> {
}
