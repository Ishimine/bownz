﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "skin_", menuName = "Variables/Pelota_Skin")]
public class VariablePelotaSkin : Variable<PelotaSkin>
{

}
