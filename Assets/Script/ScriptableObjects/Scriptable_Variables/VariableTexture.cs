﻿
using UnityEngine;
using System;

[Serializable]
[CreateAssetMenu(fileName = "texture", menuName = "Variables/Texture")]
public class VariableTexture : Variable<Texture> {
}
