﻿
using UnityEngine;
using System;

[Serializable]
[CreateAssetMenu(fileName = "IItemSpawneable_", menuName = "Variables/IItemSpawneable")]
public class VariableIItemSpawneable : Variable<IItemSpawneable> {
}
