﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;


[Serializable]
public class VariableReference<T>
{
    [HorizontalGroup("Group 1"), LabelWidth(160)]
    [Indent(3)]
    public bool UsarConstante = true;

    [ShowIf("UsarConstante", false)]
    [HorizontalGroup("Group 1"), HideLabel]
    public T ConstantValue;

    [HideIf("UsarConstante", false)]
    [HorizontalGroup("Group 1"), HideLabel]
    public Variable<T> Variable;

    public T Value
    {
        get
        {
            return UsarConstante ? ConstantValue :
                                 Variable.Value;
        }
        set
        {
            if (UsarConstante)
                ConstantValue = value;
            else
                Variable.Value = value;
        }
    }





}
