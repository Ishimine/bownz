﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;


[Serializable]
[CreateAssetMenu(fileName = "RecursoBarra", menuName = "Configuracion/RecursoBarra", order = 0)]
public class RecursoBarra : SerializedScriptableObject
{
    public bool usarCargas;
    public int cargasMaximas;

    public VariableReference<float> maximo;
    public VariableReference<float> actual;
    public VariableReference<float> solicitado;

    private void OnEnable()
    {
        actual.Value = maximo.Value;
    }

    public void Recargar(float cant)
    {
        actual.Value += cant;
        if (actual.Value > maximo.Value) actual.Value = maximo.Value;
//        Debug.Log("Recargado " + cant);
        solicitado.Value = actual.Value;
    }

    


    public float Maximo
    {
        get { return maximo.Value; }
        set { maximo.Value = value; }
    }

    public float Actual
    {
        get { return actual.Value; }
        set { actual.Value = value; }
    }


    /// <summary>
    /// Consulta si se puede consumir la cantidad solicitada
    /// </summary>
    /// <param name="x"></param>
    /// <returns></returns>
    public bool PuedeConsumir(float x)
    {
        if (x <= Actual)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Exije consumir la cantidad solicitada
    /// </summary>
    /// <param name="x"></param>
    /// <returns></returns>
    public bool Consumir(float x)
    {
        if (PuedeConsumir(x))
        {
            Actual -= x;
            return true;
        }
        else
        {
            return false;
        }
    }


    /// <summary>
    /// Consulta si la cantidad solicitada esta disponible, devuelve lo que NO se pudo consumir
    /// </summary>
    /// <param name="x"></param>
    /// <returns></returns>
    public float SolicitarDisponibilidadRecurso(float x)
    {
        float aux;
        if (x < Actual)
        {
            aux = x;
        }
        else
        {
            aux = Actual;
        }
        solicitado.Value = Actual - aux;
        return aux;

    }


    public void Reiniciar()
    {
        maximo.Value = maximo.Variable.DefaultValue;
        actual.Value = maximo.Value;
    }

}
