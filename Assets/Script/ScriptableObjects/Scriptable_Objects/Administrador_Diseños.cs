﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public abstract class Administrador_Diseños<T> : SerializedScriptableObject
{
    public T[] diseños;
    [HideReferenceObjectPicker]
    public VariableReference<T> diseñoActual;


    public T visualizador;
    public GameEvent diseñoCambiado;

    public int id;


    public T GetDiseño(int id)
    {
        return diseños[id];
    }

    public T GetDiseñoActual()
    {
        return diseñoActual.Value;
    }

    [Button("Aplicar Diseño")]
    public void AplicarId()
    {
        SetDiseño(id);
    }

    [Button("Random")]
    public void Random()
    {
        SetDiseño(UnityEngine.Random.Range(0, diseños.Length));
    }



    public void SetDiseño (int i)
    {
        diseñoActual.Value = diseños[i % diseños.Length];
        ActualizarVisualizador();
        if(diseñoCambiado != null) diseñoCambiado.Raise();
    }

    [Button("Siguiente")]
    public void Siguiente()
    {
        id++;
        SetDiseño(id);
    }

    [Button("Anterior")]
    public void Anterior()
    {
        id--;
        SetDiseño(id);
    }

    public void ActualizarVisualizador()
    {
        visualizador = diseñoActual.Value;
    }




}
