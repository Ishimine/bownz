﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Archivo_Progreso
{
    [HideInInspector]
   public int reseteoUniversal = 3;

    [SerializeField] private bool isFirstTime = true;
    public bool IsFirstTime
    {
        get { return isFirstTime; }
        set { isFirstTime = value; }
    }

    public int GetLatestSkinUnlocked()
    {
        return skinsUnlockedIds[skinsUnlockedIds.Count - 1];
    }



    public bool HasSkins
    {
        get
        {
            if (skinsUnlockedIds.Count > 0)
                return true;
            else
                return false;
        }
    }


    public bool IsFirstSkin
    {
        get {
            if (skinsUnlockedIds.Count == 1) return true;
            else return false;
        }
    }  

    public List<int> skinsUnlockedIds;

    public List<string> modoDesbloqueado;
    
    [SerializeField]private float exp;
    public float Exp
    {
        get { return exp; }
        set { exp = value; }
    }

    public int nivel;


    public int partidas;
    


}

