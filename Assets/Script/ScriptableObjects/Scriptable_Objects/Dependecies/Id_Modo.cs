﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[System.Serializable]
[CreateAssetMenu(fileName ="Id_Modo", menuName = "GameMode/Nuevo Id_Modo")]
public class Id_Modo : SerializedScriptableObject
{
    /// <summary>
    /// Id Global que identifica al modo de juego
    /// </summary>
   [SerializeField] private string id;
    public string Id
    {
        get { return id; }
        set { id = value; }
    }

    [SerializeField] private string nombre;
    public string Nombre
    {
        get { return nombre; }
        set { nombre = value; }
    }

   [SerializeField] private string id_desc_Locked;
    [SerializeField] private string id_desc_Unlocked;

    public string id_Descripcion
    {
        get
        {
            if (IsUnlocked)
                return id_desc_Unlocked;
            else
                return id_desc_Locked;
        }
    }

    public string GetIdDescBeta(bool isBetaUnlocked)
    {
        if (isBetaUnlocked)
            return id_desc_Unlocked;
        else
            return id_desc_Locked;
    }



    /// <summary>
    /// Id que designa la tabla de puntajes en linea del modo de juego
    /// </summary>
    [SerializeField] private string id_tabla;
    public string Id_tabla
    {
        get { return id_tabla; }
        set { id_tabla = value; }
    }

    /// <summary>
    /// IfTrue: el modo se encuentra desbloqueado desde el primer momento que se inicia el juego, por ende no necesita ser desbloqueado
    /// </summary>
    public bool isUnlockedFromStart = false;


    public bool isBetaMode = false;

    private bool isUnlocked;
    public bool IsUnlocked
    {
        get
        {
            if (isUnlockedFromStart)
                return true;
            else
                return AdministradorPuntaje.GetPuntajeMaximo(desbloqueador.id) >= desbloqueador.puntaje;
        }
        
    }

    [HideIf("isUnlockedFromStart")]
    public Desbloqueador_Modo desbloqueador;


    
    public bool DesbloquearModo(string id, float puntaje)
    {
        if (isUnlocked)
        {
            Debug.LogWarning("Cuidado!!! intentando desbloquear un modo Desbloqueado por defecto. Solucionar incongruencia antes de buildear");
            return true;
        }
        else
            return desbloqueador.DesbloquearModo(id, puntaje); 
    }

}

/// <summary>
/// Contiene el id y el puntaje necesarios para desbloquear un modo de juego, como tambien un metodo para consultar el desbloqueo
/// </summary>
[System.Serializable]
public class Desbloqueador_Modo
{
    public string id;
    public float puntaje;

    public bool DesbloquearModo(string id, float puntaje)
    {
        if (id == this.id && puntaje >= this.puntaje)
            return true;
        else
            return false;        
    }

}
