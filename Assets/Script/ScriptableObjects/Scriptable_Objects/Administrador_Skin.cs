﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "skin_Pelota_Admin", menuName = "Administradores/Administrador SkinPelota")]
public class Administrador_Skin : Administrador_Diseños<PelotaSkin>
{

    public Administrador_Progreso progreso;

   

    public PelotaSkin personalizado;
    [Button]
    public void DiseñoPersonalizado()
    {
        diseñoActual.Value = personalizado;
        ActualizarVisualizador();
        if (diseñoCambiado != null) diseñoCambiado.Raise();
    }


    [Button]
    public void SetLatestDesing()
    {
        SetDiseño(progreso.GetLatestSkinId());

    }

}
