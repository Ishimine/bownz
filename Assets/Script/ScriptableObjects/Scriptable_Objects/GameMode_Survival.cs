﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;


/// <summary>
/// Game Manager. Unico objeto con conocimiento del Game_Cycle se comunica con este para gatillar eventos clasicos de ciclo de juego (Inicio, Pausa, Fin, PreStart, Despausa...)
/// Los elementos del modo de juego deben comunicarse con este para dar a conocer cualquier evento importante
/// </summary>

[CreateAssetMenu(fileName ="GameMode_Survival", menuName = "GameMode_Survival/Main")]
public class GameMode_Survival : SerializedScriptableObject
{

    //public RecursoBarra recurso;

    public ConfiguracionGeneral configGen;
    public Game_Cycle game_Cycle;

    public ConfiguracionPelota configPelota;
    public ConfiguracionBarras configBarra;

    public int idActual;
    public SurvivalConfig actual;
    public SurvivalConfig[] configs;



    [SerializeField] private List<GameObject> listaPrefabs;
    [SerializeField] private List<GameObject> listaElementos;

    public GameEvent modoCambiado;


    public bool modosBetaActivados = false;


    private void Awake()
    {
        idActual = configs.Length * 10;
    }

    public void Activar()
    {
        actual = configs[0];
        InstanciarObjetos();
        SetConfig(0);
        Time.timeScale = 0;
    }

    public void Reactivar()
    {
        Time.timeScale = 0;
        SetConfig(idActual);
    }


    public void IniciarJuego()
    {
        Time.timeScale = 1;
        game_Cycle.Juego_Start();
    }


    public void InstanciarObjetos()
    {
        ExtractAllPrefabs();
        InstanciacionYAsignacion();
    }


    public void InstanciacionYAsignacion()
    {
        foreach (SurvivalConfig c in configs)
        {
            c.LimpiarLista();
        }

        foreach (SurvivalConfig c in configs)
        {
            c.prefabsOrdenados.Clear();
        }           

            foreach (GameObject g in listaPrefabs)
        {
            GameObject clone = Instantiate<GameObject>(g);
            foreach (SurvivalConfig c in configs)
            {
                if(c.GetPrefabs().Contains(g))
                {
                    c.elementos.Add(clone);
                    c.prefabsOrdenados.Add(g);
                }                
            }
            clone.SetActive(false);
        }
    }


    public void ExtractAllPrefabs()
    {
        ClearListaPrefabs();
        foreach (SurvivalConfig c in configs)
        {
            List<GameObject> auxPrefabs = c.GetPrefabs();
            foreach (GameObject g in auxPrefabs)
            {
                if (!listaPrefabs.Contains(g))
                    listaPrefabs.Add(g);
            }
        }
    }


    public void ClearListaPrefabs()
    {
        /*foreach(GameObject g in listaPrefabs)
        {
            if(g != null)
            {
                Destroy(g);
            }
        }*/
        listaPrefabs.Clear();

    }


    public void Siguiente()
    {
        idActual++;
        SetConfig(idActual);
    }

    public void Anterior()
    {
        idActual--;
        SetConfig(idActual);
    }


    public void SetConfig(int i)
    {
        AplicarConfig(Mathf.Abs(i % configs.Length));
        idActual = i;
        game_Cycle.SetIdModoActivo(configs[Mathf.Abs(i % configs.Length)].id);
        game_Cycle.SetIdTablaActiva(configs[ Mathf.Abs( i % configs.Length)].id_TablaPuntajes);

        game_Cycle.Id_ModoAct = configs[Mathf.Abs(i % configs.Length)].modo;
            


        

        if(game_Cycle.Id_ModoAct.isBetaMode)
        {
            if(configGen.BetaModesActive)
            {
                configGen.SwipeReader_Activado = true;
            }
            else
                configGen.SwipeReader_Activado = false;
        }
        else if (!game_Cycle.Id_ModoAct.IsUnlocked)     
            configGen.SwipeReader_Activado = false;        
        else
            configGen.SwipeReader_Activado = true;


        Debug.LogWarning("                   -------   MODO CAMBIADO RAISE()  -------                    ");
        modoCambiado.Raise();


    }


    public void AplicarConfig(int i)
    {
        actual.DesactivarElementos();
        actual = configs[i];


        //Aplicar configuraciones de modo
        configBarra.usarRecurso = actual.usarRecurso;
        configPelota.wrapX = actual.wrapX;
        configPelota.wrapY = actual.wrapY;


        game_Cycle.Juego_PreStart();

        //Instanciar objetos de modo (algunos responden a la configuracion)
        //actual.InstanciarElementos();
        actual.InstanciarFaltantes();
        actual.ActivarElementos();



        //APLICAR ACTIVACION Y DESACTIVACION DE RECURSO
    }
    /*
    public void ActivarElementos(GameObject[] elem)
    {
        foreach(GameObject g in elem)
        {
            if(elementosAct.Contains(g))
            {
                g.SetActive(true);
            }
        }
    }

    public void DesactivarElemetos(GameObject[] elem)
    {
        foreach (GameObject g in elem)
        {
            if (elementosAct.Contains(g))
            {
                g.SetActive(false);
            }
        }
    }*/

        /*
    public void InstanciarFaltantes(SurvivalConfig act)
    {
        List<GameObject> l = act.GetPrefabs();
        foreach (GameObject g in l)
        {
            if(!listaElementos.Contains(g))
            {
                GameObject clone = Instantiate<GameObject>(g);
                listaElementos.Add(clone);
                act.elementos.Add(clone);
                
            }
        }
    }*/


    public void BotonEscape()
    {
        game_Cycle.Juego_Exit();
    }


}
