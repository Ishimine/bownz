﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;


[CreateAssetMenu(fileName ="Progres_admin",menuName ="Administradores/Progreso")]
public class Administrador_Progreso : SerializedScriptableObject {

    public bool isEarlyAdopter = true;



    public ConfiguracionGeneral configGen;
   [SerializeField] private Archivo_Progreso progreso;

    public List<Id_Modo> modos;

    public GameEvent subidaDeNivel;

    public int GetLatestSkinId()
    {
        return progreso.GetLatestSkinUnlocked();
    }


    public Archivo_Progreso Progreso
    {
        get
        {
            return progreso;
        }
    }

    public bool usarExperienciaCreciente;

    public bool IsFirstTime
    {
        get
        {
            if(progreso.IsFirstTime || progreso.reseteoUniversal < 3)
            {
                progreso.reseteoUniversal = 3;
                return true;
            }
            else
            {
                return false;
            }
        }
        set
        {
            progreso.IsFirstTime = value;
        }
    }

    public bool IsFirstSkin
    {
        get
        {
            return progreso.IsFirstSkin;
        }
    }

    public bool HasSkins
    {
        get
        {
            return progreso.HasSkins;
        }
    }

    [HideIf("usarExperienciaCreciente")]
    public float expPorNivel = 150;
    public float ExpPorNivel
    {
        get { return expPorNivel; }
        set { expPorNivel = value; }
    }


    public float expProxNivel;

    public float ExpProxNivel
    {
        get {
            if (usarExperienciaCreciente)
            {
                int aux = (int)Mathf.Lerp(expPrimerNivel, expUltimoNivel, (float)progreso.nivel / (float)NivelMaximo);

                aux -= aux % 5;
                expProxNivel = aux;
                return aux;
            }
            else
            {
                expProxNivel = ExpPorNivel;
                return ExpPorNivel;
            }
        }
    }

    public bool EsNivelMaximo
    {
        get
        {
            if (progreso.nivel >= NivelMaximo)
                return true;
            else
                return false;
        }
    }

    [ShowIf("usarExperienciaCreciente")]
    public int expPrimerNivel = 1;
    [ShowIf("usarExperienciaCreciente")]
    public int expUltimoNivel = 100;
    public int NivelMaximo
    {
        get
        {
            return skins.diseños.Length - 1;
        }
    }

    public int NivelActual
    {
        get { return progreso.nivel; }
    }

    public Administrador_Skin skins;


    public float ExpActual
    {
        get { return progreso.Exp; }
        set { progreso.Exp = value; }
    }
    
    public int GetUltimoSkinID()
    {
        return progreso.skinsUnlockedIds[progreso.skinsUnlockedIds.Count - 1];
    }
    


    public List<int> ListaSkinsDesbloqueados
    {
        get { return progreso.skinsUnlockedIds; }
    }


    [Button]
    public void BorrarProgreso()
    {
        progreso.Exp = 0;
        progreso.skinsUnlockedIds.Clear();
        progreso.nivel = 0;
        progreso.partidas = 0;
        GuardarProgresoLocal();
    }

    public void CargarProgresoLocal()
    {
        if (AdministradorDeDatos.CargarDatos<Archivo_Progreso>(ref progreso, "archivo_Progreso"))
            Debug.Log("Progreso Cargado");
        else
            Debug.Log("Progreso NO pudo ser cargado");
    }

    public void GuardarProgresoLocal()
    {
        AdministradorDeDatos.GuardarDatos<Archivo_Progreso>(ref progreso, "archivo_Progreso");
        Debug.Log("Progreso Guardad con exito");
    }

    [Button]
    public void DesbloquearSkins()
    {
        for (int i = 1; i < skins.diseños.Length; i++)
        {
            if (!progreso.skinsUnlockedIds.Contains(i))
            {
                progreso.skinsUnlockedIds.Add(i);
            }
        }
    }

    [Button]
    public int DesbloquearSkinAleatorio()
    {
        if(skins.diseños.Length-1 <= progreso.skinsUnlockedIds.Count)
        {
            Debug.Log("Ya no quedan mas skins para desbloquear");
            Debug.Log("skins.diseños.Length:" + skins.diseños.Length);
            Debug.Log("progreso.skinsUnlockedIds:" + progreso.skinsUnlockedIds.Count);
            return -1;
        }
        Debug.LogWarning("skins.diseños.Length:"+ skins.diseños.Length);
        Debug.LogWarning("progreso.skinsUnlockedIds.Count" + progreso.skinsUnlockedIds.Count);

        List<int> list = GetSkinsFaltantes();             

        int indexNuevoSkin = Random.Range(1, list.Count);
        int idNuevoSkin = list[indexNuevoSkin];
        progreso.skinsUnlockedIds.Add(idNuevoSkin);

            
    


        GuardarProgresoLocal();
        return idNuevoSkin;
    }

  

    private List<int> GetSkinsFaltantes()
    {
        ///Armamos una lista para contener los skins que no tenemos
        List<int> idsFaltantes = new List<int>();

        for(int i = 0; i< skins.diseños.Length; i++)
        {
            if (!progreso.skinsUnlockedIds.Contains(i))
            {
                idsFaltantes.Add(i);
            }
        }

        return idsFaltantes;
    }




    /// <summary>
    /// Suma la experiencia obtenida y en caso de resultar en una suba de nivel se devuelve true
    /// </summary>
    /// <param name="exp"></param>
    /// <returns></returns>
    public bool SumarExperiencia(float exp, UI_BarraDeProgresion barra)
    {
        if (EsNivelMaximo)
            return false;

        Debug.Log("Sumando Experiencia: " + exp);


        float expIni = progreso.Exp;
        float expFinal = progreso.Exp + exp;
        float expSobra = 0;

        progreso.partidas++;
        progreso.Exp += exp;

        bool subeDeNivel = false;

        if(progreso.Exp >= ExpProxNivel)
        {
            DesbloquearSkinAleatorio();             //Desbloquea el skin
            expSobra = progreso.Exp - ExpProxNivel;
            progreso.Exp = 0;
            expFinal = ExpProxNivel;            
            subeDeNivel = true;
            configGen.SwipeReader_Desactivado = true;
            subidaDeNivel.Raise();
        }

        GuardarProgresoLocal();


        if (barra == null) barra = FindObjectOfType<UI_BarraDeProgresion>();

        if (barra != null)
        {
            barra.gameObject.SetActive(true);
            barra.Activar(expIni, expFinal, ExpProxNivel, expSobra);            
        }
        else if (expSobra != 0)                          //Si no hay animacion vuelve a ejecutar el codigo recursivamente para descontar exp y desbloqeuar skin hasta que ya NO sobre experienca     
            SumarExperiencia(expSobra, null);

        if (subeDeNivel)
        {
            Debug.Log("Nivel Subido");
            progreso.nivel++;
        }     
        

        return subeDeNivel;
    }
}
