﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;




[System.Serializable]
[CreateAssetMenu(fileName = "Idioma", menuName = "Configuracion/Configuracion Idioma")]
public class ConfiguracionIdioma : SerializedScriptableObject
{
    public bool debug;

    public SystemLanguage languageActual;


    private IdiomaPack activo;

    public IdiomaPack Activo
    {
        get { return activo; }
    }
    
    public List<IdiomaPack> idiomas;

    public void IdentificarLenguajeSistema()
    {
        if (!debug)
        {
            languageActual = Application.systemLanguage;
        }

        activo = idiomas.Find(x => x.id == languageActual);
        if (activo == null)
        {
            activo = idiomas.Find(x => x.id == SystemLanguage.English);
        }
    }
}

[System.Serializable]
public class IdiomaPack
{
    [Space(4)]
    public string name;
    public SystemLanguage id;

    public IdiomaString[] texts = new IdiomaString[]
    {
        new IdiomaString("yes",""),
        new IdiomaString("no",""),
        new IdiomaString("quit",""),
        new IdiomaString("back",""),
        new IdiomaString("forward",""),
        new IdiomaString("start",""),
        new IdiomaString("menu",""),
        new IdiomaString("options",""),
        new IdiomaString("classic",""),
        new IdiomaString("classicDesc",""),
        new IdiomaString("hardcore",""),
        new IdiomaString("hardcoreDesc",""),
        new IdiomaString("levels",""),
        new IdiomaString("levelsDesc",""),
        new IdiomaString("puzzle",""),
        new IdiomaString("puzzleDesc",""),
        new IdiomaString("warning_BetaModes",""),
    };

    public string GetText(string id)
    {
        for(int i = 0; i < texts.Length;i++)
        {
            if(texts[i].Id == id)
            {
                return texts[i].Text;
            }
        }
        return "Text not found";
    }
}

[System.Serializable]
public class IdiomaString
{

[SerializeField]
    private string id;
    public string Id
    {
        get { return id; }
    }

[SerializeField]
    [TextArea]
    private string text;
    public string Text
    {
        get { return text; }
    }


    public IdiomaString(string id, string text)
    {
        this.id = id;
        this.text = text;
    }
}