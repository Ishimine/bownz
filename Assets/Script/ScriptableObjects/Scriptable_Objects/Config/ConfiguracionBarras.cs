﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

[Serializable]
[CreateAssetMenu(fileName = "ConfigBarras", menuName = "Configuracion/ConfigBarras", order = 0)]
public class ConfiguracionBarras : SerializedScriptableObject {


    [Header("Propiedades")]

    [HideReferenceObjectPicker]
    [Tooltip("Cantidad de barras totales en el pool de barras")]
    public VariableReference<int> barrasTotales;

    [HideReferenceObjectPicker]
    [Tooltip("Cantidad De barras que pueden existir simultaneamente")]
    public VariableReference<int> barrasActivas;

    [Tooltip("longitudBarra[0] = Minimo, longitudBarra[1] = Maximo ,")]
    [HideReferenceObjectPicker]
    public VariableReference<Vector2> longitudBarra;

    [HideReferenceObjectPicker]
    [Tooltip("grosorBarra[0] = Minimo, grosorBarra[1] = Maximo ,")]
    public VariableReference<Vector2> grosorBarra;


    [Space(4)]

    [Header("Comportamientos")]

    [HideReferenceObjectPicker]
    [Tooltip("If True: las barras desaparecen al impacto")]
    public bool desaparecerPorContacto;

    [HideReferenceObjectPicker]
    [Tooltip("If True: las barras desaparecen al impacto")]
    public bool desaparecerPorDobleTap;

    [HideReferenceObjectPicker]
    [Tooltip("If True: las barras tambien hacen contacto con la parte transparente")]
    public bool usarContactoTransparente;

    [HideReferenceObjectPicker]
    public bool usarMultiplesBarras;

    [Tooltip("If True: las barras desaparecen luego del tiempo designado en 'tiempoPorBarra'")]
    public bool usarTiempoPorBarra = true;

    [HideReferenceObjectPicker]
    [Tooltip("Tiempo de vida de la barra hasta iniciar su Desaparicion")]
    [ShowIf("usarTiempoPorBarra")]
    public VariableReference<float> tiempoDeVidaPorBarra;

    [HideReferenceObjectPicker]
    public bool usarMouse;

    [HideReferenceObjectPicker]
    public bool usarVibracion;


    [ShowIf("usarVibracion")]
    public float vibrarAPartirDe = .8f;
    [ShowIf("usarVibracion")]
    /// <summary>   
    /// Convertir en long
    /// </summary>
    [SerializeField] private Vector2 fVibracion = new Vector2();
    
    public long[] FVibracion
    {
        get
        {
            long[] aux = new long[2];
            aux[0] = (long)fVibracion.x;
            aux[1] = (long)fVibracion.y;
            return aux;
        }
    }

    public bool usarRecurso = false;

    
    public bool usarEstiramientoFragil;


    /// <summary>
    /// 1 = 100% No puede ser menor que 1
    /// </summary>
    [ShowIf("usarEstiramientoFragil")]
    [SerializeField]private float puntoDeRuptura = 2f;

    public float PuntoDeRuptura
    {
        get
        {
            if (puntoDeRuptura < 1)
                return longitudBarra.Value.y;
            else
                return longitudBarra.Value.y*puntoDeRuptura;
        }
    }



}
