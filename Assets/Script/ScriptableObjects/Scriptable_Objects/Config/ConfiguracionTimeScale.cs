﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Sirenix.OdinInspector;
using System;


[Serializable]
[CreateAssetMenu(fileName = "ConfigTimeScale", menuName = "Configuracion/Configuracion TimeScale")]
public class ConfiguracionTimeScale : SerializedScriptableObject
{
    public void ResetearPasoActual()
    {
        PasoActual = 0;
    }

    [HideReferenceObjectPicker]
   [SerializeField]private VariableReference<float> timeScale_PasoActual;
    public float PasoActual
    {
        get { return timeScale_PasoActual.Value; }
        set { timeScale_PasoActual.Value = value; }
    }

    [HideReferenceObjectPicker]
    [SerializeField]
    private VariableReference<float> timeScale_Base;

    public float Base_Scale
    {
        get { return timeScale_Base.Value; }
        set { timeScale_Base.Value = value; }
    }

    /// <summary>
    /// Cantidad de pasos necesarios para llegar al maximo
    /// </summary>
    [HideReferenceObjectPicker]
    [SerializeField]
    private VariableReference<float> timeScale_PasosNecesarios;
    public float PasosNecesarios
    {
        get { return timeScale_PasosNecesarios.Value; }
        set { timeScale_PasosNecesarios.Value = value; }
    }

    [HideReferenceObjectPicker]
    [SerializeField]
    private VariableReference<float> timeScale_Maximo;
    public float Maximo_Scale
    {
        get { return timeScale_Maximo.Value; }
        set { timeScale_Maximo.Value = value; }
    }

    private VariableReference<float> timeScale_Actual;
    public float TimeScale_Actual
    {
        get { return timeScale_Actual.Value; }
        set { timeScale_Actual.Value = value; }
    }




}
