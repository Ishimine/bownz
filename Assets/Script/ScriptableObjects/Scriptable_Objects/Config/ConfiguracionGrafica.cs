﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;


[System.Serializable]
[CreateAssetMenu(fileName = "Graficos", menuName = "Configuracion/Configuracion Grafica")]
public class ConfiguracionGrafica : SerializedScriptableObject
{
    /// <summary>
    /// Activa Efectos de particulas
    /// </summary>
    [SerializeField] private bool usarParticulas;
    public bool UsarParticulas
    {
        get { return usarParticulas; }
        set { usarParticulas = value; }
    }

    [SerializeField] private bool usarTrail;
    public bool UsarTrail
    {
        get { return usarTrail; }
        set { usarTrail = value; }
    }

    /// <summary>
    /// Activa cambio de color de pantalla aleatorio
    /// </summary>
   [SerializeField] private bool usarFiltroColor;
    public bool UsarFiltroColor
    {
        get { return usarFiltroColor; }
        set { usarFiltroColor = value; }
    }


    public bool lPerf = false;
    /// <summary>
    /// El cambio se produce gradualmente
    /// </summary>
    [ShowIf("usarFiltroColor")]
    public bool usarLerpColor;
    /// <summary>
    /// Velocidad con la que se produce el cambio de color en segundos
    /// </summary>
    [ShowIf("usarLerpColor")]
    public float velLerpColor;

    /// <summary>
    /// IfTrue: Utiliza un fondo en base al diseño almacenado en el archivo "fondoActual.texture" 
    /// </summary>
    public bool usarBGDinamico;


    public bool UsarBGDinamico
    {
        get { return usarBGDinamico; }
        set { usarBGDinamico = value; }
    }

    /// <summary>
    /// IfTrue: Activa la animacion del movimiento para el fondo
    /// </summary>
    [ShowIf("usarBGDinamico")]
    public bool usarBGConMov;
    public bool UsarBGConMov
    {
        get { return usarBGConMov; }
        set { usarBGConMov = value; }
    }


    /// <summary>
    /// Velocidad con la que se mueve el fondo
    /// </summary>
    [ShowIf("usarBGConMov")]
    public float velMovBG;
    public float VelMovBG
    {
        get { return velMovBG; }
        set { velMovBG = value; }
    }

    /// <summary>
    /// IfTrue el cambio de direccion del fondo se hace gradualmente
    /// </summary>
    [ShowIf("usarBGConMov")]
    public bool usarLerpMov;
    public bool UsarLerpMov
    {
        get { return usarLerpMov; }
        set { usarLerpMov = value; }
    }

    /// <summary>
    /// Velocidad con la que se da el cambio de direccion del fondo
    /// </summary>
    [ShowIf("usarLerpMov")]
    public float velLerpMov = .5f;




    public void LowPerformance()
    {

        lPerf = !lPerf;

        UsarTrail = !UsarTrail;
        UsarParticulas = !UsarParticulas;
    }
}
