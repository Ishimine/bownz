﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;


[Serializable]
[CreateAssetMenu(fileName = "ConfigPelota", menuName = "Configuracion/ConfiguracionPelota")]
public class ConfiguracionPelota : SerializedScriptableObject
{
    public bool wrapX;
    public bool wrapY;

    [HideReferenceObjectPicker]
    [SerializeField] private VariableReference<float> fuerzaRebote;
    public float FuerzaRebote
    {
        get { return fuerzaRebote.Value; }
        set { fuerzaRebote.Value = value; }
    }



    [Tooltip("IfTrue: Cuando contacto un objeto sin propiedades de rebote, rebotada con la fuerza por default")]
    public bool rebotarSiempre;

    [HideReferenceObjectPicker]
    [SerializeField] private VariableReference<Sprite> diseño;

    public Sprite Diseño
    {
        get { return diseño.Value; }
        set { diseño.Value = value; }
    }

    [HideReferenceObjectPicker]
    [SerializeField] private VariableReference<float> tamaño;
    public float Tamaño
    {
        get { return tamaño.Value; }
        set { tamaño.Value = value;
            onChangeTamaño_Pelota.Raise();
        }
    }

    public GameEvent onChangeTamaño_Pelota;

    [HideReferenceObjectPicker]
    [SerializeField] private VariableReference<float> masa;
    public float Masa
    {
        get { return masa.Value; }
        set { masa.Value = value;
            onChangeMasa_Pelota.Raise(); }
    }

    public GameEvent onChangeMasa_Pelota;






}
