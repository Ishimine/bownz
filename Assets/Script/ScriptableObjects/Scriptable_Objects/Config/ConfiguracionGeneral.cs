﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.Audio;

[CreateAssetMenu(fileName = "ConfigGeneral", menuName = "Configuracion/General")]
public class ConfiguracionGeneral : SerializedScriptableObject {

    [SerializeField] private bool betaModesActive;
    public bool BetaModesActive
    {
        get
        {
            return betaModesActive;
        }
        set
        {
            betaModesActive = value;
        }
    }



    private bool touchReader = true;
    public bool TouchReader_Activado
    {
        get { return touchReader; }
        set { touchReader = value; }
    }

    [SerializeField]    private bool swipeReader = true;
    public bool SwipeReader_Activado
    {
        get { return swipeReader; }
        set {
            swipeReader = value; }
    }

    public bool SwipeReader_Desactivado
    {
        get { return !swipeReader; }
        set { swipeReader = !value; }
    }



    public AudioMixer mixer_Maestro;

   // [SerializeField] private string sonidoText;

    [SerializeField] private bool vibracion_Activada;
    public bool Vibracion
    {
        get { return vibracion_Activada; }
        set {
            vibracion_Activada = value;
            SetVibracion(value);
        }

    }

    [SerializeField] private bool musica_Activada;

    public bool Musica
    {
        get { return musica_Activada; }
        set { musica_Activada = value;
            SetMusica(value);
        }
    }

    [SerializeField] private bool sonido_Activo;

    public bool Sonido
    {
        get { return sonido_Activo; }
        set { sonido_Activo = value;
            SetSonido(value);
        }
    }

        
    [Button]
    public void ToggleSonido()
    {
        SetSonido(!sonido_Activo);
    }
    

    [Button]
    public void ToggleMusica()
    {
        SetMusica(!musica_Activada);
    }

    [Button]
    public void ToggleVibracion()
    {
        SetSonido(!sonido_Activo);
    }

    private void SetSonido(bool x)
    {
        if(x)
        {
            mixer_Maestro.SetFloat("VolMaster", 0);
        }
        else
        {
            mixer_Maestro.SetFloat("VolMaster", -80);
        }
    }

    private void SetMusica(bool x)
    {
        if (x)
        {
            mixer_Maestro.SetFloat("VolMusica", 0);
        }
        else
        {
            mixer_Maestro.SetFloat("VolMusica", -80);
        }
    }
   
    private void SetVibracion(bool x)
    {
        Vibration.isActive = x;
    }
}
