﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

/// <summary>
/// Objeto encargado del funcionamiento de una configuracion de modo, dentro del modo Survival
/// contiene los objetos necesarios para que un modo funciona y se comunica con el GameMode_Survival para instanciar y organizar los varios modos de juego sin repeticion
/// </summary>

[CreateAssetMenu(fileName = "surv_Config", menuName = "GameMode_Survival/Survival_Config")]
public class SurvivalConfig : SerializedScriptableObject
{
   

    public Id_Modo modo;
    public string id
    {
        get { return modo.Id; }
    }
    public string id_TablaPuntajes
    {
        get { return modo.Id_tabla; }
    }

    public bool usarRecurso;
    public bool wrapX;
    public bool wrapY;





    [SerializeField] private List<GameObject> prefabs;


    public List<GameObject> elementos;
    [HideInInspector]   public List<GameObject> prefabsOrdenados;

    public GameEvent endGame_Event;

   

    public List<GameObject> GetPrefabs()
    {
        return prefabs;
    }

    public void ActivarElementos()
    {        
        foreach(GameObject g in elementos)
        {
            if (g != null)
                g.SetActive(true);
        }
    }

    public void DesactivarElementos()
    {
        foreach (GameObject g in elementos)
        {
            if(g != null)            g.SetActive(false);
        }
    }

    public void InstanciarElementos()
    {
        LimpiarLista();
        foreach (GameObject g in prefabs)
        {            
            GameObject clone = Instantiate(g);
            elementos.Add(clone);
            clone.SetActive(false);
        }
    }
    
    public void LimpiarNulos()
    {

    }

    public void LimpiarLista()
    {
        foreach(GameObject g in elementos)
        {
            if (g == null) continue;
            Destroy(g.gameObject);
        }
        elementos.Clear();
    }


    public void InstanciarFaltantes()
    {
        //prefabsOrdenados.Clear();
        for (int i = 0; i < elementos.Count; i++)
        {
            if (elementos[i] == null)
            {
                elementos[i] = Instantiate<GameObject>(prefabsOrdenados[i]);
            }
        }
    }


}


