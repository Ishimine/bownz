﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "Administradores/AdministradorPuntaje")]
public class AdministradorPuntaje : SerializedScriptableObject
{
    public Administrador_Progreso progreso;


    [HideReferenceObjectPicker]
    public VariableReference<string> id_ModoActivo;
    [HideReferenceObjectPicker]
    public VariableReference<string> id_TablaActiva;
    [HideReferenceObjectPicker]
    public VariableReference<float> puntajeActual;
    [HideReferenceObjectPicker]
    public VariableReference<float> puntajeMaximo;

    public GameEvent puntajeModificado;

    
    [Button("Preparar")]
    public void PrepararPuntajes()
    {
        puntajeMaximo.Value = 0;
        CargarPuntajeMaximo();
        ResetearPuntajeActual();
    }

    [Button("Resetear")]
    public void ResetearPuntajeActual()
    {
        puntajeActual.Value = 0;
        puntajeModificado.Raise();
    }

    [Button("CargarPuntajeMaximo")]
    public void CargarPuntajeMaximo()
    {
        float aux = puntajeMaximo.Value;
        AdministradorDeDatos.CargarDatos<float>(ref aux, id_ModoActivo.Value + "Maximo");
        puntajeMaximo.Value = aux;
    }

    public static float GetPuntajeMaximo(string id_Modo)
    {
        float aux = 0;
        AdministradorDeDatos.CargarDatos<float>(ref aux, id_Modo + "Maximo");
        return aux;
    }


    [Button("GuardarPuntaje")]
    public void GuardarPuntaje()
    {
        if (progreso != null && !progreso.EsNivelMaximo) progreso.SumarExperiencia(puntajeActual.Value, null);
        else Debug.LogWarning("NO hay archivo de progreso");

        PlayServicesControl.AddScoreToLeaderBoard(id_TablaActiva.Value, (long)puntajeActual.Value);

        if (puntajeActual.Value > puntajeMaximo.Value)
        {
            float aux = puntajeActual.Value;
            AdministradorDeDatos.GuardarDatos<float>(ref aux, id_ModoActivo.Value + "Maximo");
        }
    }


    [Button("GuardarPuntajeForzado")]
    public void GuardarPuntajeForzado()
    {
        float aux = puntajeActual.Value;
        AdministradorDeDatos.GuardarDatos<float>(ref aux, id_ModoActivo.Value + "Maximo");
        
    }


    public void SumarPuntaje(float x)
    {
        puntajeActual.Value += x;
        puntajeModificado.Raise();
    }

    public void VaciarTodosLosPuntajes()
    {
    }


}
