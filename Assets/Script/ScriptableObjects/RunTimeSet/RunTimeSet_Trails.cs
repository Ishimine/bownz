﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;




[Serializable]
[CreateAssetMenu(fileName = "RunTimeSet_Trails", menuName = "RunTimeSet/RTS_Trails")]
public class RunTimeSet_Trails : RunTimeSet<ParticleTrail>
{

    public void AddById(ParticleTrail nuevo)
    {
        ParticleTrail t = Items.Find(x => x.id == nuevo.id);
        if (t == null)
        {
            Add(nuevo);
        }

    }


    public void RemoveById(string id)
    {
        Items.Remove(GetById(id));
    }

    public ParticleTrail GetById(string id)
    {
        return Items.Find(x => x.id == id);
    }



}




public class ParticleTrail
{
    public string id;
    public ParticleSystem particles;

    public ParticleTrail(string id, ParticleSystem particles)
    {
        this.id = id;
        this.particles = particles;
    }
}

