﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class AnimadorDeFondoGrid2 : SerializedMonoBehaviour {

    [HideReferenceObjectPicker]
    public VariableReference<Texture> diseño;

    public ConfiguracionGrafica config;

    //public  Material fondoAnimadoGrid;

    public Renderer render;

    public Vector4 velObj = new Vector4(5,5,5);
    public Vector4 anim = new Vector4(5,5,5);
    //  public float vel = 4;


    private IEnumerator rutina;

    public void Awake()
    {
        CambiarDireccion();
        CargarFondo();


    }
    public void CargarFondo()
    {
        //fondoAnimadoGrid.EnableKeyword("_MainTex");
        // fondoAnimadoGrid.SetTexture("_MainTex", diseño.Value);

        render.material.mainTexture = diseño.Value;
    }
    public void Update()
    {
        if (!config.usarBGDinamico && render.material.mainTexture != null)
        {
            Debug.Log("5555");
            render.material.mainTexture = null;
            return;
        }

        if (config.usarBGDinamico && render.material.mainTexture == null)
        {
            Debug.Log("6666");
            render.material.mainTexture = diseño.Value;
        }

        if(!config.usarBGConMov)
        {
            Debug.Log("7777");
            return;
        }

        //Debug.Log(Time.realtimeSinceStartup);

        render.material.EnableKeyword("_Tiempo");
        render.material.EnableKeyword("_ScrollSpeeds");

        render.material.SetFloat("_Tiempo", Time.realtimeSinceStartup/20);

        

       // render.material.SetVector("_ScrollSpeeds", Vector3.SmoothDamp(render.material.GetVector("_ScrollSpeeds"), velObj, ref vel, 0.3f));



        //Debug.Log(Time.realtimeSinceStartup);
        render.material.SetFloat("_Tiempo", Time.realtimeSinceStartup / 20);        

        anim += (Vector4)new Vector2( velObj.x, velObj.y) * Time.unscaledDeltaTime / 10;
        render.material.SetVector("_Animation", anim);
    }


    public void CambiarDireccion()
    {
        Vector4 ini = velObj;

        Vector2 a = new Vector2(Random.Range(0, 10), Random.Range(0, 10));
        a = a.normalized * config.velMovBG;
        if (Random.Range(0, 2) == 1)
            a.y *= -1;
        if (Random.Range(0, 2) == 1)
            a.x *= -1;

        if (config.usarLerpMov)
        {
            if (rutina != null) StopCoroutine(rutina);
            rutina = CambiarDireccionLerp(ini, a);
            StartCoroutine(rutina);
        }
        else
        {
            velObj = a;
        }
    }

    public IEnumerator CambiarDireccionLerp(Vector4 ini,Vector4 fin)
    {
        float a = 0;
        do
        {
            a += Time.fixedUnscaledDeltaTime / config.velLerpMov;
            velObj = Vector4.Lerp(ini, fin, a);
            yield return null;
        } while (a < 1);
    }
}
