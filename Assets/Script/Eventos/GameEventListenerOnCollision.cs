﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameEventListenerOnCollision : MonoBehaviour {

    public bool usarTag;
    [ConditionalHide("usarTag")]
    public string otherTag;

    public GameEvent respuesta;
    
    public void OnCollisionEnter2D(Collision2D collision)
{
        if (respuesta != null)
        {
            if (usarTag && collision.gameObject.tag == otherTag)
            {
                respuesta.Raise();
            }
            else if(!usarTag)
            {
                respuesta.Raise();
            }

        }
    }






}
