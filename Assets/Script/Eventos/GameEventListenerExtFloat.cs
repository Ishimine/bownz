﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class GameEventListenerExtFloat : GameEventListener
{
    public VariableFloat var;
    
    public UnityEventFloat newResponse;



    public override void OnEnable()
    {
        if(Event != null)
            Event.RegisterListener(this);
    }

    public override void OnDisable()
    {
        if(Event != null)
            Event.UnregisterListener(this);
    }

    public override void OnEventRaised()
    {
       /* if(Event != null)
            Debug.Log("                             " + Event.name);
        Debug.Log(gameObject + "                Listener:              IN");*/
        if (var != null)
            newResponse.Invoke(var.Value);
     /*   else
            Debug.Log("Variable de referencia nula");
        Debug.Log(gameObject + "                Listener:              OUT");*/
    }
}

