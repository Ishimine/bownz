﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameEventListenerIf : GameEventListener
{

 

    /// <summary>
    /// Se ejecuta si booleano es False;
    /// </summary>
    public UnityEvent ResponseFalse;

    [SerializeField] private bool booleano;

    public bool Value
    {
        get { return booleano; }
        set { booleano = value; }
    }



    public override void OnEventRaised()
    {
        if (booleano)
        {
            //Debug.Log("Value: " + booleano);
            Response.Invoke();
        }
        else
        {
            ResponseFalse.Invoke();
            //Debug.Log("Value: " + booleano);
        }
    }



}