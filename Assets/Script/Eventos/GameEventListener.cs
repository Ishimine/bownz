﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

public class GameEventListener : SerializedMonoBehaviour {

    public bool usarDescripcion;
    [ShowIf("usarDescripcion")]
    public string descripcion;

    public bool ocultarEvento;

    [HideIf("ocultarEvento")]
    public GameEvent Event;
    public bool ocultarRespuesta;
    [HideIf("ocultarRespuesta")]
    public UnityEvent Response;

    public virtual void OnEnable()
    { 
        if(Event != null)
            Event.RegisterListener(this);
    }

    public virtual void OnDisable()
    {
        if(Event != null)
            Event.UnregisterListener(this);
    }

    public virtual void OnEventRaised()
    {
        // if(Event != null) Debug.LogWarning("Evento Ejecutado: "+ Event.name  + "  en objeto:           " + gameObject.name);     
        if(Response != null)
            Response.Invoke();
    }
}
