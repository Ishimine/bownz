﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomPropertyDrawer(typeof(Touch))]
public class TouchPropertyDrawer : PropertyDrawer
{


    public override void OnGUI(Rect pos, SerializedProperty prop, GUIContent label)
    {
        EditorGUI.BeginProperty(pos, label, prop);
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;


        SerializedProperty phase = prop.FindPropertyRelative("phase");
        SerializedProperty fingerId = prop.FindPropertyRelative("fingerId");
        SerializedProperty tapCount = prop.FindPropertyRelative("tapCount");
        SerializedProperty position = prop.FindPropertyRelative("position");
        SerializedProperty rawPosition = prop.FindPropertyRelative("rawPosition");


        pos = EditorGUI.PrefixLabel(pos, GUIUtility.GetControlID(FocusType.Passive), label);

        var amountRect = new Rect(pos.x, pos.y, 30, pos.height);
        var unitRect = new Rect(pos.x + 35, pos.y, 50, pos.height);
        var nameRect = new Rect(pos.x + 90, pos.y, pos.width - 90, pos.height);



        EditorGUI.PropertyField(amountRect, phase, GUIContent.none);
        EditorGUI.PropertyField(unitRect, fingerId, GUIContent.none);
        EditorGUI.PropertyField(nameRect, tapCount, GUIContent.none);

        EditorGUI.PropertyField(unitRect, position, GUIContent.none);
        EditorGUI.PropertyField(nameRect, rawPosition, GUIContent.none);


        EditorGUI.indentLevel = indent;
    }


    public override float GetPropertyHeight(SerializedProperty prop,
                                             GUIContent label)
    {
        return base.GetPropertyHeight(prop, label);
    }
}

