﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class VFX_BrilloBorde : SerializedMonoBehaviour {


    public bool activado;
    public VariableReference<Direccion> dir;

    public GameMode_Survival gameMode;

    public float tEntreResplandor = .15f;
    public float velResplandor = 1;
    public float frecuencia;
    public Vector2 alphaMinMax = new Vector2(.1f,.2f);
    public Color c;
    public SpriteRenderer[] renders;

    public SpriteRenderer rArriba;
    public SpriteRenderer rAbajo;
    public SpriteRenderer rIzquierda;
    public SpriteRenderer rDerechar;

    public GameObject goHorizontal;
    public GameObject goVertical;

    public bool usarHorizontal;
    public bool usarVertical;


    public bool animLatente;

    IEnumerator rutina;

    public void Awake()
    {
        SetAlpha(alphaMinMax.x);       

    }

    public void ChequearEstado()
    {

        usarHorizontal = gameMode.actual.wrapX;
        usarVertical = gameMode.actual.wrapY;

        if (usarHorizontal)
            goHorizontal.SetActive(true);
        else
            goHorizontal.SetActive(false);


        if (usarVertical)
            goVertical.SetActive(true);
        else
            goVertical.SetActive(false);
    }

    public void Update()
    {
        if (!activado) return;

        if(animLatente || !Application.isPlaying)        SetAlpha(alphaMinMax.x + Mathf.Sin(Time.unscaledTime * frecuencia) * alphaMinMax.y);
                
    }
        
    public void OnValidate()
    {
        if(!Application.isPlaying)        AplicarColor();
    }

    public void AplicarColor()
    {
        foreach (SpriteRenderer r in renders)
        {
            r.color = new Color(c.r, c.g, c.b, alphaMinMax.x + alphaMinMax.y);
        }
    }

    public void SetAlpha(float alpha)
    {
        foreach(SpriteRenderer r in renders)
        {
            r.color = new Color(c.r, c.g, c.b, alpha);
        }
    }


    public void Resplandor()
    {
        if (!activado) return;
        Resplandor(dir.Value);
        Debug.Log("Resplandor, Dir: " + dir.Value);
    }

    public void Resplandor(Direccion ori)
    {
        if (rutina != null) StopCoroutine(rutina);
        switch (ori)
        {
            case Direccion.Arriba_Abajo:
                rutina = AnimResplandor(rArriba, rAbajo);
                break;
            case Direccion.Abajo_Arriba:
                rutina = AnimResplandor(rAbajo, rArriba);
                break;
            case Direccion.Izquierda_Derecha:
                rutina = AnimResplandor(rIzquierda, rDerechar);
                break;
            case Direccion.Derecha_Izquierda:
                rutina = AnimResplandor(rDerechar, rIzquierda);
                break;
            default:
                break;
        }
        StartCoroutine(rutina);
    }

    public IEnumerator AnimResplandor(SpriteRenderer rA, SpriteRenderer rB)
    {
        StartCoroutine(Resplandor(rA));
        yield return new WaitForSeconds(tEntreResplandor);
        StartCoroutine(Resplandor(rB));
    }

    private IEnumerator Resplandor(SpriteRenderer r)
    {
        r.color = new Color(c.r, c.g, c.b, alphaMinMax.y);        
        float t = 0;
        do
        {
            t += Time.fixedUnscaledDeltaTime / velResplandor;
            r.color = new Color(c.r, c.g, c.b, Mathf.Lerp(alphaMinMax.y, alphaMinMax.x, t));
            yield return null;
        } while (t < 1);

        r.color = new Color(c.r, c.g, c.b, alphaMinMax.x);
    }

}
