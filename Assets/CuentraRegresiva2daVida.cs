﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CuentraRegresiva2daVida : MonoBehaviour
{
    public Game_Cycle cycle;

    public Text txt;

    public float tiempoPredeterminado;

    public GameEvent segundaVidaPlay;

    IEnumerator rutina;

    public VariableFloat timeScaleInGame;

    public GameObject boton;

    public void Boton()
    {
        boton.SetActive(true);
    }


    public void ActivarCuentaRegresiva()
    {
        ActivarCuentaRegresiva(tiempoPredeterminado);
    }

    public void ActivarCuentaRegresiva(float x)
    {
        if (rutina != null) StopCoroutine(rutina);
        rutina = CuentaRegresiva(x);
        StartCoroutine(rutina);
    }

    IEnumerator CuentaRegresiva(float segundos)
    {
        float t = segundos+1;
        do
        {
            t -= Time.fixedUnscaledDeltaTime;
            txt.text = ((int)t).ToString();
            yield return null;
        } while (t > 1);

        txt.text = "";

        cycle.SetJuego_Pause(false);
        ActivarTimeScale();
    }


    public void ActivarTimeScale()
    {
        Time.timeScale = timeScaleInGame.Value;
    }

    public void Despausar(bool x)
    {
        if(!x)
        {

            ActivarCuentaRegresiva();
        }
    }

}
