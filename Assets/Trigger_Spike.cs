﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

public class Trigger_Spike : SerializedMonoBehaviour {

    public bool esEspecial = false;

    public bool usarColoracion = true;
    public bool usarEscala = true;

    public Color cEspecial;
    public Color cBrillo;
    public Color cActivado;
    public Color cDesactivado = Color.clear;

    public Vector2 id;

    /// <summary>
    /// En segundos
    /// </summary>
    public float tAnimacion;
    public float escalaInicial = 0;
    public float escalaFinal = 1;


    public Collider2D col;

    public SpriteRenderer render;

    IEnumerator rutina;


    public RunTimeSet_GameObject contactosHabilitados;

    public UnityEventVector2 respuestaDeContacto;
    public UnityEventVector2 respuestaDeContactoEspecial;



    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (contactosHabilitados.Contains(collision.gameObject))
        {
            if (esEspecial)
                respuestaDeContactoEspecial.Invoke(id);
            else
                respuestaDeContacto.Invoke(id);
        }
  
    }


    [Button]
    public void Activar()
    {     
        esEspecial = false;
        Activar(tAnimacion, false);
    }

    public void Activar(bool esEspecial)
    {
        if (esEspecial)
            ActivarEspecial(tAnimacion);
        else
            Activar();
    }

    public void ActivarNormal()
    {
        esEspecial = false;
        Activar(tAnimacion, false);
    }

    public void ActivarEspecial(float tiempo)
    {
        esEspecial = true;
        Activar(cDesactivado,cEspecial,Vector3.one * escalaInicial, Vector3.one * escalaFinal, tiempo);
    }

    public void Activar(float tiempo, bool especial)
    {
        esEspecial = false;

        if (especial)
            ActivarEspecial(tiempo);
        else
            Activar(cDesactivado, cActivado, escalaInicial * Vector3.one, escalaFinal * Vector3.one, tiempo);
    }


    public void Activar(Color colorInicial, Color colorFinal, Vector3 escalaInicial, Vector3 escalaFinal, float segundos)
    {
        gameObject.SetActive(true);
        col.isTrigger = false;
        IniciarAnimTransicion(colorInicial, colorFinal, escalaInicial, escalaFinal, segundos, false);
    }


    [Button]
    public void Desactivar()
    {
        Desactivar(tAnimacion);
    }

    public void Desactivar(float t)
    {
        Desactivar(render.color, cDesactivado, Vector3.one * escalaInicial, Vector3.one * escalaFinal, t);
    }

    public void Desactivar(Color colorInicial, Color colorFinal, Vector3 escalaInicial, Vector3 escalaFinal, float segundos)
    {
        esEspecial = false;
        if (!gameObject.activeSelf) return;
        col.isTrigger = true;
        IniciarAnimTransicion(colorInicial, colorFinal, escalaInicial,escalaFinal,segundos,true);

    
    }
    

   

   


    private void IniciarAnimTransicion(Color colorInicial, Color colorFinal, Vector3 escalaInicial, Vector3 escalaFinal, float segundos, bool desactivarAlFinal)
    {
        if (rutina != null) StopCoroutine(rutina);
        rutina = Transicion(colorInicial, colorFinal, escalaInicial,escalaFinal,segundos,desactivarAlFinal);
        StartCoroutine(rutina);
    }





    IEnumerator Transicion(Color colorInicial, Color colorFinal, Vector3 escalaInicial, Vector3 escalaFinal, float segundos, bool desactivarAlFinal)
    {
        float t = 0;
        transform.localScale = escalaInicial;


        render.color = colorInicial;

        float a;
        do
        {
            t += Time.deltaTime / segundos;

            a = Mathf.Lerp(0, 1, Mathf.Sin(Mathf.PI * (t / 2)));

            if (float.IsNaN(a))
                a = 0;

            if(usarEscala)
                transform.localScale = Vector3.Lerp(escalaInicial,escalaFinal,a);

            if(usarColoracion)
                render.color = Color.Lerp(colorInicial, colorFinal, a);

            yield return null;
        } while (t < 1);


        if(desactivarAlFinal)
        {
            gameObject.SetActive(false);
        }
    }






}
