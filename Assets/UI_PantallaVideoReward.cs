﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using UnityEngine.SocialPlatforms;

public class UI_PantallaVideoReward : SerializedMonoBehaviour {

    public ConfiguracionGeneral config;

    public GameObject contenedor;
    public Image play;
    public Image icono;
    public Text txt;

    public float tiempoCuentaRegresiva = 5;
    public float tiempoIconoPlay = .5f;

    IEnumerator rutina;

    public float probDeVideoRewardMax = 100;
    public float probDeVideoRewardActual = 0;
    public int pasosMaximosVideoReward = 14;
    public int pasoActual = 0;

    public GameEvent finDelJuego;
    public GameEvent videoRewardSucess;
    public GameEvent errorCargaDeVideo;

    private float timeScaleInGame;

    [Button]
    public void JugadorMuerto_PedirVideoReward()
    {
        if (!AdMobSingleton.instance.IsVideoRewardReady())
        {
            Cancelar();
            return;
        }
        pasoActual++;
        probDeVideoRewardActual = Mathf.Lerp(0, probDeVideoRewardMax, (float)pasoActual / (float)pasosMaximosVideoReward);

        float aux = UnityEngine.Random.Range(0, 100);

        Debug.Log("Aux :" + aux);

        if (aux > probDeVideoRewardActual)      //No se muestra posibilidad de VideoReward. 
        {
            Cancelar();
            return;
        }
        else
        {
            Activar();
        }
    }


    [Button]
    public void Activar()
    {
        pasoActual = 0;
        timeScaleInGame = Time.timeScale;
        Time.timeScale = 0;


        config.SwipeReader_Activado = false;

        if (rutina != null) StopCoroutine(rutina);
        rutina = CuentaRegresiva();
        StartCoroutine(rutina);

        contenedor.SetActive(true);

    }

    [Button]
    public void Desactivar()
    {
        config.SwipeReader_Activado = true;

        if (rutina != null) StopCoroutine(rutina);

        contenedor.SetActive(false);
    }   



    IEnumerator CuentaRegresiva()
    {
        float t = tiempoCuentaRegresiva- tiempoIconoPlay + 1;

        play.gameObject.SetActive(true);
        txt.text = "";

        yield return new WaitForSecondsRealtime(tiempoIconoPlay);

        play.gameObject.SetActive(false);
        do
        {
            t -= Time.unscaledDeltaTime;
            txt.text = ((int)(t - tiempoIconoPlay + 1)).ToString();
            yield return null;
        } while (t - tiempoIconoPlay > 0);

        Cancelar();
    }


    [Button]
    public void Aceptar()
    {
        if(rutina != null)        StopCoroutine(rutina);

        timeScaleInGame = Time.timeScale;
        Time.timeScale = 0;
            
        if (Application.isEditor)
        {
            videoRewardSucess.Raise();
            Desactivar();
        }
        else if (!AdMobSingleton.instance.ShowVideoReward())
        {
            errorCargaDeVideo.Raise();
            Cancelar();
        }
    }

    [Button]
    public void Cancelar()
    {
        if(rutina != null)        StopCoroutine(rutina);

        finDelJuego.Raise();
        Desactivar();
    }

    public void VideoRewordError()
    {
        finDelJuego.Raise();
        Desactivar();
    }

    

}
