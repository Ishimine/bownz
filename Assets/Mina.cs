﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[RequireComponent(typeof(Collider2D))]
public class Mina : SerializedMonoBehaviour, IItemSpawneable
{
    [Header("VFX Latido")]
    public bool usarVFXLatido = true;
    [ShowIf("usarVFXLatido")]
    public float escalaMin = .7f;
    [ShowIf("usarVFXLatido")]
    public float escalaMax = 1;

    [ShowIf("usarVFXLatido")]
    public bool usarFrecuenciaCreciente = true;

    [ShowIf("usarFrecuenciaCreciente")]
    public float frecuenciaInicial = 0;
    public float frecuenciaFinal = 5;

    [Header("Tiempos De Animacion")]
    public float tiempoIn = .5f;
    public float tiempoOut = .5f;
    public bool usarTiempoDeVida;
    [ShowIf("usarTiempoDeVida")]
    public float tiempoDeVida = 5f;

    [Header("VFX Brillo")]
    public bool usarBrillo = true;
    [ShowIf("usarBrillo")]
    [Range(0,1)]
    public float momentoDeBrillo;
    
    [ShowIf("usarBrillo")]
    public float frecuenciaBrillo = 3;

    public bool usarEscalaDesaparicion = true;

    public IEnumerator rutina;
    IEnumerator rutinaDestellos;


    [Header("Dependecias")]
    public Collider2D col;
    public GameObject cuerpo;
    public SpriteRenderer render;


    Color cOrig;

  /*  public GameEvent evento_MinaDesactivada;
    public VariableReference<IItemSpawneable> itemSpawneable;*/

   private void Awake()
    {
        cOrig = render.color;
        Activar();
    }

    IEnumerator TiempoDeVida(float x)
    {
        yield return new WaitForSeconds(x);
    }
    

    public IEnumerator Anim()
    {
        float t = 0;
        bool enAnimPreSalida = false;

        float frecuenciaActual = frecuenciaFinal;

        float anim = 0;

        do
        {
            t += Time.deltaTime / tiempoDeVida;

            if(usarVFXLatido)
            {
                if(usarFrecuenciaCreciente)
                {
                    frecuenciaActual = Mathf.Lerp(frecuenciaInicial, frecuenciaFinal, t);
                }
                anim += Time.deltaTime * frecuenciaActual * Mathf.PI;
                float aux = Mathf.Lerp(escalaMin, escalaMax, Mathf.Sin(anim));
                transform.localScale = Vector3.one  * aux;
            }



            if(!enAnimPreSalida && t >= momentoDeBrillo)
            {
                enAnimPreSalida = !enAnimPreSalida;
                if (rutinaDestellos != null) StopCoroutine(rutinaDestellos);
                rutinaDestellos = AnimDestellos((1 - t) * tiempoDeVida);
                StartCoroutine(rutinaDestellos);
            }

            yield return null;
        } while (t < 1);

        yield return new WaitForSeconds(.1f);
        AnimacionOut();

    }

    IEnumerator AnimDestellos(float x)
    {
       // float frec = frecuenciaBrillo + 1;

        float paso = x / frecuenciaBrillo;

        IEnumerator destello = Destello(paso);
        IEnumerator latido = Latido(paso);

        for (int i = 0; i < frecuenciaBrillo; i++)
        {
            if (usarBrillo)
            {
                Debug.Log("+++++++++++++++++++++++++++              Destello");
                if (destello != null) StopCoroutine(destello);
                destello = Destello(paso);
                StartCoroutine(destello);
            }
            if (usarEscalaDesaparicion)
            {
                Debug.Log("+++++++++++++++++++++++++++              Latido");
                if (latido != null) StopCoroutine(latido);
                latido = Latido(paso);
                StartCoroutine(latido);
            }

            yield return new WaitForSeconds(paso);
        }


  
    }

    IEnumerator Destello(float x)
    {
        float t = 0;
        render.color = Color.white;
        do
        {
            t += Time.deltaTime / x;
            render.color = Color.Lerp(Color.white, cOrig, t);
            yield return null;
        } while (t < 1);
    }

    IEnumerator Latido(float x)
    {
        float t = 0;
        Vector3 min = Vector3.one * escalaMin;
        do
        {
            t += Time.deltaTime / x;
            transform.localScale = Vector3.Lerp(min, Vector3.one, t);
            yield return null;
        } while (t < 1);
    }







    public void SetParent(Transform parent)
    {
        transform.SetParent(parent);
    }

    public virtual void Activar()
    {
        gameObject.SetActive(true);
        Debug.Log("ACTIVADO---------------------------------------------------------------------");
        if (rutina != null) StopCoroutine(rutina);
        rutina = AnimIn();
        StartCoroutine(rutina);
    }

    public virtual void Activar(float tDeVida)
    {
        tiempoDeVida = tDeVida;
        Activar();
    }

  public  IEnumerator AnimIn()
    {
        cuerpo.SetActive(true); 
        float t = 0;

        do
        {
            t += Time.fixedUnscaledDeltaTime / tiempoIn;
            transform.rotation = Quaternion.Lerp(Quaternion.identity, Quaternion.Euler(0, 0, 720), t);
            transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, t);

            yield return null;

        } while (t < 1);

        if (usarTiempoDeVida)
        {
        Debug.Log("TIMEPO DE VIDA---------------------------------------------------------------------");
            rutina = Anim();
            StartCoroutine(rutina);
        }
    }

    public void AnimacionOut()
    {
        gameObject.SetActive(true);
        if (rutina != null) StopCoroutine(rutina);
        rutina = AnimOun();
        StartCoroutine(rutina);

        if (rutinaDestellos != null) StopCoroutine(rutinaDestellos);
    }

    public virtual void Desactivar()
    {
        gameObject.SetActive(false);
    }


    IEnumerator AnimOun()
    {       
        float t = 1;
        do
        {
            t -= Time.fixedUnscaledDeltaTime / tiempoOut;
            transform.rotation = Quaternion.Lerp(Quaternion.identity, Quaternion.Euler(0, 0, 360), t);
            transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, t);

            yield return null;

        } while (t > 0);


        /*     itemSpawneable.Value = this;
             evento_MinaDesactivada.Raise();*/

        AutoDesactivacion();
    }

    public virtual void AutoDesactivacion()
    {
        Desactivar();
    }

    public bool IsActive()
    {
        return gameObject.activeSelf;
    }
}
