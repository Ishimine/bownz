﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class UI_PantallaPremio : MonoBehaviour {

    public Administrador_Progreso progreso;
    public Administrador_Skin skins;

    public GameObject pivot;

    public Image brillo;
    public Image diseño;
    public Image sombra;
    public Image cuerpo;

    public GameObject fondo;

    public GameObject luces;
    //public GameObject botonContinue;


    public UnityEvent respuestaPostAnimacion;

    public float vel = .7f;

    IEnumerator rutina;

    public void Activar()
    {
        Debug.Log("AAA");
        int id = progreso.GetUltimoSkinID();

        Debug.Log("id: " + id);
        PelotaSkin d = skins.GetDiseño(id);

        Debug.Log("PelotaSkin" + d);

        brillo.sprite = d.Brillo;
        diseño.sprite = d.Diseño;

        diseño.color = d.cDiseño;

        sombra.sprite = d.Sombra;

        cuerpo.sprite = d.Cuerpo;

        cuerpo.color = d.cCuerpo;

        if (diseño.sprite == null)
            diseño.color = new Color(diseño.color.r, diseño.color.g, diseño.color.b, 0);
        else
            diseño.color = new Color(diseño.color.r, diseño.color.g, diseño.color.b, .5f);

        if (cuerpo.sprite == null)
            cuerpo.color = Color.clear;

        if (sombra.sprite == null)
            sombra.color = Color.clear;
        else
            sombra.color = new Color(0, 0, 0, .2f);


        if (brillo.sprite == null)
            brillo.color = Color.clear;
        else
            brillo.color = Color.white;

        Debug.Log("A");

        if (rutina != null) StopCoroutine(rutina);

        rutina = AnimacionIn();
        StartCoroutine(rutina);
        Debug.Log("B");
    }


    IEnumerator AnimacionIn()
    {
       // botonContinue.SetActive(false);
        float t = 0;
        Debug.Log("C");

        fondo.SetActive(true);

        do
        {
            t += Time.fixedUnscaledDeltaTime / vel;
            pivot.transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, t);
            pivot.transform.rotation = Quaternion.Lerp(Quaternion.Euler(0, 0, -360), Quaternion.identity, t);

            luces.transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, t);

            yield return null;
        } while (t < 1);

        Debug.Log("D");

        yield return new WaitForSecondsRealtime(3.75f);
       // botonContinue.SetActive(true);
        respuestaPostAnimacion.Invoke();

    }


}
