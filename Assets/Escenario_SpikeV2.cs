﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;


public class Escenario_SpikeV2 : SerializedMonoBehaviour
{


    [SerializeField] private VariableReference<Vector2> id_TriggerActivado;

    public Vector2 areaDeJuego; 
    public Vector2 centroAreaDeJuego;




    public GameObject prefabTrigger;
    public SpriteRenderer prefabTriggerRender;
    public GameObject prefabSpike;

    public Transform[] paredes;
    public Trigger_Spike[,] triggers;



    public int triggersPorPared;
    public int spikesPorPared;

    [SerializeField] private Vector2 dimensionesPorPieza;


   [SerializeField] private VariableReference<float> triggerTamaño;

    public int TriggerTamaño
    {
        get { return (int)triggerTamaño.Value; }
    }

    public int triggerTamañoMin = 2;



    public int triggerCentral;


    [Header("Animacion")]
    public float tTransicion;
    public float tSolapamiento = .1f;

    IEnumerator rutinaIzquierda;
    IEnumerator rutinaDerecha;



    public void TriggerActivado(bool esEspecial)
    {
        TriggerActivado(id_TriggerActivado.Value);
    }

    public void TriggerActivado(Vector2 id)
    {
        Siguiente();
    }


    void ActivarPared(Vector2 id, float segundos)
    {
        Debug.Log("TriggerTamaño: " + TriggerTamaño);
        Debug.Log("triggerTamañoMin: " + triggerTamañoMin);


        bool especial = TriggerTamaño <= triggerTamañoMin;

        Debug.Log("especial: " + especial);


        if (id.x == 0)
        {
            if (rutinaIzquierda != null) StopCoroutine(rutinaIzquierda);
            rutinaIzquierda = ActivarParedAnim(id, segundos, especial);
            StartCoroutine(rutinaIzquierda);
        }
        else
        {
            if (rutinaDerecha != null) StopCoroutine(rutinaDerecha);
            rutinaDerecha = ActivarParedAnim(id, segundos, especial);
            StartCoroutine(rutinaDerecha);
        }
    }
    

    IEnumerator ActivarParedAnim (Vector2 id, float segundos, bool especial)
    {
        Debug.Log("Activando Pared ID: " + id);



        int pared = (int)id.x;
        int centro = (int)id.y;

        float paso = segundos / TriggerTamaño;



        triggers[pared, centro].Activar(paso + tSolapamiento,especial);


        int aux = 1;

        yield return new WaitForSeconds(paso);

        for (int i = 1; i < triggers.GetLength(1); i++)
        {
            if (aux >= TriggerTamaño) break;

            if (centro + i < triggers.GetLength(1))
            {
                    triggers[pared, centro + i].Activar(paso + tSolapamiento, especial);
                    aux++;
            }


            if (aux >= TriggerTamaño) break;
            if (centro - i >= 0)
            {
                triggers[pared, centro - i].Activar(paso + tSolapamiento, especial);
                aux++;
            }

            yield return new WaitForSeconds(paso);
        }


        Debug.Log("Activados " + aux + " paneles");

    }
      


    private void Awake()
    {
        InstanciarParedes();
    }

    public void Activar()
    {
        DesactivarTodos();
        Siguiente();
    }

    public void DesactivarTodos()
    {
        DesactivarPared(0);
        DesactivarPared(1);
    }

    public void DesactivarPared(int idPared)
    {
        for(int i = 0; i < triggers.GetLength(1);i++)
        {
            triggers[idPared, i].Desactivar();
        }
    }

    public void DesactivarPared(Vector2 id, float tTransicion)
    {
        if(id.x == 0)
        {
            if (rutinaIzquierda != null) StopCoroutine(rutinaIzquierda);
            rutinaIzquierda = DesactivarParedAnim(id, tTransicion);
            StartCoroutine(rutinaIzquierda);
        }
        else
        {
            if (rutinaDerecha != null) StopCoroutine(rutinaDerecha);
            rutinaDerecha = DesactivarParedAnim(id, tTransicion);
            StartCoroutine(rutinaDerecha);
        }
    }

    IEnumerator DesactivarParedAnim(Vector2 id, float tiempo)
    {
        float paso = tiempo / triggers.GetLength(1);

        for (int i = 0; i<triggers.GetLength(1); i++)
        {
            int pId = (int)id.y + i;

            if (pId < triggers.GetLength(1))
            {
                triggers[(int)id.x, pId].Desactivar(paso + tSolapamiento);                
            }

            pId = (int)id.y - i;
            if(pId >= 0)
            {
                triggers[(int)id.x, pId].Desactivar(paso + tSolapamiento);
            }
            yield return new WaitForSeconds(paso);
        }
    }


    [Button]
    public void Siguiente()
    {
        Siguiente(id_TriggerActivado.Value);
    }


    public void Siguiente(Vector2 id)
    {

        Debug.LogWarning("id_TriggerActivado: " + id_TriggerActivado.Value);

        //Animacion transicion pared desactivada
        DesactivarPared(id, tTransicion * triggersPorPared);


        int rangoDesplazamiento = TriggerTamaño / 2; 
     //   triggerCentral = Random.Range(0, triggersPorPared - rangoDesplazamiento /2 ) - ((triggersPorPared / 2 ) - rangoDesplazamiento / 2);

        triggerCentral = Random.Range(0, (triggersPorPared  / 2) - rangoDesplazamiento) + (triggersPorPared / 2);


        int pared = Random.Range(0, 2);

        /*    Debug.Log("                                     Trigger Activado :" + pared + "," + triggerCentral);
            Debug.Log("                     TriggerTamaño :" + triggerTamaño);
            Debug.Log("                     RangoDesplazamiento :" + rangoDesplazamiento);
            Debug.Log("                     -----------------------------------------                     ");*/

        DesactivarPared(pared);

        ActivarPared(new Vector2(pared,triggerCentral), tTransicion * triggersPorPared);

    }


    public void ActivarTrigger(int centro, int tamaño)
    {
        int desplazamiento = (tamaño / 2 ) * -1; 

        for(int i = desplazamiento; i < tamaño; i++)
        {
            for(int j = 0; j < 2; j++)
            {
                triggers[j,i].Activar();
            }
        }

    }




    #region CreacionParedes

    [Button]
    public void CalcularElementosPorPared()
    {
        dimensionesPorPieza = prefabTriggerRender.sprite.bounds.size;
        triggersPorPared = (int)(areaDeJuego.y * 2 / dimensionesPorPieza.y);
        spikesPorPared = triggersPorPared + 1;
    }

    [Button]
    public void InstanciarParedes()
    {
        paredes[0].transform.position = centroAreaDeJuego;
        paredes[1].transform.position = centroAreaDeJuego;
        paredes[0].transform.rotation = transform.rotation;
        paredes[1].transform.rotation = transform.rotation;



        DestruirElementos();
        CalcularElementosPorPared();

        
        //Calcular PosSpawnInicial
        float y = dimensionesPorPieza.y * triggersPorPared / 2;
        Vector3 primerSpawnPos = Vector3.down * y + (Vector3)centroAreaDeJuego;

        triggers = new Trigger_Spike[2, triggersPorPared];

        //Instanciamos los Triggers
        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < triggersPorPared; j++)
            {
                GameObject clone = Instantiate<GameObject>(prefabTrigger, paredes[i]);
                clone.transform.position = primerSpawnPos + j * dimensionesPorPieza.y * Vector3.up;
                clone.transform.rotation = Quaternion.Euler(0, 0, 180);
                triggers[i, j] = clone.GetComponent<Trigger_Spike>();

                triggers[i, j].id = new Vector2(i, j);
            }
        }

        primerSpawnPos += Vector3.down * dimensionesPorPieza.y * .5f;

        //Spawneamos los pinches
        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < spikesPorPared; j++)
            {
                GameObject clone = Instantiate<GameObject>(prefabSpike, paredes[i]);

                clone.transform.position = primerSpawnPos + j * dimensionesPorPieza.y * Vector3.up;
            }
        }


        //Giramos y posicionamos la pared derecha
        paredes[1].transform.position += Vector3.right * (centroAreaDeJuego.x - primerSpawnPos.x);
        paredes[1].transform.rotation = Quaternion.Euler(0, 0, 180);




        PosicionarParedes();
    }

    public void PosicionarParedes()
    {
        paredes[0].position = areaDeJuego.x * Vector3.left + (Vector3)centroAreaDeJuego + (Vector3)dimensionesPorPieza / 2;
        paredes[1].position = areaDeJuego.x * Vector3.right + (Vector3)centroAreaDeJuego - (Vector3)dimensionesPorPieza / 2;
    }

    [Button]
    public void DestruirElementos()
    {
        DestruirHijos(paredes[0]);
        DestruirHijos(paredes[1]);
    }

    void DestruirHijos(Transform t)
    {
        for (int i = t.childCount - 1; i >= 0; i--)
        {
            if (Application.isPlaying)
            {
                Destroy(t.GetChild(i).gameObject);
            }
            else
            {
                DestroyImmediate(t.GetChild(i).gameObject);
            }
        }
    }

   
    #endregion

}
