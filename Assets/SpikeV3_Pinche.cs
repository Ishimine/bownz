﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeV3_Pinche : SpikeV3_Trigger {


    public override void Activar(float t, bool esEspecial)
    {
        if (esEspecial)
            base.ActivarEspecial(t);
        else
            base.ActivarNormal(t);

        col.isTrigger = true;
    }


    public override void Desactivar(float t, bool contacto)
    {
        base.Activar(render.color, cDesactivado, Vector3.one, Vector3.one, t);
        col.isTrigger = true;
    }


}
