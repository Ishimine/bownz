﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.UI;

public class UI_HighscorePlayer : MonoBehaviour {

    public GameObject contenedor;
    public Text txt;

    public VariableString id_TablaPuntajes;

    public LeaderboardTimeSpan timeSpan;


    public string bestPlayerId;

    public void PedirActualizacion()
    {
            contenedor.SetActive(false);
        if (Social.localUser.authenticated)
        {
            PlayServicesControl.GetScores(id_TablaPuntajes.Value, timeSpan);
        }
    }


    public void CheckIfYouAreTheBest()
    {
        if(bestPlayerId == Social.Active.localUser.id)
        {
            PlayServicesControl.UnlockAchivement(GPGSIds.achievement_the_best);
        }
    }

    public void ActualizacionLista()
    {
        contenedor.SetActive(true);
        IScore score = PlayServicesControl.extractedData.Scores[0];

        string[] ids = new string[1];
        ids[0] = score.userID;

        bestPlayerId = ids[0];

        Social.LoadUsers(ids,
         (data) =>
         {
             txt.text = data[0].userName + " " + score.formattedValue;             
         });



        CheckIfYouAreTheBest();
    }
}
