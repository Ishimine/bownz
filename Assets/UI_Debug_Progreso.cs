﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Debug_Progreso : MonoBehaviour {

    public Administrador_Progreso progreso;

    public Slider expActual;

    public Slider nivelActual;

    public float GetExperienciaActual()
    {
        return progreso.ExpActual;
    }
    
    public void BorrarProgreso()
    {
        progreso.BorrarProgreso();
        Actualizar();
    }

    public void Actualizar()
    {
        expActual.value = GetExperienciaActual();
        nivelActual.value = progreso.NivelActual;
    }

    public void OnEnable()
    {
        Actualizar();
    }

}
