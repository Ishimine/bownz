﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class UI_LuzFx : SerializedMonoBehaviour {

    public static float velGlobal = 1.1f;

    public Image render;

    [SerializeField] private bool activado;
    public bool Activado
    {
        get { return activado; }
        set { activado = value; }
    }


    public float velGiro = 360;
    public float offset;


    [SerializeField] private bool girar;
    public bool Girar
    {
        get { return girar; }
        set { girar = value; }
    }




    [Button]
    public void ActivarGiro()
    {
        Girar = true;
    }

    [Button]
    public void DesactivarGiro()
    {
        Girar = false;
    }
    [Button]
    public void Prender()
    {
        Activado = true;
    }

    [Button]
    public void Apagar()
    {
        Activado = false;
    }

    public void Awake()
    {
        offset += transform.rotation.z; 
    }

    public void Update()
    {
        if (!activado) return;

        if(girar)
        {
            transform.rotation = Quaternion.Euler(0, 0, (Time.realtimeSinceStartup * velGlobal * velGiro + offset ) % 360);
        }
    }

    [Button]
    public void AumentarVelocidad()
    {
        velGlobal += .1f;

        Debug.Log("Vel Luces actual = " + velGlobal);
    }



}
