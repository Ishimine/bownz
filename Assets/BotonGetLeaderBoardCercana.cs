﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using UnityEngine.SocialPlatforms;

public class BotonGetLeaderBoardCercana : SerializedMonoBehaviour {

    public Text txt;

    public VariableReference<string> id_TablaModoActual;

    private void OnValidate()
    {
         if (txt == null)
        {
            txt = GetComponent<Text>();
        }
    }


    public void Presionado_LeaderBoardStatus()
    {
        txt.text = PlayServicesControl.GetLeaderBoardStatus(id_TablaModoActual.Value,GooglePlayGames.BasicApi.LeaderboardTimeSpan.AllTime);
    }

    public void Presionado_RecordActual()
    {
        IScore score = PlayServicesControl.GetFirstPlace(id_TablaModoActual.Value, GooglePlayGames.BasicApi.LeaderboardTimeSpan.AllTime);
        

        txt.text = score.userID + " " + score.value;
    }

    public void Presionado_Player()
    {
        IScore score = PlayServicesControl.GetNextPlace(id_TablaModoActual.Value, GooglePlayGames.BasicApi.LeaderboardTimeSpan.AllTime);

       
        txt.text = score.userID + " " + score.value;
    }

    public void Presionado_CodManual()
    {       

        txt.text = PlayServicesControl.GetPlayersIDs(id_TablaModoActual.Value, GooglePlayGames.BasicApi.LeaderboardTimeSpan.AllTime);
    }
}
