﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class EarlyAdopterCheck : MonoBehaviour {

    public Administrador_Progreso progreso;
    

    public void Aplicar()
    {
        if(progreso.isEarlyAdopter && Social.localUser.authenticated)
        {
            PlayServicesControl.UnlockAchivement(GPGSIds.achievement_early_adopter);
        }
    }
}
