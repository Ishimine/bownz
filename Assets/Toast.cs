﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Toast : MonoBehaviour {

    public Image fondo;
    public Text txt;
    public float duracion;

    Color cFondo;
    Color cTxt;

    IEnumerator rutina;

    private void Awake()
    {
        cFondo = fondo.color;
        cTxt = txt.color;
    }

    public void Activar()
    {
        if (rutina != null) StopCoroutine(rutina);
        rutina = Animacion();
        StartCoroutine(rutina);
    }

    IEnumerator Animacion()
    {
        fondo.gameObject.SetActive(true);
        float t = 0;
        do
        {
            t += Time.unscaledDeltaTime / duracion;            
            fondo.color = Color.Lerp(cFondo, Color.clear, Mathf.Sin(t * Mathf.PI));
            txt.color = Color.Lerp(cTxt, Color.clear, Mathf.Sin(t * Mathf.PI));
            yield return null;
        } while (t < 1);
        fondo.gameObject.SetActive(false);
    }




}
