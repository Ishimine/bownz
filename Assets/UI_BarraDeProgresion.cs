﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class UI_BarraDeProgresion : MonoBehaviour {

    public AudioSource audioPlayer;
    public bool animarDesaparicion = false;

    public Administrador_Progreso progreso;
    public IEnumerator rutina;

    public Slider slider;

    public Image rRelleno;
    public Color cRellenoNormal = Color.magenta;
    public Color cRellenoSubidaDeNivel = Color.yellow;


    public float tBrillo = .5f;
    public float vel = .7f;
    public float tVelSubidaNivel = .7f;

    [Header("Debug")]
    public bool usarDebug = false;
    public GameObject textDebug;
    public Text expActual;
    public Text expProxNivel;



    public GameEventListener apagarTouch;


    private void Awake()
    {
        if(usarDebug)
        {
            textDebug.SetActive(true);
        }
        else
        {
            textDebug.SetActive(false);
        }
    }

    public void OnValidate()
    {
        if(rRelleno != null)
            rRelleno.color = cRellenoNormal;
    }


    public GameEvent animacionFinDeNivel;


    [Button]
    public void  ActivacionDeDebug()
    {
    //    Activar(expInicial, expFinal, expMax, expSobrante);
    }

  

    public void Activar(float expInicial, float expMax, float expFinal)
    {
        Activar(expInicial, expFinal, expMax, 0);
    }

    /// <summary>
    /// Si no hay subida de nivel expSobrante == -1
    /// </summary>
    /// <param name="expInicial"></param>
    /// <param name="expFinal"></param>
    /// <param name="expSobrante"></param>
    public void Activar(float expInicial, float expFinal, float expMax, float expSobrante)
    {   
        slider.gameObject.SetActive(true);

        if (rutina != null) StopCoroutine(rutina);

        rutina = AnimarCarga(expInicial, expFinal, expMax, expSobrante);

        StartCoroutine(rutina);
    }


    IEnumerator AnimarCarga(float expInicial, float expFinal, float expMax, float expSobrante)
    {
        bool ok = false;
        if (expFinal == expMax)
        {
            apagarTouch.OnEventRaised();

            ok = true;
        }

        transform.localScale = Vector3.one;

        float inicial =   expInicial / expMax;
        float final = expFinal / expMax;

        slider.value = inicial;
        float t = 0;


        if (usarDebug)
        {
            expActual.text = expInicial.ToString("0");
            expProxNivel.text = expMax.ToString("0");
        }

        do
        {

            t += Time.fixedUnscaledDeltaTime / vel;
            slider.value = Mathf.Lerp(inicial,final, t);

            if (usarDebug)
            {
                expActual.text = Mathf.Lerp(expInicial, expFinal, t).ToString("0");
            }

            if(ok)
            {
                apagarTouch.OnEventRaised();
            }

            yield return null;
        } while (t < 1);
        


        if (expFinal == expMax)
        {
            rutina = AnimarSubidaDeNivel(expSobrante);
            StartCoroutine(rutina);
        }
        else
        {
            yield return new WaitForSecondsRealtime(1f);
            if (animarDesaparicion)
            {
                animacionFinDeNivel.Raise();
                rutina = Desaparecer(1.5f);
                StartCoroutine(rutina);
            }
            else
            {
                transform.localScale = Vector3.zero;
            }
            StartCoroutine(rutina);
        }

       
    }




    IEnumerator Desaparecer(float x)
    {
        float t = 0;
        Vector3 dimFinal = new Vector3(1, 0, 1);


        do
        {
            t += Time.fixedUnscaledDeltaTime * x;
            transform.localScale = Vector3.Lerp(Vector3.one, dimFinal, t);         


            yield return null;
        } while (t < 1);
    }


    IEnumerator Aparecer(float x)
    {
        float t = 0;
        Vector3 dimInicial = new Vector3(1, 0, 1);
        do
        {
            t += Time.fixedUnscaledDeltaTime * x;
            transform.localScale = Vector3.Lerp(dimInicial, Vector3.one, t);
            yield return null;
        } while (t < 1);
    }

    IEnumerator AnimarSubidaDeNivel(float expSobrante)
    {
        /*  if(usarDebug)
          {
              expActual.text = "0";
              expProxNivel.text = progreso.ExpProxNivel.ToString("0");
          }*/

        audioPlayer.Play();
        rRelleno.color = cRellenoSubidaDeNivel;
        slider.value = 1;
        float t = 0;
        do
        {
            t += Time.fixedUnscaledDeltaTime / tBrillo;
            rRelleno.color = Color.Lerp(cRellenoSubidaDeNivel, Color.clear, t);

           

            yield return null;
        } while (rRelleno.color != Color.clear);

        t = 0;
        do
        {
            t += Time.fixedUnscaledDeltaTime / tVelSubidaNivel;
            // slider.value = Mathf.Lerp(1, 0, t);
            yield return null;  
        } while (t < 1);

        rRelleno.color = cRellenoNormal;
        slider.value = 0;









        if (animarDesaparicion)
        {
            rutina = Desaparecer(2f);
            StartCoroutine(rutina);
        }
        else
        {
            transform.localScale = Vector3.zero;
        }

        animacionFinDeNivel.Raise();
        progreso.SumarExperiencia(expSobrante, this);


    }





}
