﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_MenuSkins : MonoBehaviour {

    public Administrador_Progreso progreso;
    
    public void ChequearEstado()
    {
        if (progreso.HasSkins)        
            gameObject.SetActive(true);        
        else
            gameObject.SetActive(false);        
    }


    public void OnEnable()
    {
        ChequearEstado();
    }

}
