﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[System.Serializable]
public class C_NumeroCreciente : SerializedMonoBehaviour {


    public Variable<float> valorActual;

    public bool devolverEnteros;

    /// <summary>
    /// IfTrue: Una vez que se llega al peso maximo, se volvera al primer paso
    /// </summary>
    public bool usarCrecimientoCiclico = false;




    public bool usarCurvaDeCrecimiento;
    [ShowIf("usarCurvaDeCrecimiento")]
    public AnimationCurve curvaDeCrecimiento;



    public bool usarCrecimientoGradual;

    [ShowIf("usarCrecimientoGradual")]
    public float tiempoDeCrecimientoGradual = 300;

    [HideIf("usarCrecimientoGradual")]
    public int pasosMaximo;
    [HideIf("usarCrecimientoGradual")]
    public int pasosActual;
    
    public float valorInicial;
    public float valorFinal;

    void Awake()
    {
        Resetear();
    }

   public void Iniciar()
    {
        if(usarCrecimientoGradual)
        {
            StartCoroutine(CrecimientoGradual());
        }
    }

    IEnumerator CrecimientoGradual()
    {
        float t = 0;
        do
        {
            t += Time.deltaTime / tiempoDeCrecimientoGradual;


            /*if (usarCurvaDeCrecimiento)
            {
                valorActual.Value = valorFinal / tiempoDeCrecimientoGradual;
            }
            else
            {*/
                valorActual.Value = valorFinal / tiempoDeCrecimientoGradual;
          //  }

            yield return null;
        } while (t < tiempoDeCrecimientoGradual);

        if(usarCrecimientoCiclico)
        {
            StartCoroutine(CrecimientoGradual());
        }
    }

    public void Resetear()
    {
        if(devolverEnteros)
            valorActual.Value = (int)valorInicial;
        else
            valorActual.Value = valorInicial;
        pasosActual = 0;
    }


    public void Siguiente()
    {
        pasosActual++;

        if(usarCrecimientoCiclico && pasosActual > pasosMaximo)
        {
            pasosActual = 0;
        }

        //      Debug.Log(pasosActual+"/"+ pasosMaximo + ":" + pasosActual / pasosMaximo);

        if (usarCurvaDeCrecimiento)
        {
            valorActual.Value = Mathf.Lerp(valorInicial, valorFinal, curvaDeCrecimiento.Evaluate((float)pasosActual / (float)pasosMaximo));
        Debug.Log("CON Curva " + Mathf.Lerp(valorInicial, valorFinal, curvaDeCrecimiento.Evaluate((float)pasosActual / (float)pasosMaximo)));
        }
        else
        {
            valorActual.Value = Mathf.Lerp(valorInicial, valorFinal, (float)pasosActual / (float)pasosMaximo);
        Debug.Log("SIN Curva " + Mathf.Lerp(valorInicial, valorFinal, (float)pasosActual / (float)pasosMaximo));
        }

        if (devolverEnteros)
        {
            valorActual.Value = (int)valorActual.Value;
            Debug.Log("Valor en entero:" + valorActual.Value);
        }

        //Debug.Log("valorActual.Value: " + valorActual.Value);
    }

}
