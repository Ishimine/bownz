﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class SpikeV3_Trigger : SerializedMonoBehaviour {

    public int id;


    public bool esEspecial = false;
    public bool usarColoracion = true;
    public bool usarEscala = true;


    [Header("Colores")]
    public Color cEspecial;
    public Color cBrillo;
    public Color cActivado;
    public Color cDesactivado = Color.clear;


    [Header("Animacion")]
    /// <summary>
    /// En segundos
    /// </summary>
    public float tAnimacion = .7f;
    public float escalaActivado = 1;
    public float escalaDesactivado = 0;


    public Collider2D col;

    public SpriteRenderer render;

    IEnumerator rutina;


    public SpikeV3_Pinche[] pinches;


    [SerializeField] private SpikeV3_Pared pared;

    public SpikeV3_Pared Pared
    {
        get { return pared; }
        set { pared = value; }
    }
    

    [Button]
    public virtual void Activar()
    {
        Activar(tAnimacion, false);
    }

    public virtual void Activar(bool esEspecial)
    {
        if (esEspecial)
            ActivarEspecial(tAnimacion);
        else
            ActivarNormal(tAnimacion);
    }

    public virtual void ActivarNormal(float tiempo)
    {
        esEspecial = false;
        Activar(cDesactivado, cActivado, Vector3.one * escalaDesactivado, Vector3.one * escalaActivado, tiempo);


        foreach(SpikeV3_Trigger sp in pinches)
        {
            sp.ActivarNormal(tiempo);
        }
    }

    public virtual void ActivarEspecial(float tiempo)
    {
        esEspecial = true;
        Activar(cDesactivado, cEspecial, Vector3.one * escalaDesactivado, Vector3.one * escalaActivado, tiempo);

        foreach (SpikeV3_Trigger sp in pinches)
        {
            sp.ActivarEspecial(tiempo);
        }
    }

    public virtual void Activar(float tiempo, bool especial)
    {
        if (especial)
            ActivarEspecial(tiempo);
        else
            ActivarNormal(tiempo);
    }


    public virtual void Activar(Color colorInicial, Color colorFinal, Vector3 escalaInicial, Vector3 escalaFinal, float segundos)
    {
        gameObject.SetActive(true);
        col.isTrigger = false;
        IniciarAnimTransicion(colorInicial, colorFinal, escalaInicial, escalaFinal, segundos, false);
    }


    [Button]
    public virtual void Desactivar()
    {
        Desactivar(tAnimacion,false);

        foreach (SpikeV3_Pinche p in pinches)
        {
            p.Desactivar(tAnimacion, false);
        }
    }

    
    public virtual void Desactivar(float t, bool contacto)
    {
        if(contacto)
            Desactivar(cBrillo, cDesactivado, Vector3.one * escalaActivado, Vector3.one * escalaDesactivado, t);
        else
            Desactivar(cActivado, cDesactivado, Vector3.one * escalaActivado, Vector3.one * escalaDesactivado, t);


        foreach (SpikeV3_Pinche p in pinches)
        {
            p.Desactivar(t,contacto);
        }
    }

    public virtual void Desactivar(Color colorInicial, Color colorFinal, Vector3 escalaInicial, Vector3 escalaFinal, float segundos)
    {
        esEspecial = false;
        if (!gameObject.activeSelf) return;
        col.isTrigger = true;
        IniciarAnimTransicion(colorInicial, colorFinal, escalaInicial, escalaFinal, segundos, true);
    }


    private void IniciarAnimTransicion(Color colorInicial, Color colorFinal, Vector3 escalaInicial, Vector3 escalaFinal, float segundos, bool desactivarAlFinal)
    {
        if (rutina != null) StopCoroutine(rutina);
        rutina = Transicion(colorInicial, colorFinal, escalaInicial, escalaFinal, segundos, desactivarAlFinal);
        StartCoroutine(rutina);
    }
     
    private IEnumerator Transicion(Color colorInicial, Color colorFinal, Vector3 escalaInicial, Vector3 escalaFinal, float segundos, bool desactivarAlFinal)
    {
        float t = 0;
            if (usarEscala)        transform.localScale = escalaInicial;
        render.color = colorInicial;

    /*    Debug.Log("TimeScale: " + Time.timeScale);
        Debug.Log("Segundos: " + segundos);*/
        float a;
        do
        {
            t += Time.deltaTime / segundos;

            a = Mathf.Lerp(0, 1, Mathf.Sin(Mathf.PI * (t / 2)));

            if (float.IsNaN(a))
                a = 0;

            if (usarEscala)
                transform.localScale = Vector3.Lerp(escalaInicial, escalaFinal, a);

            if (usarColoracion)
                render.color = Color.Lerp(colorInicial, colorFinal, a);

            yield return null;
        } while (t < 1);


        if (desactivarAlFinal)
        {
            gameObject.SetActive(false);
        }
    }








}
