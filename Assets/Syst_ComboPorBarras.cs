﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Syst_ComboPorBarras : MonoBehaviour {



    public ComboPorBarra[] combos;

    public int actual = 0;

    [Header("VariablesCompartida")]
    public VariableString text_ComboPorBarra;
    public VariableColor color_ComboPorBarra;
    public VariableFloat multiplicador_ComboPorBarra;
    public VariableFloat puntaje_cache;


    public void Resetear()
    {
        actual = -1;
    }

    public void ResetearVariablesCompartidas()
    {
        actual = -1;
        ActualizarVariablesCompartidas();
    }


    public void BarraTocada()
    {
        actual++;
        if (actual >= combos.Length) actual = combos.Length - 1;
        ActualizarVariablesCompartidas();
    }


    public void PuntoRealizado()
    {        
        actual = -1;

        puntaje_cache.Value = multiplicador_ComboPorBarra.Value;
    }


    void ActualizarVariablesCompartidas()
    {
        int aux = actual;

        if (aux <= -1) aux = 0;

        color_ComboPorBarra.Value = combos[aux].color;
        text_ComboPorBarra.Value = combos[aux].texto;
        multiplicador_ComboPorBarra.Value = combos[aux].valor;
    }





}


[System.Serializable]
public class ComboPorBarra
{
    public string texto;
    public float valor;
    public Color color;
}


