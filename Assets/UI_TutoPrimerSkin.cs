﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_TutoPrimerSkin : MonoBehaviour {

    public ConfiguracionGeneral configGeneral;
    public Administrador_Progreso progreso;

    public GameObject fondo;

    public void ChequearEstado()
    {
        if (progreso.IsFirstSkin)
        {
            fondo.SetActive(true);
            configGeneral.SwipeReader_Activado = false;
        }
        else
        {
            fondo.SetActive(false);
        }
    }
}
